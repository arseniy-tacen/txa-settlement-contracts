// Copyright © 2022 TXA PTE. LTD.
import { extendEnvironment, task, types } from "hardhat/config"
import { getDeployed } from "./test/utils"

import "@nomiclabs/hardhat-ethers"
import "@nomiclabs/hardhat-waffle"
import "hardhat-deploy"
import "hardhat-deploy-ethers"
if (!process.env.CI_JOB_TOKEN) {
  import("hardhat-gas-reporter")
}
const { NETWORK_URL } = process.env

import "@typechain/hardhat"

task("dmc-balance", "Display user's balance of DummyCoin")
  .addParam("address", "User's address")
  .setAction(async (args, env) => {
    let dummyCoin = await env.deployments.get("DummyCoin")
    let contract = await env.ethers.getContractAt(
      "DummyCoin",
      dummyCoin.address
    )
    let balance = await contract.balanceOf(args.address)
    console.log(balance)
  })

import { depositToken } from "./token-deposit"

task(
  "send-to-escrow",
  "Sends a token to an escrow smart contract belonging to an account"
)
  .addOptionalParam(
    "account",
    `This could be anyone of the named/unnamed accounts such as:
    \t\texchange,
    \t\tsdpAdmin1,
    \t\tsdpAdmin2,
    \t\tsdpAdmin3,
    \t\tlocalCoord,
    \t\tauditor,
    \t\talice,
    \t\tbob,
    \t\tnoRole,
    \t\ttrustedForwarder,
    \t\tfeeRecipient,
    \t\tconsensus,
    \t\tsdpOperator1,
    \t\tsdpOperator2,
    \t\tsdpOperator3,
    \t\tsdpOperator4,
    \t\tsdpAdmin4,
     \t\t0x...\n\t\t`,
    "alice"
  )
  .addOptionalParam(
    "amount",
    "The amount to send to the contract\n\t\t",
    1000,
    types.int
  )
  .addOptionalParam(
    "token",
    `The token to send to the contract, this could be:
     \t\tDummyCoin
     \t\tDummyCoin777
     \t\tUSDT\n\t\t`,
    "DummyCoin"
  )
  .setAction(async ({ account, amount, token }, env) => {
    await depositToken(account, amount, env, token)
  })

export default {
  docgen: { outputDir: "docs", pages: "files" },
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 400,
      },
    },
  },
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
      live: false,
      saveDeployments: true,
      accounts: {
        count: 100,
      },
    },
    ganache: {
      live: false,
      saveDeployments: false,
      url: "http://localhost:8547",
      accounts: {
        mnemonic:
          "pupil describe wild around truth fiber grocery solar oval repair oxygen drift",
        path: "m/44'/60'/0'/0",
        initialIndex: 0,
        count: 30,
      },
    },
    cliquebait: {
      live: true,
      saveDeployments: true,
      url: "http://localhost:8545",
      accounts: {
        mnemonic:
          "pupil describe wild around truth fiber grocery solar oval repair oxygen drift",
        path: "m/44'/60'/0'/0",
        initialIndex: 0,
        count: 30,
      },
    },
    test_eth: {
      live: true,
      saveDeployments: true,
      url: "http://localhost:8547",
      accounts: {
        mnemonic:
          "pupil describe wild around truth fiber grocery solar oval repair oxygen drift",
        path: "m/44'/60'/0'/0",
        initialIndex: 0,
        count: 30,
      },
    },
    test_etc: {
      live: true,
      saveDeployments: true,
      url: "http://localhost:8546",
      accounts: {
        mnemonic:
          "pupil describe wild around truth fiber grocery solar oval repair oxygen drift",
        path: "m/44'/60'/0'/0",
        initialIndex: 0,
        count: 30,
      },
    },
    cliquebait_remote: {
      live: true,
      saveDeployments: true,
      url: `${NETWORK_URL}`,
      accounts: {
        mnemonic:
          "pupil describe wild around truth fiber grocery solar oval repair oxygen drift",
        path: "m/44'/60'/0'/0",
        initialIndex: 0,
        count: 30,
      },
    },
  },
  // Note: ensure that total_accounts in .solcover.js
  //       is gte the number of named accounts
  namedAccounts: {
    exchange: 0,
    sdpAdmin1: 1,
    sdpAdmin2: 2,
    sdpAdmin3: 3,
    localCoord: 4,
    auditor: 5,
    alice: 7,
    bob: 8,
    noRole: 9,
    trustedForwarder: 10,
    feeRecipient: 11,
    consensus: 12,
    sdpOperator1: 21,
    sdpOperator2: 22,
    sdpOperator3: 23,
    sdpOperator4: 24,
    sdpAdmin4: 25,
  },
}
