// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./PredeployConstants.sol";
import "../CollateralCustody.sol";

contract CollateralCustodyPreDeploy is CollateralCustody, PredeployConstants {
    constructor() CollateralCustody(IDENTITY_REGISTRY) {}

    function initialize() external {
        identity = IdentityRegistryInterface(IDENTITY_REGISTRY);
    }
}
