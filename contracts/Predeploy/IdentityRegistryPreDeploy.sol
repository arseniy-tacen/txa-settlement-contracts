// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../IdentityRegistry.sol";
import "./PredeployConstants.sol";

interface PreDeployInitialize {
    function initialize() external;
}

/**
 * Version of IdentityRegistry instrumented for pre-deployment in a geth genesis block.
 */
contract IdentityRegistryPreDeploy is IdentityRegistry, PredeployConstants {
    // Here just so contract compiles
    // Doesn't effect storage since we're predeploying
    constructor(address governance, uint256 _unlockInterval) IdentityRegistry(governance, _unlockInterval) {}

    /**
     * Called by a script from a docker container after starting cliquebait
     * with predeployed contracts.
     *
     * Calling this function brings the storage of each system contract to the
     * the same state as if every constructor had been run.
     */
    function initialize(uint256 _unlockInterval) external {
        // run code from original IdentityRegistry constructor
        fee = Fraction(1, 1000); // 0.1%
        _setupRole(SettlementLib.ROLE_GOVERNANCE, GOVERNANCE);
        _setRoleAdmin(SettlementLib.TRADEABLE_TOKEN, SettlementLib.ROLE_GOVERNANCE);
        require(_unlockInterval > 240);
        unlockInterval = _unlockInterval;

        // setup system roles
        _initializeRole(SettlementLib.ROLE_CONSENSUS, CONSENSUS);
        _initializeRole(SettlementLib.ROLE_LOCALCOORD, COORDINATOR);
        _initializeRole(SettlementLib.ROLE_ASSET_CUSTODY, SINGLETON);
        _initializeRole(SettlementLib.ROLE_COLLATERAL_CUSTODY, COLLATERAL_CUSTODY);
        feeRecipient = FEE_RECIPIENT;

        // call initialize on all system contracts
        PreDeployInitialize(CONSENSUS).initialize();
        PreDeployInitialize(COORDINATOR).initialize();
        PreDeployInitialize(SINGLETON).initialize();
        PreDeployInitialize(COLLATERAL_CUSTODY).initialize();
    }

    function _initializeRole(bytes32 role, address initialAddress) internal {
        require(getRoleMemberCount(role) == 0, "ALREADY_INITIALIZED");
        _setupRole(role, initialAddress);
        versionInfo[initialAddress] = Version(1, 0);
    }
}
