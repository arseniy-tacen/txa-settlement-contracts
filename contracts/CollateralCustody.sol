// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./Interfaces/IdentityRegistryInterface.sol";

/**
 * CollateralCustody allows SDP admins to manage collateral used in settlements.
 *
 * Admins deposit assets into this contract and authorize operators to use them for
 * collateral when reporting obligations for settlements.
 *
 * The SettlementDataConsensus contract is the only address authorized to lock and
 * unlock collateral deposited into this contract.
 */
contract CollateralCustody {
    /**
     * Instance of IdentityRegistry interface.
     */
    IdentityRegistryInterface internal identity;

    /**
     * The zero address is used as the key for tracking native token balances in mappings.
     */
    address internal constant _za = address(0);

    /**
     * Maps admin address to token address to amount deposited.
     *
     * Tracks how much of each token an admin has deposited into this contract
     * for use as collateral in a settlement.
     */
    mapping(address => mapping(address => uint256)) public collateralBalances;

    /**
     * Maps admin address to token address to amount locked.
     *
     * Tracks how much of an admin's collateral has been locked in a settlement.
     */
    mapping(address => mapping(address => uint256)) public lockedBalances;

    /**
     * Maps operator address to authorized administrator address.
     *
     * Tracks which balances an operator can use when reporting for settlement.
     */
    mapping(address => address) public adminFor;

    /**
     * Emitted when an admin deposits collateral into this contract.
     */
    event CollateralDeposit(address admin, uint256 amount, address token);

    /**
     * Emitted when an admin withdraws collateral from this contract.
     */
    event CollateralWithdraw(address admin, uint256 amount, address token);

    /**
     * Emitted when an admin approves an operator address.
     */
    event OperatorApproved(address operator, address admin);

    /**
     * Emitted when an admin revokes approval of an operator address.
     */
    event OperatorRevoked(address operator, address admin);

    constructor(address _identity) {
        identity = IdentityRegistryInterface(_identity);
    }

    /**
     * Called by SDP admins to deposit native tokens for use as collateral.
     */
    function deposit() external payable {
        require(identity.isSDPAdmin(msg.sender), "NOT_SDP_ADMIN");
        collateralBalances[msg.sender][_za] += msg.value;
        emit CollateralDeposit(msg.sender, msg.value, _za);
    }

    /**
     * Called by SDP admins to deposit an approved ERC20 token for use as collateral.
     *
     * Admin must approve this contract as a spender before calling.
     *
     * @param amount Amount of token to be deposited
     * @param token  Address of the token
     */
    function depositToken(uint256 amount, address token) external {
        require(identity.isSDPAdmin(msg.sender), "NOT_SDP_ADMIN");
        require(identity.isTradeableToken(token) || identity.isProtocolToken(token), "NOT_DEPOSITABLE_TOKEN");
        require(IERC20(token).transferFrom(msg.sender, address(this), amount), "TRANSFER_FAILED");
        collateralBalances[msg.sender][token] += amount;
        emit CollateralDeposit(msg.sender, amount, token);
    }

    /**
     * Called by SDP admins to withdraw an asset that was previously deposited for use as collateral.
     *
     * Assets cannot be withdrawn if they are currently locked in a settlement.
     *
     * @param amount Amount of tokens to be withdrawn
     * @param token   Address of token to be withdrawn
     */
    function withdraw(uint256 amount, address token) external {
        require(identity.isSDPAdmin(msg.sender), "NOT_SDP_ADMIN");
        require(
            collateralBalances[msg.sender][token] - lockedBalances[msg.sender][token] >= amount,
            "INSUFFICIENT_BALANCE"
        );

        collateralBalances[msg.sender][token] -= amount;
        if (token == _za) {
            (bool success, ) = msg.sender.call{value: amount}("");
            require(success, "TRANSFER_FAILED");
        } else {
            require(IERC20(token).transfer(msg.sender, amount), "TRANSFER_FAILED");
        }
        emit CollateralWithdraw(msg.sender, amount, token);
    }

    /**
     * Called by the consensus contract to lock an admin's asset for use as collateral in a settlement.
     *
     * When an SDP operator attempts to report obligations for a settlement, the consensus contract calls
     * this function to ensure that its corresponding admin has sufficient collateral to participate.
     * Any asset locked through this function becomes unavailable for withdrawal or use in a settlement.
     *
     * @param operator Address of the SDP operator reporting obligations
     * @param token Address of the token used as collateral
     * @param amount Amount of the token that must be locked
     */
    function lockCollateral(
        address operator,
        address token,
        uint256 amount
    ) external {
        require(identity.getLatestConsensus() == msg.sender, "SENDER_NOT_CONSENSUS");
        address admin = adminFor[operator];
        lockedBalances[admin][token] += amount;
        require(
            lockedBalances[admin][token] <= collateralBalances[admin][token],
            "INSUFFICIENT_UNLOCKED_TOKENS"
        );
    }

    /**
     * Called by the consensus contract to unlock an admin's asset after settlement clears.
     *
     * @param operator Address of the SDP operator that participated in settlement
     * @param token Address of the token used as collateral
     * @param amount Amount of the token that must be unlocked
     */
    function unlockCollateral(
        address operator,
        address token,
        uint256 amount
    ) external {
        require(identity.getLatestConsensus() == msg.sender, "SENDER_NOT_CONSENSUS");
        require(amount <= lockedBalances[adminFor[operator]][token], "INSUFFICIENT_LOCKED_TOKENS");
        lockedBalances[adminFor[operator]][token] = lockedBalances[adminFor[operator]][token] - amount;
    }

    /**
     * Called by SDP admins to approve an address as an operator.
     *
     * Off-chain, the operator private key is used to sign the admin
     * address. That signature is submitted by the admin in this function.
     * This allows the operator to use the collateral deposited by the
     * admin address when reporting settlements.
     *
     * @param operator   Address that admin is approving as an operator
     * @param v                 v parameter in Ethereum signature
     * @param r                 r parameter in Ethereum signature
     * @param s                 s parameter in Ethereum signature
     */
    function approveOperator(
        address operator,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        require(identity.isSDPAdmin(msg.sender), "NOT_SDP_ADMIN");
        require(verifySig(msg.sender, v, r, s) == operator, "SIGNATURE_VERIF_FAILED_OPERATOR_OR_ADMIN");

        adminFor[operator] = msg.sender;
        emit OperatorApproved(operator, msg.sender);
    }

    /**
     * Called by SDP admins to revoke an operator.
     *
     * @param operator   Address of operator to revoke
     */
    function revokeOperator(address operator) external virtual {
        require(identity.isSDPAdmin(msg.sender), "NOT_SDP_ADMIN");
        require(adminFor[operator] == msg.sender, "ADMIN_NOT_OPERATOR_OWNER");

        delete adminFor[operator];
        emit OperatorRevoked(operator, msg.sender);
    }

    /**
     * Internal function for verifying an Ethereum signature where the message body is an address.
     *
     * @param message           Message body of the signature
     * @param v                 v parameter in Ethereum signature
     * @param r                 r parameter in Ethereum signature
     * @param s                 s parameter in Ethereum signature
     */
    function verifySig(
        address message,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) internal pure returns (address signer) {
        bytes32 messageDigest = keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n20", message));
        return ecrecover(messageDigest, v, r, s);
    }
}
