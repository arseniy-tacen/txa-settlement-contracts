// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./SettlementLib.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "./Interfaces/IdentityRegistryInterface.sol";
import "./FullMath.sol";

/**
 * IdentityRegistry allows the governance address to grant system roles to
 * other addresses and manage key parameters such as the settlement timeout
 * interval, fee percentage, collateral percentage, and tradeable tokens.
 *
 * Other system contracts call the IdentityRegistry for the information above.
 * e.g. AssetCustody must only allow a trusted instance of the SettlementCoordination
 * contract to write trader obligations. To verify that an address is actually the
 * coordinator, it will check with the IdentityRegistry.
 */
contract IdentityRegistry is AccessControlEnumerable, IdentityRegistryInterface {
    /**
     * Number of blocks that must be mined after a user requests settlement before
     * considering it failed and allowing withdrawal.
     */
    uint256 public unlockInterval;

    /**
     * Stores the version id of a system contract and, if an old version,
     * the date at which it is considered expired.
     */
    struct Version {
        uint256 id;
        uint256 expirationDate;
    }

    /**
     * Maps contract addresses to version information.
     */
    mapping(address => Version) public versionInfo;

    /**
     * Amount of time until an old contract is considered expired.
     */
    uint256 public constant EXPIRATION_TIME = 7 days;

    /**
     * Address that receives a fee each time a trader requests
     * funds owed from another trader.
     */
    address public feeRecipient;

    /**
     * Stores a numerator and denominator.
     *
     * Used to calculate percentages of another value.
     * e.g. obligationAmount * n / d = collateralAmount
     */
    struct Fraction {
        uint256 n;
        uint256 d;
    }

    /**
     * Percentage of each settlement that is taken as a fee by the protocol.
     */
    Fraction public fee;

    /**
     * Percentage of each settlement that is required as collateral.
     */
    Fraction public collateralPercent;

    /**
     * Amount of protocol tokens required as additional collateral for each settlement.
     */
    uint256 public protocolStakeAmount;

    constructor(address governance, uint256 _unlockInterval) {
        fee = Fraction(1, 1000); // 0.1%
        collateralPercent = Fraction(1100, 1000); // 110%
        _setupRole(SettlementLib.ROLE_GOVERNANCE, governance);
        _setRoleAdmin(SettlementLib.TRADEABLE_TOKEN, SettlementLib.ROLE_GOVERNANCE);
        _setRoleAdmin(SettlementLib.PROTOCOL_TOKEN, SettlementLib.ROLE_GOVERNANCE);
        _setRoleAdmin(SettlementLib.ROLE_AUDITOR, SettlementLib.ROLE_GOVERNANCE);

        // arbitrary minimum unlock interval of approx. 1 hour on Ethereum mainnet
        require(_unlockInterval > 240);
        unlockInterval = _unlockInterval;
    }

    /**
     * Called by governance address to change the fee recipient address.
     *
     * @param _feeRecipient Address that will receive fees
     */
    function setFeeRecipient(address _feeRecipient) external onlyGovernance {
        feeRecipient = _feeRecipient;
    }

    /**
     * Called by governance address to change the amount of protocol tokens
     * required to be locked in order for an SDP to participate in settlement.
     *
     * @param _protocolStakeAmount Amount of protocol tokens
     */
    function setStakeAmount(uint256 _protocolStakeAmount) external onlyGovernance {
        protocolStakeAmount = _protocolStakeAmount;
    }

    /**
     * Called by governance address to set a system role for the first time.
     *
     * @param role The hash of the role to initialize
     * @param initialAddress The address that will receive the role
     */
    function initializeRole(bytes32 role, address initialAddress) external onlyGovernance {
        require(getRoleMemberCount(role) == 0, "ALREADY_INITIALIZED");
        _setupRole(role, initialAddress);
        versionInfo[initialAddress] = Version(1, 0);
    }

    /**
     * Called by governance address to update a system role.
     *
     * Forbids granting a role to the same address twice.
     * Sets an expiration date for the old version of the role.
     *
     * @param role The hash of the role to initialize
     * @param newAddress The address that will receive the role
     */
    function updateRole(bytes32 role, address newAddress) external onlyGovernance {
        require(getRoleMemberCount(role) > 0, "NOT_INITIALIZED");
        require(!hasRole(role, newAddress), "ALREADY_HAS_ROLE");

        versionInfo[getRoleMember(role, getRoleMemberCount(role) - 1)].expirationDate =
            block.timestamp +
            EXPIRATION_TIME;
        _setupRole(role, newAddress);
        versionInfo[newAddress] = Version(getRoleMemberCount(role), 0);
    }

    function grantSDPAdmin(address admin) external {
        _setupRole(SettlementLib.ROLE_SDP_ADMIN, admin);
    }

    /**
     * Returns the latest version number of the coordinator contract.
     */
    function getLatestCoordinatorVersion() external view returns (uint256) {
        return getRoleMemberCount(SettlementLib.ROLE_LOCALCOORD);
    }

    /**
     * Returns the address of the latest coordinator contract.
     */
    function getLatestCoordinator() external view returns (address) {
        uint256 memberCount = getRoleMemberCount(SettlementLib.ROLE_LOCALCOORD);
        require(memberCount > 0, "ERROR_NO_LOCALCOORD");
        return getRoleMember(SettlementLib.ROLE_LOCALCOORD, memberCount - 1);
    }

    /**
     * Returns the address of a coordinator based on the version number.
     *
     * @param version Version of the coordinator to lookup
     */
    function getCoordinator(uint256 version) external view returns (address) {
        return getRoleMember(SettlementLib.ROLE_LOCALCOORD, version);
    }

    /**
     * Returns the address of the latest asset custody contract.
     */
    function getLatestAssetCustody() external view returns (address) {
        uint256 memberCount = getRoleMemberCount(SettlementLib.ROLE_ASSET_CUSTODY);
        require(memberCount > 0, "ERROR_NO_ASSET_CUSTODY");
        return getRoleMember(SettlementLib.ROLE_ASSET_CUSTODY, memberCount - 1);
    }

    /**
     * Returns the address of the latest collateral custody contract.
     */
    function getLatestCollateralCustody() external view returns (address) {
        uint256 memberCount = getRoleMemberCount(SettlementLib.ROLE_COLLATERAL_CUSTODY);
        require(memberCount > 0, "ERROR_NO_COLLATERAL_CUSTODY");
        return getRoleMember(SettlementLib.ROLE_COLLATERAL_CUSTODY, memberCount - 1);
    }

    /**
     * Determines whether or not a coordinator is considered expired.
     *
     * Called by AssetCustody to determine if the coordinator is eligible
     * to interact with a specific trader. Checks the latest coordinator
     * version that the trader opted in to, as well as the earliest version
     * of the coordinator that the trader accepts.
     * Also checks coordinator expiration based on function below.
     *
     * TODO: Upgrading coordinators is not fully supported yet
     *
     * @param coord Address to check
     * @param versionId Latest version trader opted in to
     * @param minVersionId Earliest version the trader accepts
     */
    function isUnexpiredCoordinator(
        address coord,
        uint256 versionId,
        uint256 minVersionId
    ) external view returns (bool) {
        return (versionInfo[coord].id <= versionId &&
            versionInfo[coord].id >= minVersionId &&
            isUnexpiredCoordinator(coord));
    }

    /**
     * Determines whether or not a coordinator is considered expired.
     *
     * Checks if the given address is either the latest coordinator or
     * an older coordinator that has not yet past its expiration date.
     *
     * @param coord Address to check
     */
    function isUnexpiredCoordinator(address coord) public view returns (bool) {
        return (
            (coord ==
                getRoleMember(
                    SettlementLib.ROLE_LOCALCOORD,
                    getRoleMemberCount(SettlementLib.ROLE_LOCALCOORD) - 1
                ) ||
                versionInfo[coord].expirationDate > block.timestamp)
        );
    }

    /**
     * Given an obligation amount, calculates the fee that must be paid.
     *
     * @param amount Amount of an asset transferred from one trader to another
     */
    function calculateFee(uint256 amount) external view returns (uint256) {
        return FullMath.mulDiv(fee.n, amount, fee.d);
    }

    /**
     * Given a settlement amount, calculates the amount of collateral an SDP must
     * stake in order to participate.
     *
     * @param amount Total amount of an asset in a single settlement
     */
    function calculateCollateral(uint256 amount) external view returns (uint256) {
        return FullMath.mulDiv(collateralPercent.n, amount, collateralPercent.d);
    }

    /**
     * Returns the address of the latest consensus contract.
     */
    function getLatestConsensus() external view returns (address) {
        uint256 memberCount = getRoleMemberCount(SettlementLib.ROLE_CONSENSUS);
        require(memberCount > 0, "ERROR_NO_CONSENSUS");
        return getRoleMember(SettlementLib.ROLE_CONSENSUS, memberCount - 1);
    }

    /**
     * Returns the address with authorization to perform governance actions.
     */
    function getGovernanceAddress() external view returns (address) {
        return getRoleMember(SettlementLib.ROLE_GOVERNANCE, 0);
    }

    /**
     * Checks if an address has been approved as a tradeable token in the DSL.
     *
     * In order for a token to be traded on the DSL, the governance address must
     * grant it the tradeable token role. This function is also used to check if
     * an SDP can stake this token as collateral.
     *
     * @param token Address to check
     */
    function isTradeableToken(address token) external view returns (bool) {
        return hasRole(SettlementLib.TRADEABLE_TOKEN, token);
    }

    /**
     * Checks if an address has been approved as a settlement auditor in the DSL.
     *
     * In order for an address to participate in settlements as an auditor, the
     * governance address must grant it the auditor role.
     *
     * @param auditor Address to check
     */
    function isAuditor(address auditor) external view returns (bool) {
        return hasRole(SettlementLib.ROLE_AUDITOR, auditor);
    }

    /**
     * Checks if an address has been approved as a protocol token in the DSL.
     *
     * An SDP must stake the protocol token and provide it as additional
     * collateral in order to participate in settlements.
     *
     * @param token Address to check
     */
    function isProtocolToken(address token) external view returns (bool) {
        return hasRole(SettlementLib.PROTOCOL_TOKEN, token);
    }

    /**
     * Returns the latest version of the protocol token.
     */
    function getLatestProtocolToken() external view returns (address) {
        uint256 memberCount = getRoleMemberCount(SettlementLib.PROTOCOL_TOKEN);
        require(memberCount > 0, "ERROR_NO_PROTOCOL_TOKEN");
        return getRoleMember(SettlementLib.PROTOCOL_TOKEN, memberCount - 1);
    }

    /**
     * Checks if an address has been approved as an SDP admin.
     *
     * @param admin Address to check
     */
    function isSDPAdmin(address admin) external view returns (bool) {
        return hasRole(SettlementLib.ROLE_SDP_ADMIN, admin);
    }

    /**
     * Forbids any address without the governance role from executing an action.
     */
    modifier onlyGovernance() {
        require(hasRole(SettlementLib.ROLE_GOVERNANCE, _msgSender()), "SENDER_NOT_GOVERNANCE");
        _;
    }
}
