// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.

pragma solidity ^0.8.9;

abstract contract MerkleTreeGeneration {
    function getMerkleRoot(bytes32[] memory leaves) public pure returns (bytes32) {
        return recursiveMerkleTree(leaves, 0, leaves.length);
    }

    function recursiveMerkleTree(
        bytes32[] memory leaves,
        uint256 start,
        uint256 end
    ) internal pure returns (bytes32) {
        if (end - start > 2) {
            uint256 byTwo = 2;
            while (end - start - byTwo > byTwo) {
                byTwo = byTwo * 2;
            }
            uint256 middle = start + byTwo;
            bytes32 node1 = recursiveMerkleTree(leaves, start, middle);
            bytes32 node2 = recursiveMerkleTree(leaves, middle, end);

            return _hashPair(node1, node2);
        } else if (end - start == 1) {
            return leaves[start];
        } else if (end - start == 2) {
            return _hashPair(leaves[start], leaves[end - 1]);
        } else {
            revert("Recursive State: indexes start = end. State should not be reached");
        }
    }

    /*
    The functions below are based on code from OpenZeppelin: 
    https://github.com/OpenZeppelin/openzeppelin-contracts/blob/6ab8d6a67e3281ab062bdbe4df32b95c6409ee6d/contracts/utils/cryptography/MerkleProof.sol#L200
    The code below is subject to the following license

    The MIT License (MIT)

    Copyright (c) 2016-2022 zOS Global Limited and contributors

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above Copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */
    function _hashPair(bytes32 a, bytes32 b) private pure returns (bytes32) {
        return a < b ? _efficientHash(a, b) : _efficientHash(b, a);
    }

    function _efficientHash(bytes32 a, bytes32 b) private pure returns (bytes32 value) {
        /// @solidity memory-safe-assembly
        assembly {
            mstore(0x00, a)
            mstore(0x20, b)
            value := keccak256(0x00, 0x40)
        }
    }
}
