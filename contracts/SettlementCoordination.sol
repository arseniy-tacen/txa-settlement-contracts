// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./Interfaces/IdentityRegistryInterface.sol";
import "./Interfaces/AssetInterface.sol";
import "./Interfaces/CoordinationInterface.sol";

/**
 * SettlementCoordination (aka coordinator) acts as an intermediary between the AssetCustody
 * and the SettlementDataConsensus contracts. It tracks the number of settlements recorded
 * on the chain its deployed to, as well as the block number at which settlement was requested
 * and the asset being settled.
 *
 * Traders requesting settlement call the coordinator through the custody contract.
 * SDPs reporting obligations call the coordinator through the consensus contract.
 */
contract SettlementCoordination is CoordinationInterface {
    /**
     * The ID of the last settlement for which all obligations have been written.
     */
    uint256 public lastSettlementIdProcessed = 1;

    /**
     * The settlement ID that will be assigned to the next settlement requested.
     */
    uint256 public nextRequestId = 2;

    /**
     * Maps settlement ID to the block number at the time of request and
     * the asset being settled.
     */
    mapping(uint256 => SettlementData) internal settlementRequests;

    /**
     * Instance of IdentityRegistry interface.
     */
    IdentityRegistryInterface internal identity;

    /**
     * Emitted when all obligations for a settlement have been written and trader
     * balances in AssetCustody have been updated.
     */
    event ObligationsWritten(uint256 id);

    constructor(address _identity) {
        identity = IdentityRegistryInterface(_identity);
    }

    /**
     * Called by traders through the AssetCustody contract to start the settlement process.
     *
     * Verifies with IdentityRegistry that caller has the proper role.
     * Records the block number and token being requested for this settlement ID.
     * Returns the ID back to the AssetCustody contract.
     * Increments the ID for the next request.
     *
     * Based on the implementation of `AssetCustody.requestSettlement()`, we can
     * assume that the caller has opted-in to this version of the coordinator.
     *
     * @param token Address of the asset for which settlement is being requested
     */
    function requestSettlement(address token) external virtual returns (uint256) {
        require(identity.getLatestAssetCustody() == msg.sender, "NOT_WALLET_SINGLETON");
        settlementRequests[nextRequestId].requestBlock = block.number;
        settlementRequests[nextRequestId].token = token;
        nextRequestId++;
        return nextRequestId - 1;
    }

    /**
     * Upon reaching a quorum, SDPs call this function through SettlementDataConsensus
     * to report obligations for a settlement.
     *
     * Checks that settlement being reported for has the next ID in the sequence.
     * Checks that all obligations involve the token for which settlement was requested.
     * For each obligation, the `reallocate` flag determines if the coordinator will
     * create a new obligation or reallocate a previously written obligation.
     * Increments the last settlement ID processed.
     *
     * @param id ID of the settlement that originated on this chain
     * @param obligations Array of obligations for this settlement
     */
    function reportObligations(uint256 id, SettlementLib.Obligation[] calldata obligations) external {
        require(id == lastSettlementIdProcessed + 1, "WRONG_ID");
        require(msg.sender == identity.getLatestConsensus(), "SENDER_NOT_CONSENSUS");

        for (uint256 i = 0; i < obligations.length; i++) {
            require(settlementRequests[id].token == obligations[i].token, "INVALID_TOKEN");
            if (!obligations[i].reallocate) {
                require(
                    AssetInterface(identity.getLatestAssetCustody()).writeObligation(
                        obligations[i].deliverer,
                        obligations[i].recipient,
                        obligations[i].token,
                        obligations[i].amount
                    ),
                    "WRITE_OB_FAILED"
                );
            } else {
                require(
                    AssetInterface(identity.getLatestAssetCustody()).reallocateObligation(
                        obligations[i].deliverer,
                        obligations[i].recipient,
                        obligations[i].token,
                        obligations[i].amount
                    ),
                    "WRITE_OB_FAILED"
                );
            }
        }
        emit ObligationsWritten(id);
        lastSettlementIdProcessed++;
    }

    /**
     * Returns the block number and token address associated with a given ID.
     *
     * @param reqId ID of the settlement request
     */
    function getReqData(uint256 reqId) external view returns (SettlementData memory) {
        return settlementRequests[reqId];
    }
}
