// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../AssetCustody.sol";

/**
 * Provides a mocked version of the AssetCustody to call when
 * unit testing SettlementCoordination.
 */
contract AssetMock is AssetCustody {
    /**
     * Tracks how many times writeObligation was called in tests.
     */
    uint256 public writeCount;

    /**
     * Tracks how many times reallocateObligation was called in tests.
     */
    uint256 public reallocateCount;

    /**
     * Flag for reverting writeObligation and reallocateObligation.
     */
    bool public forceFailure;

    constructor(address _identity) AssetCustody(_identity) {
        forceFailure = false;
    }

    /**
     * Overides writeObligation to either revert or record number of calls.
     */
    function writeObligation(
        address delivererAddress,
        address recipientAddress,
        address tokenAddress,
        uint256 amount
    ) external override returns (bool) {
        assert(!forceFailure);
        writeCount++;
        return true;
    }

    /**
     * Overides reallocateObligation to either revert or record number of calls.
     */
    function reallocateObligation(
        address delivererAddress,
        address recipientAddress,
        address tokenAddress,
        uint256 amount
    ) external override returns (bool) {
        require(!forceFailure, "UNDEROBLIGATED");
        reallocateCount++;
        return true;
    }

    /**
     * Called from unit test to mock when AssetCustody calls requestSettlement on
     * SettlementCoordination.
     */
    function coordinationRequestSettlement(address coordAddress, address tokenAddress)
        external
        returns (uint256)
    {
        return CoordinationInterface(coordAddress).requestSettlement(tokenAddress);
    }

    /**
     * Sets flag for forcing mocked functions to fail.
     */
    function setFunctionFailure() external returns (bool) {
        forceFailure = true;
        return forceFailure;
    }
}
