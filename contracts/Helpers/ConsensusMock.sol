// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../SettlementDataConsensus.sol";

contract ConsensusMock is SettlementDataConsensus {
    constructor(address _identity) SettlementDataConsensus(_identity) {}

    function callLockCollateral(
        address accountAddress,
        address tokenAddress,
        uint256 amount,
        address collateralAddress
    ) external {
        CollateralInterface(collateralAddress).lockCollateral(accountAddress, tokenAddress, amount);
    }

    function callUnlockCollateral(
        address accountAddress,
        address tokenAddress,
        uint256 amount,
        address collateralAddress
    ) external {
        CollateralInterface(collateralAddress).unlockCollateral(accountAddress, tokenAddress, amount);
    }
}
