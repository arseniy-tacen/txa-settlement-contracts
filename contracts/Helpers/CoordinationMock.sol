// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../SettlementCoordination.sol";

contract CoordinationMock is SettlementCoordination {
    constructor(address _identity) SettlementCoordination(_identity) {}

    /**
     * Overrides requestSettlement to operate without auth checks.
     */
    function requestSettlement(address token) external override returns (uint256) {
        settlementRequests[nextRequestId].requestBlock = block.number;
        settlementRequests[nextRequestId].token = token;
        nextRequestId++;
        return nextRequestId - 1;
    }

    /**
     * Moves forward the last processed settlement ID by numSettlements.
     */
    function completeSettlements(uint256 numSettlements) external {
        lastSettlementIdProcessed += numSettlements;
    }

    /**
     * Force Coordinator to write an obligation to custody contract.
     */
    function assetWriteObligation(
        address deliverer,
        address recipient,
        address token,
        uint256 amount,
        address assetCustody
    ) external {
        AssetInterface(assetCustody).writeObligation(deliverer, recipient, token, amount);
    }

    /**
     * Force Coordinator to reallocate an obligation to custody contract.
     */
    function assetReallocateObligation(
        address deliverer,
        address recipient,
        address token,
        uint256 amount,
        address assetCustody
    ) external {
        AssetInterface(assetCustody).reallocateObligation(deliverer, recipient, token, amount);
    }
}
