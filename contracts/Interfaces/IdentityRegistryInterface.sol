// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

interface IdentityRegistryInterface {
    function feeRecipient() external returns (address);

    function unlockInterval() external returns (uint256);

    function isUnexpiredCoordinator(
        address coord,
        uint256 versionId,
        uint256 minVersionId
    ) external view returns (bool);

    function protocolStakeAmount() external view returns (uint256);

    function getLatestProtocolToken() external view returns (address);

    function getLatestAssetCustody() external view returns (address);

    function getLatestCollateralCustody() external view returns (address);

    function getLatestConsensus() external view returns (address);

    function getLatestCoordinator() external view returns (address);

    function getCoordinator(uint256 version) external view returns (address);

    function getLatestCoordinatorVersion() external view returns (uint256);

    function getGovernanceAddress() external view returns (address);

    function calculateFee(uint256 amount) external view returns (uint256);

    function calculateCollateral(uint256 amount) external view returns (uint256);

    function isTradeableToken(address token) external view returns (bool);

    function isProtocolToken(address token) external view returns (bool);

    function isAuditor(address auditor) external view returns (bool);

    function isSDPAdmin(address adminAddress) external view returns (bool);
}
