// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

interface CollateralInterface {
    function lockCollateral(
        address account,
        address token,
        uint256 amount
    ) external;

    function unlockCollateral(
        address account,
        address token,
        uint256 amount
    ) external;

    function adminFor(address operatorAddress) external returns (address);

    function collateralBalances(address admin, address token) external returns (uint256);

    function lockedBalances(address admin, address token) external returns (uint256);
}
