// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../SettlementLib.sol";

interface CoordinationInterface {
    function nextRequestId() external returns (uint256);

    function lastSettlementIdProcessed() external returns (uint256);

    struct SettlementData {
        uint256 requestBlock;
        address token;
    }

    function getReqData(uint256) external returns (SettlementData memory);

    struct Obligation {
        uint256 amount;
        address deliverer;
        address recipient;
        address token;
        bool reallocate;
    }

    function requestSettlement(address) external returns (uint256);

    function reportObligations(uint256 id, SettlementLib.Obligation[] calldata obligations) external;
}
