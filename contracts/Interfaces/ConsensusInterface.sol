// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../SettlementLib.sol";

interface ConsensusInterface {
    function reportSettlementObligations(
        address coordinator,
        uint256 settlementId,
        SettlementLib.Obligation[] memory reportedObligations,
        bytes32 merkleRoot
    ) external;

    function verifyMerkleTree(SettlementLib.Obligation[] memory reportedObligations)
        external
        pure
        returns (bytes32);
}
