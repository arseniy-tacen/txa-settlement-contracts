// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

interface AssetInterface {
    function writeObligation(
        address delivererAddress,
        address recipientAddress,
        address tokenAddress,
        uint256 amount
    ) external returns (bool);

    function reallocateObligation(
        address delivererAddress,
        address recipientAddress,
        address tokenAddress,
        uint256 amount
    ) external returns (bool);
}
