// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./SettlementLib.sol";
import "./Interfaces/IdentityRegistryInterface.sol";
import "./Interfaces/CoordinationInterface.sol";
import "./Interfaces/AssetInterface.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

/**
 * AssetCustody allows traders to interact with the TXA Decentralized Settlement Layer.
 *
 * When traders first deposit a token or the native asset into this contract, the funds become available for submitting
 * orders on a participating exchange. This contract tracks how much of each asset a trader deposits.
 *
 * When a trader is ready to settle for an asset, they make a request which notifies the Settlement Data Providers to
 * calculate and submit the obligation data. At this point, any orders for the requested asset cannot be submitted or filled
 * by the trader on participating exchanges.
 *
 * After the providers reach a consensus and the obligations are written to this contract, a trader can request funds owed
 * by counterparties as a result of trading on a participating exchange, updating the trader's available balance in the
 * Asset Custody contract. The trader can then withdraw from this balance.
 *
 * When a trader is ready to trade that asset once again, they can call the collateralize function. This disables further
 * withdrawals and signals to the participating exchanges that the asset can once again be used to submit orders.
 *
 */
contract AssetCustody is AssetInterface {
    /**
     * The zero address is used as the key for tracking native token balances in mappings.
     * Also used as the key for tokens that were traded away but do not yet have a final recipient.
     */
    address internal constant _za = address(0);

    /**
     * Instance of IdentityRegistry interface.
     */
    IdentityRegistryInterface internal identity;

    /**
     * Maps trader address to the latest version of the coordinator contract the trader opted-in to.
     */
    mapping(address => uint256) public coordinatorVersionId;

    /**
     * Maps trader address to the latest version of the coordinator contract the trader opted-in to.
     */
    mapping(address => uint256) public minCoordinatorVersionId;

    /**
     * Maps trader address to token address to amount deposited.
     *
     * Tracks how much of each token a trader has deposited into this contract.
     */
    mapping(address => mapping(address => uint256)) public depositBalances;

    /**
     * Maps trader address to token address to total amount allocated to counter-parties.
     *
     * Tracks the total amount of each token a trader owes to other traders, as reported
     * by the settlement data providers.
     */
    mapping(address => mapping(address => uint256)) public allocated;

    /**
     * Maps deliverer address to recipient address to token address to obligation amount.
     *
     * Tracks how much of each token one trader owes another trader.
     * e.g. obligations[aliceAddress][bobAddress][_za] stores the amount of
     * the native token owed by alice to bob.
     */
    mapping(address => mapping(address => mapping(address => uint256))) public obligations;

    /**
     * Maps trader address to token address to most recent settlement ID
     *
     * Whenever a trader requests settlement for an asset, an identifier for that
     * settlement is stored in this mapping. This value can be compared with the
     * last settlement processed by SettlementCoordination in order to determine
     * the state of a trader's funds.
     */
    mapping(address => mapping(address => uint256)) public lastSettlementReqId;

    /**
     * Maps settlement ID to the block number at the time the settlement was requested.
     *
     * If, after a pre-determined amount of blocks mined, the settlement network fails to
     * report data, the trader will be able to withdraw funds. This prevents a trader's funds
     * from staying locked in the case of a catastrophic system failure.
     */
    mapping(uint256 => uint256) public settlementBlock;

    /**
     * Maps trader address to token address to collateralization status.
     *
     * Designates whether or not a trader's token balance is available for submitting trades
     * on a participating exchange.
     *
     * Defaults to false, meaning every trader starts collateralized for every token.
     * Switches to true when a trader requests settlement for a token.
     */
    mapping(address => mapping(address => bool)) public uncollateralized;

    /**
     * Emitted when a trader deposits an asset into this contract.
     */
    event Deposit(address wallet, uint256 amount, address token);

    /**
     * Emitted when a trader requests settlement for an asset.
     */
    event SettlementRequested(uint256 settlementID, address trader, address token);

    /**
     * Emitted when one trader receives obligated funds from another trader.
     */
    event RequestFundsOwed(address requester, address counterparty, address token);

    /**
     * Emitted when a trader withdraws an asset from this contract.
     */
    event Withdraw(address wallet, uint256 amount, address token);

    /**
     * Emitted when a trader collateralizes an asset for trading on participating exchanges.
     */
    event Collateralized(address wallet, address token);

    constructor(address _identity) {
        identity = IdentityRegistryInterface(_identity);
    }

    /**
     * Called by traders to deposit native token.
     */
    function deposit() external payable {
        depositBalances[msg.sender][_za] += msg.value;
        emit Deposit(msg.sender, msg.value, _za);
    }

    /**
     * Called by traders to deposit an approved ERC20 token.
     *
     * Trader must approve this contract as a spender before calling.
     *
     * @param amount Amount of token to be deposited
     * @param token  Address of the token
     */
    function depositToken(uint256 amount, address token) external {
        require(identity.isTradeableToken(token), "NOT_TRADEABLE_TOKEN");
        require(IERC20(token).transferFrom(msg.sender, address(this), amount), "TRANSFER_FAILED");
        depositBalances[msg.sender][token] += amount;
        emit Deposit(msg.sender, amount, token);
    }

    /**
     * Called by traders to request settlement for an asset.
     *
     * Emits an event detected off-chain by the settlement infrastructure to
     * initiate the settlement process.
     * After this point, the trader should not be able to use the asset's balance
     * for any trades on a participating exchange until collateralizing again.
     */
    function requestSettlement(address token) external returns (uint256) {
        require(
            coordinatorVersionId[msg.sender] + 1 == identity.getLatestCoordinatorVersion(),
            "TRADER_NOT_OPTED_IN"
        );
        require(!uncollateralized[msg.sender][token], "NOT_COLLATERALIZED");
        require(
            getCoordinator().lastSettlementIdProcessed() > lastSettlementReqId[msg.sender][token],
            "SETTLEMENT_IN_PROGRESS"
        );
        lastSettlementReqId[msg.sender][token] = getCoordinator().requestSettlement(token);
        uncollateralized[msg.sender][token] = true;
        emit SettlementRequested(lastSettlementReqId[msg.sender][token], msg.sender, token);
        return lastSettlementReqId[msg.sender][token];
    }

    /**
     * Called by the coordinator contract to record an obligation from one trader to a counter-party.
     *
     * Checks with the identity registry that the coordinator is valid.
     * Updates total amount of an asset owed by the deliverer.
     * Updates amount of an asset owed by the deliverer to the recipient.
     *
     * Note: When a trader requests settlement, any funds owed by that trader will be allocated to the
     *       zero address until the final recipient(s) request settlement for that asset.
     *       Funds owed to the trader will be available to request once settlement completes.
     *
     * @param deliverer Address of the trader that owes an asset to a counterparty
     * @param recipient Address of the trader owed by the deliverer
     * @param token Address of the owed token
     * @param amount Amount of the owed token
     */
    function writeObligation(
        address deliverer,
        address recipient,
        address token,
        uint256 amount
    ) external virtual returns (bool) {
        require(
            identity.isUnexpiredCoordinator(
                msg.sender,
                coordinatorVersionId[deliverer] + 1,
                minCoordinatorVersionId[deliverer] + 1
            ),
            "SENDER_NOT_VALID_COORD"
        );
        allocated[deliverer][token] += amount;
        obligations[deliverer][recipient][token] += amount;

        assert(depositBalances[deliverer][token] >= allocated[deliverer][token]);
        return true;
    }

    /**
     * Called by coordinator contract to reallocate a trader's obligation from zero address to a counter-party address.
     *
     * The total amount of the token owed by the trader remains the same.
     * Reduces amount trader owes to the zero address and increments amount owed to a counterparty.
     *
     * @param deliverer Address of the trader with tokens owed to the zero address as the result of a previous settlement
     * @param recipient Address of the trader owed by the deliverer
     * @param token Address of the owed token
     * @param amount Amount of the owed token
     */
    function reallocateObligation(
        address deliverer,
        address recipient,
        address token,
        uint256 amount
    ) external virtual returns (bool) {
        require(
            identity.isUnexpiredCoordinator(
                msg.sender,
                coordinatorVersionId[deliverer] + 1,
                minCoordinatorVersionId[deliverer] + 1
            ),
            "SENDER_NOT_VALID_COORD"
        );
        require(obligations[deliverer][_za][token] >= amount, "UNDEROBLIGATED");
        obligations[deliverer][_za][token] -= amount;
        obligations[deliverer][recipient][token] += amount;

        return true;
    }

    /**
     * Verifies that either the user's last settlement request has been processed
     * or that the settlement timeout interval has passed.
     *
     * Checked before trader is allowed to request funds owed or withdraw funds.
     *
     * If the settlement requested is not processed within a specified time frame, the user
     * should still be able to request funds already owed and withdraw.
     */
    function settlementProcessedOrTimedOut(uint256 reqId) internal {
        require(
            getCoordinator().lastSettlementIdProcessed() > reqId ||
                getCoordinator().getReqData(reqId).requestBlock + identity.unlockInterval() < block.number,
            "SETTLEMENT_IN_PROGRESS"
        );
    }

    /**
     * Called by traders to request the total amount of a token owed by a counterparty.
     *
     * Trader must not be collateralized when requesting from a counterparty.
     * Trader must not have an unfinished settlement for the asset being requested.
     * Asks counterparty to transfer total amount of a token owed to the trader.
     *
     * @param token Address of the token being requested
     * @param counterparty Address of the counterparty
     */
    function requestFundsOwed(address counterparty, address token) external {
        require(uncollateralized[msg.sender][token], "COLLATERALIZED");
        settlementProcessedOrTimedOut(lastSettlementReqId[msg.sender][token]);

        emit RequestFundsOwed(msg.sender, counterparty, token);
        releaseOwedFunds(counterparty, token);
    }

    /**
     * Called internally from requestFundsOwed to check how much of an asset a counterparty owes a trader
     * and update balances accordingly.
     *
     * Calls IdentityRegistry to calculate fee paid to the protocol as a percentage of the asset requested.
     * The fee is deducted from the total amount paid to the requesting trader.
     *
     * @param counterparty Address of user who's owed tokens are being requested
     * @param token Address of the token requested
     */
    function releaseOwedFunds(address counterparty, address token) internal {
        uint256 amountOwed = obligations[counterparty][msg.sender][token];
        require(amountOwed > 0, "NOTHING_OWED");

        obligations[counterparty][msg.sender][token] = 0;
        allocated[counterparty][token] -= amountOwed;
        depositBalances[counterparty][token] -= amountOwed;

        uint256 fee = identity.calculateFee(amountOwed);
        amountOwed -= fee;
        address feeRecipient = identity.feeRecipient();

        depositBalances[msg.sender][token] += amountOwed;
        depositBalances[feeRecipient][token] += fee;
    }

    /**
     * Called by traders to withdraw an asset from this contract.
     *
     * Checks that the trader is uncollateralized and has no settlement in progress
     * for the asset being withdrawn.
     * Checks that trader has a sufficient available balance of the asset. Any amount
     * allocated to counterparties can not be withdrawn.
     * Transfers the requested token (or native asset) from the contract to the trader.
     *
     * @param amount Amount of tokens to be withdrawn
     * @param token   Address of token to be withdrawn
     */
    function withdraw(uint256 amount, address token) external {
        require(uncollateralized[msg.sender][token], "COLLATERALIZED");
        settlementProcessedOrTimedOut(lastSettlementReqId[msg.sender][token]);

        require(
            depositBalances[msg.sender][token] - allocated[msg.sender][token] >= amount,
            "INSUFFICIENT_BALANCE"
        );

        depositBalances[msg.sender][token] -= amount;
        if (token == _za) {
            (bool success, ) = msg.sender.call{value: amount}("");
            require(success, "TRANSFER_FAILED");
        } else {
            require(IERC20(token).transfer(msg.sender, amount), "TRANSFER_FAILED");
        }
        emit Withdraw(msg.sender, amount, token);
    }

    /**
     * Called by traders to re-lock an asset for trading on participating exchanges after a settlement.
     */
    function collateralize(address token) external {
        require(uncollateralized[msg.sender][token], "COLLATERALIZED");
        emit Collateralized(msg.sender, token);
        uncollateralized[msg.sender][token] = false;
    }

    /**
     * Called by traders to opt-in to an upgrade of the coordinator.
     *
     * Cannot be called if the trader has a settlement in progress with the current coordinator.
     * Resets `lastSettlementReqId` since this user should not have any settlements with the new coordinator.
     *
     * TODO: opt-in upgradability needs to be reworked since changing to per-asset settlement
     */
    function upgradeCoordinator() external {
        // require(
        //     getCoordinator().lastSettlementIdProcessed() >
        //         lastSettlementReqId[msg.sender][token],
        //     "SETTLEMENT_IN_PROGRESS"
        // );
        uint256 latestId = identity.getLatestCoordinatorVersion();
        require(coordinatorVersionId[msg.sender] + 1 < latestId, "COORD_UP_TO_DATE");

        coordinatorVersionId[msg.sender] = latestId - 1;
        // lastSettlementReqId[msg.sender][token] = 0;
    }

    /**
     * Returns the latest coordination contract the trader has opted in to.
     */
    function getCoordinator() internal view returns (CoordinationInterface) {
        return CoordinationInterface(identity.getCoordinator(coordinatorVersionId[msg.sender]));
    }
}
