// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./Interfaces/IdentityRegistryInterface.sol";
import "./Interfaces/CoordinationInterface.sol";
import "./Interfaces/ConsensusInterface.sol";
import "./Interfaces/CollateralInterface.sol";
import "./MerkleTreeGeneration.sol";

/**
 * SettlementDataConsensus allows Settlement Data Provider (SDP) operators to report obligations
 * for settlements.
 */
contract SettlementDataConsensus is ConsensusInterface, MerkleTreeGeneration {
    /**
     * All Settlements start in the Requested stage and end in the Cleared stage.
     * In between, there may be a dispute that must be resolved.
     */
    enum SettlementStage {
        Requested,
        QuorumReached,
        Cleared,
        ExchangeDisputed,
        QuorumDisputed
    }

    /**
     * Stores the necessary data to come to consensus on a settlement.
     */
    struct Settlement {
        SettlementLib.Obligation[] obligations;
        uint256 quorum;
        SettlementStage stage;
        address[] reporters;
        bytes32 merkleRoot;
        mapping(address => bytes32) disputeRoots;
    }

    /**
     * Defines what the exchange needs to report in order to clear a settlement or
     * raise a dispute.
     */
    struct ExchangeReport {
        bytes32 merkleRoot;
        uint256 settlementId;
        address coordinator;
    }

    /**
     * Maps obligations IDs to data for that settlement.
     */
    mapping(bytes32 => Settlement) public settlements;

    /**
     * Minimum number of SDPs that must report and concur for a settlement
     * to reach quorum.
     */
    uint256 public minQuorum = 3;

    /**
     * Instance of IdentityRegistry interface.
     */
    IdentityRegistryInterface internal identity;

    /**
     * Emitted when an SDP reports obligations for a settlement.
     */
    event ObligationsReported(address coordinator, uint256 id, address sdp);

    /**
     * Emitted when a participating exchange reports obligations that conflict with the quorum.
     */
    event SettlementDisputedExchange(uint256 settlementId);

    /**
     * Emitted when a participating exchange reports obligations that concurs with the quorum.
     */
    event SettlementConcurredExchange(uint256 settlementId);

    /**
     * Emitted when SDPs report conflicting obligations.
     */
    event SettlementDisputedQuorum(uint256 settlementId);

    /**
     * Emitted when a participating exchange reports obligations to settle a dispute between SDPs.
     */
    event QuorumDisputeSettledExchange(uint256 settlementId, bytes32 correctRoot);

    constructor(address _identity) {
        identity = IdentityRegistryInterface(_identity);
    }

    /**
     * Called by SDP operators to report obligations for a settlement.
     *
     * Checks that the reporter has a corresponding administrator registered in the collateral custody contract.
     * Checks that the settlement ID is the next to be processed.
     * Attempts to lock the proper amount of the SDP administrator's deposited asset to collateralize the settlement.
     * Checks if the SDP has already reported for this settlement.
     * Verifies that the reported merkle root matches the root of the tree constructed from the obligations.
     * If other SDPs have already reported, compares the merkle root reported by this SDP with previous ones.
     *
     * If this report brings the settlement to a quorum, then the function will pass the obligations on to the
     * SettlementCoordination contract which will perform additional checks and then update the balances in AssetCustody.
     *
     * @param coordinator   Address of coordinator that settlement request was made from. Important for Coordinator version handling.
     * @param settlementId  Id of requested settlement the obligations coorespond to
     * @param reportedObligations   Array of obligations, which are a struct that fully describes an obligation.
     * @param merkleRoot Root of the merkle tree constructed from the obligations
     */
    function reportSettlementObligations(
        address coordinator,
        uint256 settlementId,
        SettlementLib.Obligation[] memory reportedObligations,
        bytes32 merkleRoot
    ) external {
        require(
            CollateralInterface(identity.getLatestCollateralCustody()).adminFor(msg.sender) != address(0),
            "NOT_APPROVED_OPERATOR"
        );
        require(
            settlementId == CoordinationInterface(coordinator).lastSettlementIdProcessed() + 1 &&
                settlementId < CoordinationInterface(coordinator).nextRequestId(),
            "INVALID_ID"
        );

        bytes32 obligationsId = getObligationsId(coordinator, settlementId);

        lockCollateral(obligationsId, reportedObligations, msg.sender);

        require(!hasReported(obligationsId, msg.sender), "ALREADY_REPORTED");

        if (settlements[obligationsId].quorum == 0) {
            bytes32 calculatedMerkleRoot = verifyMerkleTree(reportedObligations);
            require(calculatedMerkleRoot == merkleRoot, "MERKLE_DOES_NOT_MATCH_OBLIGATIONS");
            settlements[obligationsId].merkleRoot = calculatedMerkleRoot;
            for (uint256 i = 0; i < reportedObligations.length; i++) {
                settlements[obligationsId].obligations.push(reportedObligations[i]);
            }
        } else {
            if (merkleRoot != settlements[obligationsId].merkleRoot) {
                bytes32 calculatedMerkleRoot = verifyMerkleTree(reportedObligations);
                require(calculatedMerkleRoot == merkleRoot, "MERKLE_DOES_NOT_MATCH_OBLIGATIONS");
                settlements[obligationsId].stage = SettlementStage.QuorumDisputed;
                /**
                 *  @dev Important note for future development:
                 *  For now when a dispute occurs, the dispute merkle root will simply be mapped to the dispute address.
                 *  This will allow for multiple dispute roots as well as multiple disputers, and even a large quorum expansion
                 *  to resolve a dispute if this is desired.
                 *  The important idea that the settlements.reporters[] array will be used as the location for finding who reported
                 *  which merkle root. settlements.disputeRoots will be used to store their respective roots
                 *  Any reporter with an empty or zero value in the mapping will be assumed to have reported settlements.merkleRoot or
                 *  to concurr with the first merkle root.
                 *  This makes it so that settlements.disputeRoots will only be used in the event of a dispute without losing information
                 *
                 *  This note can be reduced or deleted once this functionality is built out
                 */
                settlements[obligationsId].disputeRoots[msg.sender] = merkleRoot;
                settlements[obligationsId].reporters.push(msg.sender);
                emit SettlementDisputedQuorum(settlementId);
                return;
            }
            require(
                compareObligations(reportedObligations, settlements[obligationsId].obligations),
                "MERKLE_DOES_NOT_MATCH_OBLIGATIONS"
            );
        }

        settlements[obligationsId].reporters.push(msg.sender);
        settlements[obligationsId].quorum++;
        // TODO: This event emit still needs a test case
        emit ObligationsReported(coordinator, settlementId, msg.sender);
        if (settlements[obligationsId].quorum >= minQuorum) {
            CoordinationInterface(coordinator).reportObligations(
                settlementId,
                settlements[obligationsId].obligations
            );
            settlements[obligationsId].stage = SettlementStage.QuorumReached;
        }
    }

    /**
     * Called by an auditor to report that a participating exchange concurs
     * with the result of the SDP quorum for a settlement.
     *
     * Verifies that the data reported was actually signed by the exchange.
     * Verifies that the root matches with the one reported by the quorum.
     * Unlocks collateral provided by SDPs for this settlement.
     *
     * @param exchangeReport Message body of the participating exchange's signature
     * @param v                 v parameter in Ethereum signature
     * @param r                 r parameter in Ethereum signature
     * @param s                 s parameter in Ethereum signature
     */
    function reportExchangeMerkleRootConcur(
        ExchangeReport calldata exchangeReport,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external {
        require(identity.isAuditor(msg.sender), "SENDER_NOT_AUDITOR");
        require(verifyExchangeSignature(exchangeReport, v, r, s), "SIGNATURE_VERIF_FAILED_EXCHANGE");

        bytes32 obligationsId = getObligationsId(exchangeReport.coordinator, exchangeReport.settlementId);
        // Settlement must be in the correct state.
        require(
            settlements[obligationsId].stage == SettlementStage.QuorumReached,
            "INVALID_SETTLEMENT_STATE"
        );

        // Compare exchange root with quorum root
        bytes32 quorumRoot = settlements[obligationsId].merkleRoot;

        // Case: Exchange concurs with quorum
        require(quorumRoot == exchangeReport.merkleRoot, "EXCHANGE_ROOT_DOES_NOT_MATCH_REPORTED_OBLIGATIONS");
        // Mark settlement as cleared and release collateral
        settlements[obligationsId].stage = SettlementStage.Cleared;

        emit SettlementConcurredExchange(exchangeReport.settlementId);

        unlockCollateral(
            obligationsId,
            settlements[obligationsId].obligations,
            settlements[obligationsId].reporters
        );
    }

    /**
     * Called by an auditor to report that a participating exchange disagrees
     * with the result of the SDP quorum for a settlement.
     *
     * Verifies that the data reported was actually signed by the exchange.
     * Verifies that the root DOES NOT match with the one reported by the quorum.
     * Transitions the settlement into a disputed state.
     *
     * @param reportedObligations Participating exchange's understanding of the obligations
     * @param exchangeReport Message body of the participating exchange's signature
     * @param v                 v parameter in Ethereum signature
     * @param r                 r parameter in Ethereum signature
     * @param s                 s parameter in Ethereum signature
     */
    function reportExchangeMerkleRootDispute(
        SettlementLib.Obligation[] memory reportedObligations,
        ExchangeReport calldata exchangeReport,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external {
        require(identity.isAuditor(msg.sender), "SENDER_NOT_AUDITOR");
        require(verifyExchangeSignature(exchangeReport, v, r, s), "SIGNATURE_VERIF_FAILED_EXCHANGE");

        bytes32 obligationsId = getObligationsId(exchangeReport.coordinator, exchangeReport.settlementId);
        // Settlement must be in the correct state.
        require(
            settlements[obligationsId].stage == SettlementStage.QuorumReached,
            "INVALID_SETTLEMENT_STATE"
        );

        // Compare exchange root with quorum root
        bytes32 quorumRoot = settlements[obligationsId].merkleRoot;

        require(quorumRoot != exchangeReport.merkleRoot, "EXCHANGE_ROOT_MATCHES_REPORTED_OBLIGATIONS");
        // Case: Exchange disagrees with quorum
        // Mark settlement as disputed and emit event
        require(
            verifyMerkleTree(reportedObligations) == exchangeReport.merkleRoot,
            "EXCHANGE_ROOT_DOES_NOT_MATCH_EXCHANGE_OBLIGATIONS"
        );
        settlements[obligationsId].stage = SettlementStage.ExchangeDisputed;
        emit SettlementDisputedExchange(exchangeReport.settlementId);
    }

    /**
     * Called by an auditor to report the participating exchange's
     * understanding of a settlement in order to resolve a dispute between
     * the SDP quorum.
     *
     * Verifies that the data reported was actually signed by the exchange.
     * Determines which SDPs should be punished for reporting the incorrect obligations.
     *
     * @param reportedObligations Participating exchange's understanding of the obligations
     * @param exchangeReport Message body of the participating exchange's signature
     * @param v                 v parameter in Ethereum signature
     * @param r                 r parameter in Ethereum signature
     * @param s                 s parameter in Ethereum signature
     */
    function reportMerkleRootQuorumDisputed(
        SettlementLib.Obligation[] memory reportedObligations,
        ExchangeReport calldata exchangeReport,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external returns (bool[] memory) {
        require(identity.isAuditor(msg.sender), "SENDER_NOT_AUDITOR");
        require(verifyExchangeSignature(exchangeReport, v, r, s), "SIGNATURE_VERIF_FAILED_EXCHANGE");

        bytes32 obligationsId = getObligationsId(exchangeReport.coordinator, exchangeReport.settlementId);

        require(
            settlements[obligationsId].stage == SettlementStage.QuorumDisputed,
            "INVALID_SETTLEMENT_STATE"
        );

        bytes32 quorumRoot = settlements[obligationsId].merkleRoot;
        bool[] memory rewardOrSlash = new bool[](settlements[obligationsId].reporters.length);

        // quorumRoot becomes the standard "correct root" for comparing all reporters disputeRoots mapping to.
        // set to zero if equal to exchangeReport, because these reporters will have an empty value in the mapping
        if (quorumRoot == exchangeReport.merkleRoot) {
            quorumRoot = 0;
        } else {
            quorumRoot = exchangeReport.merkleRoot;
        }

        for (uint256 ii = 0; ii < settlements[obligationsId].reporters.length; ii++) {
            if (
                settlements[obligationsId].disputeRoots[settlements[obligationsId].reporters[ii]] ==
                quorumRoot
            ) {
                rewardOrSlash[ii] = true;
            }
        }
        emit QuorumDisputeSettledExchange(exchangeReport.settlementId, exchangeReport.merkleRoot);
        //TODO: after stake is slashed and obligations are reported and settled in the Coordination and Asset Custody
        //      We will have to change the settlement state to "Cleared" until we do this, it will be possible to report
        //      An exchange root more than once.
        return rewardOrSlash;
    }

    /**
     * Calls the CollateralCustody contract to lock a reporting SDP's
     * asset in order to collateralize a settlement.
     *
     * @param obligationsId ID of the settlement in question
     * @param reportedObligations Obligations reported by the SDP
     * @param reporter Address of the SDP that must lock collateral
     */
    function lockCollateral(
        bytes32 obligationsId,
        SettlementLib.Obligation[] memory reportedObligations,
        address reporter
    ) internal {
        uint256 obligationAmount = 0;
        for (uint256 ii = 0; ii < reportedObligations.length; ii++) {
            obligationAmount += reportedObligations[ii].amount;
        }
        obligationAmount = identity.calculateCollateral(obligationAmount);
        CollateralInterface(identity.getLatestCollateralCustody()).lockCollateral(
            reporter,
            reportedObligations[0].token,
            obligationAmount
        );
        CollateralInterface(identity.getLatestCollateralCustody()).lockCollateral(
            reporter,
            identity.getLatestProtocolToken(),
            identity.protocolStakeAmount()
        );
    }

    /**
     * Calls the CollateralCustody contract to unlock a reporting SDP's
     * asset after a settlement has cleared.
     *
     * @param obligationsId ID of the settlement in question
     * @param reportedObligations Obligations reported by the SDP
     * @param reporters Addresses of the SDPs who's collateral must be released
     */
    function unlockCollateral(
        bytes32 obligationsId,
        SettlementLib.Obligation[] memory reportedObligations,
        address[] memory reporters
    ) internal {
        uint256 obligationAmount = 0;
        for (uint256 ii = 0; ii < settlements[obligationsId].obligations.length; ii++) {
            obligationAmount += settlements[obligationsId].obligations[ii].amount;
        }
        obligationAmount = identity.calculateCollateral(obligationAmount);
        for (uint256 ii = 0; ii < settlements[obligationsId].reporters.length; ii++) {
            CollateralInterface(identity.getLatestCollateralCustody()).unlockCollateral(
                reporters[ii],
                reportedObligations[0].token,
                obligationAmount
            );
            CollateralInterface(identity.getLatestCollateralCustody()).unlockCollateral(
                reporters[ii],
                identity.getLatestProtocolToken(),
                identity.protocolStakeAmount()
            );
        }
    }

    /**
     * Compares two sets of obligations.
     */
    function compareObligations(
        SettlementLib.Obligation[] memory reported,
        SettlementLib.Obligation[] memory existing
    ) public pure returns (bool) {
        return keccak256(abi.encode(reported)) == keccak256(abi.encode(existing));
    }

    /**
     * Calculates the index for a settlement.
     */
    function getObligationsId(address coordinator, uint256 settlementId) public pure returns (bytes32) {
        return keccak256(abi.encode(coordinator, settlementId));
    }

    /**
     * Returns the first set of obligations that were reported for a settlement.
     */
    function getReportedObligations(bytes32 id) external view returns (SettlementLib.Obligation[] memory) {
        return settlements[id].obligations;
    }

    /**
     * Checks if an SDP has already reported for a settlement.
     */
    function hasReported(bytes32 id, address sdp) public view returns (bool) {
        bool returnBool = false;
        for (uint256 ii = 0; ii < settlements[id].reporters.length; ii++) {
            if (settlements[id].reporters[ii] == sdp) {
                returnBool = true;
            }
        }
        return returnBool;
    }

    /**
     * Calculates the merkle root of a tree constructed from a set of obligations.
     *
     * Each leaf of the tree is the hash of the encoded contents of the obligation.
     */
    function verifyMerkleTree(SettlementLib.Obligation[] memory obligations) public pure returns (bytes32) {
        bytes32[] memory leaves = new bytes32[](obligations.length);
        for (uint256 ii = 0; ii < obligations.length; ii++) {
            leaves[ii] = keccak256(
                abi.encodePacked(
                    obligations[ii].amount,
                    obligations[ii].deliverer,
                    obligations[ii].recipient,
                    obligations[ii].token,
                    obligations[ii].reallocate
                )
            );
        }
        return getMerkleRoot(leaves);
    }

    /**
     * Verifies that a signature matches the data in exchange report
     * and was signed by the exchange's key.
     */
    function verifyExchangeSignature(
        ExchangeReport calldata exchangeReport,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) internal view returns (bool) {
        bytes32 message = keccak256(
            abi.encodePacked(
                "\x19Ethereum Signed Message:\n32",
                keccak256(
                    abi.encodePacked(
                        exchangeReport.merkleRoot,
                        exchangeReport.settlementId,
                        exchangeReport.coordinator
                    )
                )
            )
        );
        return ecrecover(message, v, r, s) == identity.getGovernanceAddress();
    }
}
