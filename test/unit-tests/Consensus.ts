// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Signer, BigNumber} from "ethers"

import {MerkleTree} from "merkletreejs"
import {
    copyArray,
    deploy,
    getSigner,
    IdentityRegistry,
    Obligation,
    ExchangeReport,
    DummyCoin,
    ProtocolToken,
    SettlementCoordination,
    SettlementDataConsensus,
    AssetMock,
    CollateralMock,
    CoordinationMock,
    setupTest,
    _za,
    approveAndDeposit,
    operatorInit,
    merkleFromObs,
} from "../utils"

describe("SettlementDataConsensus", function () {
    let coordination: CoordinationMock
    let assetCustodyMock: AssetMock
    let collateralCustodyMock: CollateralMock
    let identity: IdentityRegistry
    let noRole: Address

    let alice: Signer
    let bob: Signer
    let charlie: Signer
    let sdps: Signer[] = []
    let auditor: Signer
    let exchange: Signer

    let sdpAdmin1: Signer
    let sdpAdmin2: Signer
    let sdpAdmin3: Signer
    let sdpAdmin4: Signer
    let sdpOperator1: Signer
    let sdpOperator2: Signer
    let sdpOperator3: Signer
    let sdpOperator4: Signer

    let aliceAddr: string
    let bobAddr: string
    let charlieAddr: string

    let sdpAdmin1Address: Address
    let sdpAdmin2Address: Address
    let sdpAdmin3Address: Address
    let sdpAdmin4Address: Address
    let sdpOperator1Address: Address
    let sdpOperator2Address: Address
    let sdpOperator3Address: Address
    let sdpOperator4Address: Address

    let factory: Signer
    let unnamedAccounts: Address[]
    let consensus: SettlementDataConsensus
    let dummyCoin: DummyCoin
    let protocolToken: ProtocolToken

    let assetCustodyAddress: Address
    let collateralCustodyAddress: Address
    const zeroEth = ethers.utils.parseEther("0")
    const ethObligation = ethers.utils.parseEther("1")
    let ethCollateral = zeroEth
    let dummyCollateral = 0
    // 110% stake required for obligation reporting
    const collateralMultiplier = [110, 100]
    let stakeAmount = 10000
    let dummyDeposit = 1000

    const merkleRootZero = "0x0000000000000000000000000000000000000000000000000000000000000000"
    const bytes32Zero = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let fakeMerkleRoot = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let ethersMerkleRoot = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let merkleRootUnmatched = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let randomRoot = ethers.utils.hexlify(ethers.utils.randomBytes(32))

    let auditorRole: string
    let id: string
    let quorumCount: BigNumber

    let settlementId: BigNumber

    let message: string
    let signedMessage: string
    let signature: string
    let r: string
    let s: string
    let v: number

    let exReport: ExchangeReport

    let obs: Obligation[]
    let unmatchedObs: Obligation[]
    let bigObligations: Obligation[]
    let fakeObligations: Obligation[]

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        unnamedAccounts = await getUnnamedAccounts()
        alice = await getSigner(namedAccounts.alice)
        bob = await getSigner(namedAccounts.bob)
        auditor = await getSigner(namedAccounts.auditor)
        exchange = await getSigner(namedAccounts.exchange)
        charlie = await getSigner(namedAccounts.bob)
        factory = await getSigner(unnamedAccounts[14])
        sdps.push(
            await getSigner(namedAccounts.sdpOperator1),
            await getSigner(namedAccounts.sdpOperator2),
            await getSigner(namedAccounts.sdpOperator3)
        )

        sdpAdmin1 = await ethers.provider.getSigner(namedAccounts.sdpAdmin1)
        sdpAdmin2 = await ethers.provider.getSigner(namedAccounts.sdpAdmin2)
        sdpAdmin3 = await ethers.provider.getSigner(namedAccounts.sdpAdmin3)
        sdpAdmin4 = await ethers.provider.getSigner(namedAccounts.sdpAdmin4)
        sdpOperator1 = await ethers.provider.getSigner(namedAccounts.sdpOperator1)
        sdpOperator2 = await ethers.provider.getSigner(namedAccounts.sdpOperator2)
        sdpOperator3 = await ethers.provider.getSigner(namedAccounts.sdpOperator3)
        sdpOperator4 = await ethers.provider.getSigner(namedAccounts.sdpOperator4)

        sdpAdmin1Address = await sdpAdmin1.getAddress()
        sdpAdmin2Address = await sdpAdmin2.getAddress()
        sdpAdmin3Address = await sdpAdmin3.getAddress()
        sdpAdmin4Address = await sdpAdmin4.getAddress()
        sdpOperator1Address = await sdpOperator1.getAddress()
        sdpOperator2Address = await sdpOperator2.getAddress()
        sdpOperator3Address = await sdpOperator3.getAddress()
        sdpOperator4Address = await sdpOperator4.getAddress()

        noRole = namedAccounts.noRole
        aliceAddr = await alice.getAddress()
        bobAddr = await bob.getAddress()
        charlieAddr = await charlie.getAddress()
        // very large obligation array for testing
    })

    beforeEach(async function () {
        let contracts = await setupTest()
        identity = contracts.identity
        coordination = contracts.coordinationMock
        assetCustodyMock = contracts.assetMock
        collateralCustodyMock = contracts.collateralMock
        consensus = contracts.consensus

        dummyCoin = contracts.dummyCoin
        protocolToken = contracts.protocolToken

        assetCustodyAddress = assetCustodyMock.address
        collateralCustodyAddress = collateralCustodyMock.address

        // await identity.initializeRole(
        //     await contracts.library.callStatic.ROLE_SDP_MASTER(),
        //     await factory.getAddress()
        // )
        // for (let sdp of sdps) {
        //     await identity.connect(factory).grantSDP(await sdp.getAddress())
        // }
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_ASSET_CUSTODY(),
            await assetCustodyAddress
        )
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_COLLATERAL_CUSTODY(),
            await collateralCustodyAddress
        )
        await identity.grantRole(
            await contracts.library.callStatic.ROLE_AUDITOR(),
            await auditor.getAddress()
        )

        bigObligations = [
            {
                amount: 1532,
                recipient: aliceAddr,
                deliverer: bobAddr,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 784,
                recipient: bobAddr,
                deliverer: charlieAddr,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 55,
                recipient: sdpAdmin1Address,
                deliverer: charlieAddr,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 44,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 55,
                recipient: charlieAddr,
                deliverer: aliceAddr,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 77,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 347,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 135,
                recipient: charlieAddr,
                deliverer: sdpAdmin1Address,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 1251,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 347,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dummyCoin.address,
                reallocate: false,
            },
            {
                amount: 1461,
                recipient: charlieAddr,
                deliverer: sdpAdmin1Address,
                token: dummyCoin.address,
                reallocate: false,
            },
        ]
    })
    // it("forbids non-sdp from reporting obligations", async () => {
    //   obs = [
    //     {
    //       amount: ethObligation,
    //       recipient: aliceAddr,
    //       deliverer: bobAddr,
    //       token: ethers.constants.AddressZero,
    //       reallocate: false,
    //     },
    //   ]
    //   let settlementId = await coordination.callStatic.requestSettlement(_za)
    //   await coordination.requestSettlement(_za)

    //   await expect(
    //     consensus
    //       .connect(alice)
    //       .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
    //   ).to.be.revertedWith("SDP")
    // })
    describe("after a settlement is requested", async () => {
        beforeEach(async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            await identity.setStakeAmount(stakeAmount)

            // deposit collateral to sdp admins and set admin - operator relationships
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)

            // deposit protocol token collateral to sdp admins
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, stakeAmount * 10)
            // deposit native token collateral for sdp admins
            await collateralCustodyMock.connect(sdpAdmin1).deposit({value: ethObligation.mul(10)})
            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: ethObligation.mul(10)})
            await collateralCustodyMock.connect(sdpAdmin3).deposit({value: ethObligation.mul(10)})
        })

        it("forbids reporting for an invalid settlement id", async () => {
            obs = [
                {
                    amount: ethObligation,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]

            ethersMerkleRoot = await merkleFromObs(obs)
            await expect(
                consensus
                    .connect(sdps[0])
                    .reportSettlementObligations(
                        coordination.address,
                        settlementId.sub(1),
                        obs,
                        ethersMerkleRoot
                    )
            ).to.be.revertedWith("INVALID_ID")
            await expect(
                consensus
                    .connect(sdps[0])
                    .reportSettlementObligations(
                        coordination.address,
                        settlementId.add(1),
                        obs,
                        ethersMerkleRoot
                    )
            ).to.be.revertedWith("INVALID_ID")
        })

        it("stores obligation when SDP reports", async () => {
            obs = [
                {
                    amount: ethObligation,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            await consensus
                .connect(sdps[0])
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)

            const id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
            const {quorum, stage} = await consensus.callStatic.settlements(id)
            expect(quorum).to.be.eq(1)
            expect(stage).to.be.eq(0)
            const reportedObs = await consensus.callStatic.getReportedObligations(id)
            expect(reportedObs[0].recipient).to.be.eq(obs[0].recipient)
            expect(reportedObs[0].deliverer).to.be.eq(obs[0].deliverer)
            expect(reportedObs[0].amount).to.be.eq(obs[0].amount)
            expect(reportedObs[0].token).to.be.eq(obs[0].token)
        })

        it("forbids same SDP from reporting twice", async () => {
            obs = [
                {
                    amount: ethObligation,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            await consensus
                .connect(sdps[0])
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            await consensus
                .connect(sdps[1])
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)

            await expect(
                consensus
                    .connect(sdps[0])
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("ALREADY_REPORTED")
            await expect(
                consensus
                    .connect(sdps[1])
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("ALREADY_REPORTED")
        })

        it("writes to coordination once quorum is reached", async () => {
            const lastIdProcessed = await coordination.callStatic.lastSettlementIdProcessed()

            obs = [
                {
                    amount: ethObligation,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            for (let sdp of sdps) {
                await consensus
                    .connect(sdp)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            }
            const id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
            const {quorum, stage} = await consensus.callStatic.settlements(id)
            expect(quorum).to.be.eq(3)
            expect(stage).to.be.eq(1)
            expect(await coordination.callStatic.lastSettlementIdProcessed()).to.be.eq(lastIdProcessed.add(1))
        })

        it("forbids reporting if obligations were already written", async () => {
            obs = [
                {
                    amount: ethObligation,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            for (let sdp of sdps) {
                await consensus
                    .connect(sdp)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            }
            await expect(
                consensus
                    .connect(sdps[0])
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INVALID_ID")
        })
    })

    it("compares obligations correctly", async () => {
        const testCompare = async (a: Obligation[], b: Obligation[], match: boolean = true) => {
            expect(await consensus.callStatic.compareObligations(a, b)).to.be.eq(match)
        }

        const a: Obligation[] = [
            {
                amount: ethObligation,
                recipient: aliceAddr,
                deliverer: bobAddr,
                token: ethers.constants.AddressZero,
                reallocate: false,
            },
            {
                amount: ethObligation,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: noRole,
                reallocate: false,
            },
        ]
        let b: Obligation[] = copyArray(a)

        await testCompare(a, b)

        b[1].token = ethers.constants.AddressZero

        await testCompare(a, b, false)

        b = copyArray(a)
        b[1].amount = ethers.utils.parseEther("0.99")

        await testCompare(a, b, false)

        b = [a[1], a[0]]

        await testCompare(a, b, false)

        b = copyArray(a)
        b.push({
            amount: ethers.utils.parseEther("0.5"),
            recipient: aliceAddr,
            deliverer: bobAddr,
            token: noRole,
            reallocate: false,
        })

        await testCompare(a, b, false)
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Test Reporting Obligations", async () => {
        beforeEach(async () => {
            obs = [
                {
                    amount: ethObligation,
                    recipient: bobAddr,
                    deliverer: aliceAddr,
                    token: _za,
                    reallocate: false,
                },
                {
                    amount: ethObligation.mul(2),
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: _za,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            await collateralCustodyMock.connect(sdpAdmin1).deposit({value: ethObligation.mul(10)})
            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: ethObligation.mul(10)})
            await collateralCustodyMock.connect(sdpAdmin3).deposit({value: ethObligation.mul(10)})

            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)
        })

        it("Non SDP Operator cannot report obligations", async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)

            await expect(
                consensus
                    .connect(bob)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("NOT_APPROVED_OPERATOR")

            await expect(
                consensus
                    .connect(sdpAdmin1)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("NOT_APPROVED_OPERATOR")
        })

        it("SDP Operator can report obligations", async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted

            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
        })

        it("reporting obligations writes them to consensus", async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            await consensus
                .connect(sdpOperator1)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)

            const id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
            const {quorum, stage} = await consensus.callStatic.settlements(id)
            expect(quorum).to.be.eq(1)
            expect(stage).to.be.eq(0)

            const reportedObs = await consensus.callStatic.getReportedObligations(id)
            expect(reportedObs[0].recipient).to.be.eq(obs[0].recipient)
            expect(reportedObs[0].deliverer).to.be.eq(obs[0].deliverer)
            expect(reportedObs[0].amount).to.be.eq(obs[0].amount)
            expect(reportedObs[0].token).to.be.eq(obs[0].token)

            expect(reportedObs[1].recipient).to.be.eq(obs[1].recipient)
            expect(reportedObs[1].deliverer).to.be.eq(obs[1].deliverer)
            expect(reportedObs[1].amount).to.be.eq(obs[1].amount)
            expect(reportedObs[1].token).to.be.eq(obs[1].token)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing Protocol Token Staking", async () => {
        // note for understanding token staking tests:
        // SDP Admins (sdpAdmin#) hold tokens for staking as collateral
        // SDP Operators (sdpOperator#) handle obligation reporting

        beforeEach(async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            await identity.setStakeAmount(stakeAmount)

            // deposit collateral to sdp admins and set admin - operator relationships
            // set admin - operator link   (don't need to submit valid proof in mock contract)
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)

            // deposit protocol token collateral to sdp1(2x stake) and sdp2 (1x stake) (no collateral for sdp3)
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount * 2)
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount)

            // make sure enough native token staked for collateral
            await collateralCustodyMock.connect(sdpAdmin1).deposit({value: ethObligation.mul(10)})
            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: ethObligation.mul(10)})
            await collateralCustodyMock.connect(sdpAdmin3).deposit({value: ethObligation.mul(10)})

            // obligation
            obs = [
                {
                    amount: ethObligation,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)
        })

        it("Forbids SDP with insufficient stake from reporting Obligations (case: no locked tokens)", async () => {
            //sdpAdmin3 has no staked protocol tokens
            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
            // give sdpAdmin3 1 less than required token stake

            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, stakeAmount - 1)
            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")

            // complete sdpAdmin3 stake requiremnt for last check
            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, 1)

            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
        })
        it("Allows SDP with sufficient PTC stake to report obligations (case: no locked tokens)", async () => {
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
        })

        it("Successful obligation report locks tokens in sdp custody account", async () => {
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted

            expect(
                await collateralCustodyMock.lockedBalances(sdpAdmin2Address, protocolToken.address)
            ).to.be.eq(stakeAmount)
        })

        it("Forbids SDP with insufficient stake from reporting Obligations, Allows Obligation report with sufficient stake (case: locked tokens)", async () => {
            // sdpAdmin2 starts the exact required amount of protocol tokens to stake once
            // lock tokens for first run
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
            expect(
                await collateralCustodyMock.lockedBalances(sdpAdmin2Address, protocolToken.address)
            ).to.be.eq(stakeAmount)
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
            // give sdpAdmin2 1 less than required token stake
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount - 1)

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")

            // complete sdpAdmin2 stake requiremnt for last check
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, 1)
            // complete quorum to allow new obligation report
            await consensus
                .connect(sdpOperator1)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, stakeAmount)
            await consensus
                .connect(sdpOperator3)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            //confirm second report now possible
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
        })
        expect(await collateralCustodyMock.lockedBalances(sdpAdmin2Address, protocolToken.address)).to.be.eq(
            stakeAmount * 2
        )
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing Collateral Token Staking", async () => {
        // note for understanding token staking tests:
        // SDP Admins (sdpAdmin#) hold tokens for staking as collateral
        // SDP Operators (sdpOperator#) handle obligation reporting

        beforeEach(async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            await identity.setStakeAmount(stakeAmount)
            ethCollateral = ethObligation.mul(collateralMultiplier[0]).div(collateralMultiplier[1])
            // deposit collateral to sdp admins and set admin - operator relationships

            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)

            // Deposit full Protocol Token Collateral
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, stakeAmount * 10)

            // deposit native token collateral to sdp1(2x stake) and sdp2 (1x stake) (no collateral for sdp3)
            await collateralCustodyMock.connect(sdpAdmin1).deposit({value: ethCollateral.mul(2)})
            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: ethCollateral})

            // deposit dummyCoin collateral to sdp1(2x stake) and sdp2 (1x stake) (no collateral for sdp3)
            await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustodyMock, dummyDeposit * 2)
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustodyMock, dummyDeposit)

            // obligation
            obs = [
                {
                    amount: ethObligation.div(2),
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
                {
                    amount: ethObligation.div(2),
                    recipient: bobAddr,
                    deliverer: aliceAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)
        })

        it("Forbids SDP with insufficient stake from reporting Obligations (case: no locked tokens)", async () => {
            //sdpAdmin3 has no staked protocol tokens
            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
            // give sdpAdmin3 1 less than required token stake

            await collateralCustodyMock.connect(sdpAdmin3).deposit({value: ethCollateral.sub(1)})

            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")

            // complete sdpAdmin3 stake requiremnt for last check

            await collateralCustodyMock.connect(sdpAdmin3).deposit({value: 1})

            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
        })
        it("Allows SDP with sufficient PTC stake to report obligations (case: no locked tokens)", async () => {
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
        })

        it("Successful obligation report locks tokens in sdp custody account", async () => {
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted

            expect(await collateralCustodyMock.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(ethCollateral)
        })

        it("Forbids SDP with insufficient stake from reporting Obligations, Allows Obligation report with sufficient stake (case: locked tokens)", async () => {
            // sdpAdmin2 starts the exact required amount of protocol tokens to stake once
            // lock tokens for first run
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
            expect(await collateralCustodyMock.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(ethCollateral)

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
            // give sdpAdmin2 1 less than required token stake

            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: ethCollateral.sub(1)})
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")

            // complete sdpAdmin2 stake requiremnt for last check

            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: 1})

            // complete quorum to allow new obligation report
            await consensus
                .connect(sdpOperator1)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            await collateralCustodyMock.connect(sdpAdmin3).deposit({value: ethCollateral})
            await consensus
                .connect(sdpOperator3)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            //confirm second report now possible
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
            expect(await collateralCustodyMock.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(
                ethCollateral.mul(2)
            )
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("One full test of collateral locking with ERC20 token collateral", async () => {
        it("One full test of collateral locking with ERC20 token collateral", async () => {
            // sdpAdmin2 starts the exact required amount of protocol tokens to stake once
            // lock tokens for first run
            settlementId = await coordination.callStatic.requestSettlement(dummyCoin.address)
            await coordination.requestSettlement(dummyCoin.address)
            await identity.setStakeAmount(stakeAmount)
            dummyCollateral = (dummyDeposit * collateralMultiplier[0]) / collateralMultiplier[1]

            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)

            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustodyMock, dummyCollateral)
            // obligation with ERC20 tokens
            obs = [
                {
                    amount: dummyDeposit / 2,
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: dummyCoin.address,
                    reallocate: false,
                },
                {
                    amount: dummyDeposit / 2,
                    recipient: bobAddr,
                    deliverer: aliceAddr,
                    token: dummyCoin.address,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
            expect(await collateralCustodyMock.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.eq(
                dummyCollateral
            )
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")

            // give sdpAdmin2 1 less than required token stake
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustodyMock, dummyCollateral - 1)
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")

            // complete sdpAdmin2 stake requiremnt for last check
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustodyMock, 1)

            // complete quorum to allow new obligation report
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin1, sdpOperator3, collateralCustodyMock)
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustodyMock, dummyCollateral * 10)
            await approveAndDeposit(dummyCoin, sdpAdmin3, collateralCustodyMock, dummyCollateral * 10)
            await consensus
                .connect(sdpOperator1)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            await consensus
                .connect(sdpOperator3)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)

            settlementId = await coordination.callStatic.requestSettlement(dummyCoin.address)
            await coordination.requestSettlement(dummyCoin.address)
            // confirm second report now possible
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
            expect(await collateralCustodyMock.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.eq(
                dummyCollateral * 2
            )
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Check Merkle Root Generation Function is Working", async () => {
        it("Verifies Correct Merkle Root Generated for Arbitrary large sized Leaf Array", async () => {
            const calculateLeafFromObligation = (obligation: Obligation) =>
                ethers.utils.solidityKeccak256(
                    ["uint256", "address", "address", "address", "bool"],
                    [
                        obligation.amount,
                        obligation.deliverer,
                        obligation.recipient,
                        obligation.token,
                        obligation.reallocate,
                    ]
                )

            let leaves = bigObligations.map(calculateLeafFromObligation)
            //test different sizes of merkle tree generation
            for (let ii = 1; ii < leaves.length; ii++) {
                // get merkle roots to compare
                ethersMerkleRoot = await merkleFromObs(bigObligations.slice(0, ii))
                let contractMerkleRoot = await consensus.callStatic.getMerkleRoot(leaves.slice(0, ii))
                expect(ethersMerkleRoot).to.be.eq(contractMerkleRoot)
                // check that the verify function is working. It accepts obligations arrays and turns them to merkle leaves.
                expect(await consensus.callStatic.verifyMerkleTree(bigObligations.slice(0, ii))).to.be.eq(
                    ethersMerkleRoot
                )
            }
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Check Merkle Root And Obligation Requirements on Settlement Reporting, Checks Quorum Disputes", async () => {
        // TODO: Will flush out these test more after the full functionality is developed
        beforeEach(async () => {
            await identity.setStakeAmount(stakeAmount)
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)
            await operatorInit(sdpAdmin4, sdpOperator4, collateralCustodyMock)
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin3, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(protocolToken, sdpAdmin4, collateralCustodyMock, stakeAmount * 10)
            await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustodyMock, 100000)
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustodyMock, 100000)
            await approveAndDeposit(dummyCoin, sdpAdmin3, collateralCustodyMock, 100000)
            await approveAndDeposit(dummyCoin, sdpAdmin4, collateralCustodyMock, 100000)

            // get merkle roots to compare
            ethersMerkleRoot = await merkleFromObs(bigObligations)
            settlementId = await coordination.callStatic.requestSettlement(dummyCoin.address)
            await coordination.requestSettlement(dummyCoin.address)
        })
        it("Denies First Settlement Report if Merkle Root does not match", async () => {
            // change one arbitrary value in an arbitrary obligation to prevent merkle root match
            bigObligations[5].amount = 429

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(
                        coordination.address,
                        settlementId,
                        bigObligations,
                        ethersMerkleRoot
                    )
            ).to.be.revertedWith("MERKLE_DOES_NOT_MATCH_OBLIGATIONS")
        })

        it("Allows First Settlement Report if Merkle Root matches", async () => {
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(
                        coordination.address,
                        settlementId,
                        bigObligations,
                        ethersMerkleRoot
                    )
            ).to.not.be.reverted
        })
        it("First Settlement Report stores Merkle Root", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportSettlementObligations(
                    coordination.address,
                    settlementId,
                    bigObligations,
                    ethersMerkleRoot
                )

            const id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
            // verify stored merkle root
            expect((await consensus.callStatic.settlements(id)).merkleRoot).to.be.eq(ethersMerkleRoot)
        })
        describe("After First Obligation Report", async () => {
            // TODO: Will flush out these test more after the full functionality is developed
            beforeEach(async () => {
                await consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(
                        coordination.address,
                        settlementId,
                        bigObligations,
                        ethersMerkleRoot
                    )
                fakeObligations = bigObligations.slice(0, bigObligations.length - 1)
                fakeMerkleRoot = await merkleFromObs(bigObligations.slice(0, bigObligations.length - 1))
            })

            describe("Sets state to quorum disputed if Merkle Root Does not match first report", async () => {
                it("Quorum Count Unchanged", async () => {
                    id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
                    quorumCount = (await consensus.callStatic.settlements(id)).quorum
                    await consensus
                        .connect(sdpOperator2)
                        .reportSettlementObligations(
                            coordination.address,
                            settlementId,
                            fakeObligations,
                            fakeMerkleRoot
                        )
                    expect((await consensus.callStatic.settlements(id)).quorum).to.be.eq(quorumCount)
                })
                it("Quorum Dispute State Set", async () => {
                    expect((await consensus.callStatic.settlements(id)).stage).to.be.eq(0)
                    await consensus
                        .connect(sdpOperator2)
                        .reportSettlementObligations(
                            coordination.address,
                            settlementId,
                            fakeObligations,
                            fakeMerkleRoot
                        )
                    expect((await consensus.callStatic.settlements(id)).stage).to.be.eq(4)
                })
                it("Emits Event", async () => {
                    await expect(
                        consensus
                            .connect(sdpOperator2)
                            .reportSettlementObligations(
                                coordination.address,
                                settlementId,
                                fakeObligations,
                                fakeMerkleRoot
                            )
                    )
                        .to.emit(consensus, "SettlementDisputedQuorum")
                        .withArgs(settlementId)
                })
                it("Reverts if only Obligations Differ", async () => {
                    id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
                    quorumCount = (await consensus.callStatic.settlements(id)).quorum
                    await expect(
                        consensus
                            .connect(sdpOperator2)
                            .reportSettlementObligations(
                                coordination.address,
                                settlementId,
                                fakeObligations,
                                ethersMerkleRoot
                            )
                    ).to.be.revertedWith("MERKLE_DOES_NOT_MATCH_OBLIGATIONS")
                    expect((await consensus.callStatic.settlements(id)).quorum).to.be.eq(quorumCount)
                })
                it("Reverts if only Merkle Root Differs", async () => {
                    id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
                    quorumCount = (await consensus.callStatic.settlements(id)).quorum
                    await expect(
                        consensus
                            .connect(sdpOperator2)
                            .reportSettlementObligations(
                                coordination.address,
                                settlementId,
                                bigObligations,
                                fakeMerkleRoot
                            )
                    ).to.be.revertedWith("MERKLE_DOES_NOT_MATCH_OBLIGATIONS")
                    expect((await consensus.callStatic.settlements(id)).quorum).to.be.eq(quorumCount)
                })
            })

            it("Allows 2nd and 3rd Report if Obligations and Merkle Root Match, and Meets Quorum Requirements", async () => {
                const id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
                // check stage is 'Requested'
                expect((await consensus.callStatic.settlements(id)).stage).to.be.eq(0)

                await expect(
                    consensus
                        .connect(sdpOperator2)
                        .reportSettlementObligations(
                            coordination.address,
                            settlementId,
                            bigObligations,
                            ethersMerkleRoot
                        )
                ).to.not.be.reverted
                await expect(
                    consensus
                        .connect(sdpOperator3)
                        .reportSettlementObligations(
                            coordination.address,
                            settlementId,
                            bigObligations,
                            ethersMerkleRoot
                        )
                ).to.not.be.reverted
                // check stage is 'QuorumReached'
                expect((await consensus.callStatic.settlements(id)).stage).to.be.eq(1)
            })
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("After quorum is reached", async () => {
        beforeEach(async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            await identity.setStakeAmount(stakeAmount)
            ethCollateral = ethObligation.mul(collateralMultiplier[0]).div(collateralMultiplier[1])

            // obligation
            obs = [
                {
                    amount: ethObligation.div(2),
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
                {
                    amount: ethObligation.div(2),
                    recipient: bobAddr,
                    deliverer: aliceAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            // deposit collateral to sdp admins and set admin - operator relationships

            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)

            // Deposit full Protocol Token Collateral
            // deposit native token collateral for all sdps
            // deposit dummyCoin collateral to sdps
            for (const sdp of [
                [sdpAdmin1, sdpOperator1],
                [sdpAdmin2, sdpOperator2],
                [sdpAdmin3, sdpOperator3],
            ]) {
                await approveAndDeposit(protocolToken, sdp[0], collateralCustodyMock, stakeAmount)
                await collateralCustodyMock.connect(sdp[0]).deposit({value: ethCollateral})
                await approveAndDeposit(dummyCoin, sdp[0], collateralCustodyMock, dummyDeposit)
                await consensus
                    .connect(sdp[1])
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            }
        })

        it("Forbids Obligation Report after Quorum is reached", async () => {
            await operatorInit(sdpAdmin4, sdpOperator4, collateralCustodyMock)
            await approveAndDeposit(protocolToken, sdpAdmin4, collateralCustodyMock, stakeAmount)
            await approveAndDeposit(dummyCoin, sdpAdmin4, collateralCustodyMock, dummyDeposit)
            await collateralCustodyMock.connect(sdpAdmin4).deposit({value: ethCollateral})

            await expect(
                consensus
                    .connect(sdpOperator4)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INVALID_ID")
        })

        it("Forbids Obligation Report for Settlements past current settlement", async () => {
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount)
            await collateralCustodyMock.connect(sdpAdmin1).deposit({value: ethCollateral})
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount)
            await collateralCustodyMock.connect(sdpAdmin2).deposit({value: ethCollateral})

            settlementId = await coordination.callStatic.requestSettlement(dummyCoin.address)
            await coordination.requestSettlement(dummyCoin.address)
            // still allows current ID
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.not.be.reverted
            // does not allow next ID
            settlementId = await coordination.callStatic.requestSettlement(dummyCoin.address)
            await coordination.requestSettlement(dummyCoin.address)
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)
            ).to.be.revertedWith("INVALID_ID")
        })

        //-------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------

        describe("Function for Auditor Submitting Matching Exchange Merkle Root to Agree", async () => {
            beforeEach(async () => {
                message = ethers.utils.solidityKeccak256(
                    ["bytes32", "uint256", "address"],
                    [ethersMerkleRoot, settlementId, coordination.address]
                )
                signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)
                // report struct and signature
                exReport = {
                    merkleRoot: ethersMerkleRoot,
                    settlementId,
                    coordinator: coordination.address,
                }
                id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
            })

            it("verifies exchange signature and changes state of settlement", async () => {
                await consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)
                expect((await consensus.callStatic.settlements(id)).stage).to.be.eq(2)
            })

            it("Emits Event", async () => {
                await expect(consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s))
                    .to.emit(consensus, "SettlementConcurredExchange")
                    .withArgs(settlementId)
            })

            it("Forbids non auditor from reporting root", async () => {
                await expect(
                    consensus.connect(bob).reportExchangeMerkleRootConcur(exReport, v, r, s)
                ).to.be.revertedWith("SENDER_NOT_AUDITOR")
            })

            it("forbids reporting invalid exchange signature", async () => {
                const invalidSignatureParams: [[string, BigNumber, string], Signer, boolean][] = [
                    // should pass
                    [[ethersMerkleRoot, settlementId, coordination.address], exchange, true],
                    // Invalid merkle root
                    [[randomRoot, settlementId, coordination.address], exchange, false],
                    // Invalid settlement ID
                    [[ethersMerkleRoot, settlementId.add(1), coordination.address], exchange, false],
                    // Invalid coord address
                    [[ethersMerkleRoot, settlementId, consensus.address], exchange, false],
                    // Non-exchange signer
                    [[ethersMerkleRoot, settlementId, coordination.address], alice, false],
                ]
                for (const [params, signer, pass] of invalidSignatureParams) {
                    message = ethers.utils.solidityKeccak256(["bytes32", "uint256", "address"], params)
                    signedMessage = await signer.signMessage(ethers.utils.arrayify(message))

                    signature = signedMessage.substring(2)
                    r = "0x" + signature.substring(0, 64)
                    s = "0x" + signature.substring(64, 128)
                    v = parseInt(signature.substring(128, 130), 16)

                    // report struct and signature
                    exReport = {merkleRoot: ethersMerkleRoot, settlementId, coordinator: coordination.address}
                    if (pass) {
                        await expect(
                            consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)
                        ).to.not.be.reverted
                    } else {
                        await expect(
                            consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)
                        ).to.be.revertedWith("SIGNATURE_VERIF_FAILED_EXCHANGE")
                    }
                }
            })

            it("forbids reporting exchange merkle root twice", async () => {
                await consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)

                await expect(
                    consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)
                ).to.be.revertedWith("INVALID_SETTLEMENT_STATE")
            })

            it("Forbids Reporting unmatched root using the exchange root concur function", async () => {
                // alter obligation from exchange
                unmatchedObs = copyArray(obs)
                unmatchedObs[0].amount = 720
                merkleRootUnmatched = await merkleFromObs(unmatchedObs)

                message = ethers.utils.solidityKeccak256(
                    ["bytes32", "uint256", "address"],
                    [merkleRootUnmatched, settlementId, coordination.address]
                )
                signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)
                // report struct and signature
                exReport = {
                    merkleRoot: merkleRootUnmatched,
                    settlementId,
                    coordinator: coordination.address,
                }

                await expect(
                    consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)
                ).to.be.revertedWith("EXCHANGE_ROOT_DOES_NOT_MATCH_REPORTED_OBLIGATIONS")
            })
            it("Unlocks Collateral for all SDPs on successful Auditor Report", async () => {
                // check locked stake
                for (const sdp of [sdpAdmin1Address, sdpAdmin2Address, sdpAdmin3Address]) {
                    expect(await collateralCustodyMock.lockedBalances(sdp, _za)).to.be.equal(
                        await identity.calculateCollateral(ethObligation)
                    )
                    expect(
                        await collateralCustodyMock.lockedBalances(sdp, protocolToken.address)
                    ).to.be.equal(stakeAmount)
                }
                await consensus.connect(auditor).reportExchangeMerkleRootConcur(exReport, v, r, s)
                // check unlocked stake
                for (const sdp of [sdpAdmin1Address, sdpAdmin2Address, sdpAdmin3Address]) {
                    expect(await collateralCustodyMock.lockedBalances(sdp, _za)).to.be.equal(0)
                    expect(
                        await collateralCustodyMock.lockedBalances(sdp, protocolToken.address)
                    ).to.be.equal(0)
                }
            })
        })

        describe("Function for Auditor Submitting Unmatching Exchange Merkle Root to Dispute", async () => {
            beforeEach(async () => {
                // alter obligation from exchange
                unmatchedObs = copyArray(obs)
                unmatchedObs[0].amount = 720
                merkleRootUnmatched = await merkleFromObs(unmatchedObs)

                message = ethers.utils.solidityKeccak256(
                    ["bytes32", "uint256", "address"],
                    [merkleRootUnmatched, settlementId, coordination.address]
                )
                signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)
                // report struct and signature
                exReport = {
                    merkleRoot: merkleRootUnmatched,
                    settlementId,
                    coordinator: coordination.address,
                }
                id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
            })

            it("verifies exchange signature and changes state of settlement", async () => {
                await consensus
                    .connect(auditor)
                    .reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                const id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
                expect((await consensus.callStatic.settlements(id)).stage).to.be.eq(3)
            })

            it("Emits Event", async () => {
                await expect(
                    consensus
                        .connect(auditor)
                        .reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                )
                    .to.emit(consensus, "SettlementDisputedExchange")
                    .withArgs(settlementId)
            })

            it("Forbids non auditor from reporting root", async () => {
                await expect(
                    consensus.connect(bob).reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                ).to.be.revertedWith("SENDER_NOT_AUDITOR")
            })

            it("forbids reporting invalid exchange signature", async () => {
                const invalidSignatureParams: [[string, BigNumber, string], Signer, boolean][] = [
                    // should pass
                    [[merkleRootUnmatched, settlementId, coordination.address], exchange, true],
                    // merkle root does not match signature
                    [[randomRoot, settlementId, coordination.address], exchange, false],
                    // Invalid settlement ID
                    [[merkleRootUnmatched, settlementId.add(1), coordination.address], exchange, false],
                    // Invalid coord address
                    [[merkleRootUnmatched, settlementId, consensus.address], exchange, false],
                    // Non-exchange signer
                    [[merkleRootUnmatched, settlementId, coordination.address], alice, false],
                ]

                for (const [params, signer, pass] of invalidSignatureParams) {
                    message = ethers.utils.solidityKeccak256(["bytes32", "uint256", "address"], params)
                    signedMessage = await signer.signMessage(ethers.utils.arrayify(message))

                    signature = signedMessage.substring(2)
                    r = "0x" + signature.substring(0, 64)
                    s = "0x" + signature.substring(64, 128)
                    v = parseInt(signature.substring(128, 130), 16)

                    // report struct and signature
                    exReport = {
                        merkleRoot: merkleRootUnmatched,
                        settlementId,
                        coordinator: coordination.address,
                    }
                    if (pass) {
                        await expect(
                            consensus
                                .connect(auditor)
                                .reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                        ).to.not.be.reverted
                    } else {
                        await expect(
                            consensus
                                .connect(auditor)
                                .reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                        ).to.be.revertedWith("SIGNATURE_VERIF_FAILED_EXCHANGE")
                    }
                }
            })

            it("Forbids Reporting Twice", async () => {
                await consensus
                    .connect(auditor)
                    .reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                await expect(
                    consensus
                        .connect(auditor)
                        .reportExchangeMerkleRootDispute(unmatchedObs, exReport, v, r, s)
                ).to.be.revertedWith("INVALID_SETTLEMENT_STATE")
            })

            it("Forbids reporting valid merkle root and obligations in dispute function", async () => {
                message = ethers.utils.solidityKeccak256(
                    ["bytes32", "uint256", "address"],
                    [ethersMerkleRoot, settlementId, coordination.address]
                )
                signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)
                // report struct and signature
                exReport = {
                    merkleRoot: ethersMerkleRoot,
                    settlementId,
                    coordinator: coordination.address,
                }
                id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
                await expect(
                    consensus.connect(auditor).reportExchangeMerkleRootDispute(obs, exReport, v, r, s)
                ).to.be.revertedWith("EXCHANGE_ROOT_MATCHES_REPORTED_OBLIGATIONS")
            })
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("After Quorum Fails", async () => {
        beforeEach(async () => {
            settlementId = await coordination.callStatic.requestSettlement(_za)
            await coordination.requestSettlement(_za)
            await identity.setStakeAmount(stakeAmount)
            ethCollateral = ethObligation.mul(collateralMultiplier[0]).div(collateralMultiplier[1])

            // obligation
            obs = [
                {
                    amount: ethObligation.div(2),
                    recipient: aliceAddr,
                    deliverer: bobAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
                {
                    amount: ethObligation.div(2),
                    recipient: bobAddr,
                    deliverer: aliceAddr,
                    token: ethers.constants.AddressZero,
                    reallocate: false,
                },
            ]
            ethersMerkleRoot = await merkleFromObs(obs)

            // deposit collateral to sdp admins and set admin - operator relationships

            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)

            // Deposit full Protocol Token Collateral
            // deposit native token collateral for all sdps
            // deposit dummyCoin collateral to sdps
            for (const sdpAdmin of [sdpAdmin1, sdpAdmin2, sdpAdmin3]) {
                await approveAndDeposit(protocolToken, sdpAdmin, collateralCustodyMock, stakeAmount)
                await collateralCustodyMock.connect(sdpAdmin).deposit({value: ethCollateral})
                await approveAndDeposit(dummyCoin, sdpAdmin, collateralCustodyMock, dummyDeposit)
            }

            await consensus
                .connect(sdpOperator1)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)

            await consensus
                .connect(sdpOperator2)
                .reportSettlementObligations(coordination.address, settlementId, obs, ethersMerkleRoot)

            unmatchedObs = copyArray(obs)
            // console.log(unmatchedObs[0].amount)
            unmatchedObs[0].amount = 720
            merkleRootUnmatched = await merkleFromObs(unmatchedObs)

            await consensus
                .connect(sdpOperator3)
                .reportSettlementObligations(
                    coordination.address,
                    settlementId,
                    unmatchedObs,
                    merkleRootUnmatched
                )

            message = ethers.utils.solidityKeccak256(
                ["bytes32", "uint256", "address"],
                [ethersMerkleRoot, settlementId, coordination.address]
            )
            signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

            signature = signedMessage.substring(2)
            r = "0x" + signature.substring(0, 64)
            s = "0x" + signature.substring(64, 128)
            v = parseInt(signature.substring(128, 130), 16)
            // report struct and signature
            exReport = {
                merkleRoot: ethersMerkleRoot,
                settlementId,
                coordinator: coordination.address,
            }
            id = await consensus.callStatic.getObligationsId(coordination.address, settlementId)
        })
        describe("Sets correct rewarded vs slashed entries in output when Auditor reports after Quorum Dispute", async () => {
            it("Exchange agrees with first reporters", async () => {
                let bool = await consensus
                    .connect(auditor)
                    .callStatic.reportMerkleRootQuorumDisputed(obs, exReport, v, r, s)

                expect(bool[0]).to.be.eq(true)
                expect(bool[1]).to.be.eq(true)
                expect(bool[2]).to.be.eq(false)
            })
            it("Exchange agrees with disputing reporter", async () => {
                message = ethers.utils.solidityKeccak256(
                    ["bytes32", "uint256", "address"],
                    [merkleRootUnmatched, settlementId, coordination.address]
                )
                signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)
                // report struct and signature
                exReport = {
                    merkleRoot: merkleRootUnmatched,
                    settlementId,
                    coordinator: coordination.address,
                }
                let bool = await consensus
                    .connect(auditor)
                    .callStatic.reportMerkleRootQuorumDisputed(unmatchedObs, exReport, v, r, s)

                expect(bool[0]).to.be.eq(false)
                expect(bool[1]).to.be.eq(false)
                expect(bool[2]).to.be.eq(true)
            })
            it("Exchange agrees with no reporters", async () => {
                unmatchedObs[0].amount = 1720
                merkleRootUnmatched = await merkleFromObs(unmatchedObs)
                message = ethers.utils.solidityKeccak256(
                    ["bytes32", "uint256", "address"],
                    [merkleRootUnmatched, settlementId, coordination.address]
                )
                signedMessage = await exchange.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)
                // report struct and signature
                exReport = {
                    merkleRoot: merkleRootUnmatched,
                    settlementId,
                    coordinator: coordination.address,
                }
                let bool = await consensus
                    .connect(auditor)
                    .callStatic.reportMerkleRootQuorumDisputed(unmatchedObs, exReport, v, r, s)

                expect(bool[0]).to.be.eq(false)
                expect(bool[1]).to.be.eq(false)
                expect(bool[2]).to.be.eq(false)
            })
        })
        it("Emits Event", async () => {
            await expect(consensus.connect(auditor).reportMerkleRootQuorumDisputed(obs, exReport, v, r, s))
                .to.emit(consensus, "QuorumDisputeSettledExchange")
                .withArgs(settlementId, exReport.merkleRoot)
        })

        it("Forbids non auditor from reporting root", async () => {
            await expect(
                consensus.connect(bob).reportMerkleRootQuorumDisputed(obs, exReport, v, r, s)
            ).to.be.revertedWith("SENDER_NOT_AUDITOR")
        })

        it("forbids reporting invalid exchange signature", async () => {
            const invalidSignatureParams: [[string, BigNumber, string], Signer, boolean][] = [
                // should pass
                [[ethersMerkleRoot, settlementId, coordination.address], exchange, true],
                // merkle does not match signature
                [[randomRoot, settlementId, coordination.address], exchange, false],
                // Invalid settlement ID
                [[ethersMerkleRoot, settlementId.add(1), coordination.address], exchange, false],
                // Invalid coord address
                [[ethersMerkleRoot, settlementId, consensus.address], exchange, false],
                // Non-exchange signer
                [[ethersMerkleRoot, settlementId, coordination.address], alice, false],
            ]

            for (const [params, signer, pass] of invalidSignatureParams) {
                message = ethers.utils.solidityKeccak256(["bytes32", "uint256", "address"], params)
                signedMessage = await signer.signMessage(ethers.utils.arrayify(message))

                signature = signedMessage.substring(2)
                r = "0x" + signature.substring(0, 64)
                s = "0x" + signature.substring(64, 128)
                v = parseInt(signature.substring(128, 130), 16)

                // report struct and signature
                exReport = {
                    merkleRoot: ethersMerkleRoot,
                    settlementId,
                    coordinator: coordination.address,
                }
                if (pass) {
                    await expect(
                        consensus.connect(auditor).reportMerkleRootQuorumDisputed(obs, exReport, v, r, s)
                    ).to.not.be.reverted
                } else {
                    await expect(
                        consensus.connect(auditor).reportMerkleRootQuorumDisputed(obs, exReport, v, r, s)
                    ).to.be.revertedWith("SIGNATURE_VERIF_FAILED_EXCHANGE")
                }
            }
        })
        //TODO: after stake is slashed and obligations are reported and settled in the Coordination and Asset Custody
        //      We will have to change the settlement state to "Cleared" until we do this, it will be possible to report
        //      An exchange root more than once.
        it.skip("Forbids Reporting Twice", async () => {
            await consensus.connect(auditor).reportMerkleRootQuorumDisputed(obs, exReport, v, r, s)
            await expect(
                consensus.connect(auditor).reportMerkleRootQuorumDisputed(obs, exReport, v, r, s)
            ).to.be.revertedWith("INVALID_SETTLEMENT_STATE")
        })
    })
})
