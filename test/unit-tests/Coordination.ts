// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Signer} from "ethers"
import {
    deploy,
    getSigner,
    IdentityRegistry,
    Obligation,
    SettlementCoordination,
    setupTest,
    AssetMock,
    _za,
} from "../utils"
import {DummyCoin} from "../../typechain/DummyCoin"

describe("SettlementCoordination", function () {
    this.timeout(200000)
    let coordination: SettlementCoordination
    let identity: IdentityRegistry
    let noRole: Address
    let alice: Signer
    let bob: Signer
    let factory: Signer
    let unnamedAccounts: Address[]
    let consensus: Signer
    let assetCustodyMock: AssetMock
    let dummyCoin: DummyCoin

    let coordAddress: Address
    let assetCustodyAddress: Address
    let aliceAddress: Address
    let bobAddress: Address

    let zeroEth = ethers.utils.parseEther("0")
    let settlementId = zeroEth // make settlementID a BigNumber of value zero

    let obligation: Obligation

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        unnamedAccounts = await getUnnamedAccounts()
        alice = await getSigner(namedAccounts.alice)
        bob = await getSigner(namedAccounts.bob)

        aliceAddress = ethers.utils.getAddress(await alice.getAddress())
        bobAddress = ethers.utils.getAddress(await bob.getAddress())

        consensus = await getSigner(namedAccounts.consensus)
        factory = await getSigner(unnamedAccounts[14])
        noRole = namedAccounts.noRole
    })

    beforeEach(async function () {
        let contracts = await setupTest()
        identity = contracts.identity
        coordination = contracts.coordination
        assetCustodyMock = contracts.assetMock
        dummyCoin = contracts.dummyCoin

        coordAddress = coordination.address
        assetCustodyAddress = assetCustodyMock.address

        await identity.updateRole(
            await contracts.library.callStatic.ROLE_CONSENSUS(),
            await consensus.getAddress()
        )
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_ASSET_CUSTODY(),
            await assetCustodyAddress
        )
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing Request Settlement", async () => {
        it("Request Settlement does not effect lastSettlementIdProcessed", async () => {
            const lastSettlementIdProcessed = await coordination.callStatic.lastSettlementIdProcessed()
            await assetCustodyMock.coordinationRequestSettlement(coordAddress, _za)
            expect(await coordination.callStatic.lastSettlementIdProcessed()).to.be.eq(
                lastSettlementIdProcessed
            )
        })

        it("Request Settlement returned request IDs the same as nextRequestId stored in contract at the time of fcn call", async () => {
            const nextId = await coordination.callStatic.nextRequestId()
            let settlementId = await assetCustodyMock.callStatic.coordinationRequestSettlement(
                coordAddress,
                _za
            )
            expect(settlementId).to.be.eq(nextId)
        })

        it("Request Settlement stores settlement block number properly", async () => {
            let settlementId = await assetCustodyMock.callStatic.coordinationRequestSettlement(
                coordAddress,
                _za
            )
            const tx = await (await assetCustodyMock.coordinationRequestSettlement(coordAddress, _za)).wait()
            expect(await (await coordination.callStatic.getReqData(settlementId)).requestBlock).to.be.eq(
                ethers.BigNumber.from(tx.blockNumber)
            )
        })

        it("Request Settlement stores settlement token address properly", async () => {
            let settlementId = await assetCustodyMock.callStatic.coordinationRequestSettlement(
                coordAddress,
                _za
            )
            const tx = await (
                await assetCustodyMock.coordinationRequestSettlement(coordAddress, dummyCoin.address)
            ).wait()
            expect(await (await coordination.callStatic.getReqData(settlementId)).token).to.be.eq(
                dummyCoin.address
            )
        })

        it("Request Settlement returned request IDs increment properly", async () => {
            let testSerial = 1

            await assetCustodyMock.coordinationRequestSettlement(coordAddress, _za)
            testSerial++
            await assetCustodyMock.coordinationRequestSettlement(coordAddress, _za)
            testSerial++

            const functionReturnSerialID = await assetCustodyMock.callStatic.coordinationRequestSettlement(
                coordAddress,
                _za
            )
            testSerial++

            expect(functionReturnSerialID).to.be.eq(testSerial)
        })

        it("forbids non asset custody contract from requesting Settlement", async () => {
            await expect(coordination.connect(bob).requestSettlement(_za)).to.be.revertedWith(
                "NOT_WALLET_SINGLETON"
            )
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing reportObligations", async () => {
        beforeEach(async () => {
            settlementId = await assetCustodyMock.callStatic.coordinationRequestSettlement(coordAddress, _za)
            await assetCustodyMock.coordinationRequestSettlement(coordAddress, _za)

            obligation = {
                amount: ethers.utils.parseEther("1"),
                deliverer: aliceAddress,
                recipient: bobAddress,
                token: ethers.constants.AddressZero,
                reallocate: true,
            }
        })

        it("calling reportObligations with correct settlement ID as consensus succeeds", async () => {
            await expect(coordination.connect(consensus).reportObligations(settlementId, [obligation])).to.not
                .be.reverted
        })

        it("forbids non-consensus from calling reportObligations", async () => {
            await expect(
                coordination.connect(bob).reportObligations(settlementId, [obligation])
            ).to.be.revertedWith("SENDER_NOT_CONSENSUS")
        })

        it("forbids reporting obligations with settlement Id out of order", async () => {
            await expect(
                coordination.connect(consensus).reportObligations(settlementId.add(1), [obligation])
            ).to.be.revertedWith("WRONG_ID")
            await expect(
                coordination.connect(consensus).reportObligations(settlementId.sub(1), [obligation])
            ).to.be.revertedWith("WRONG_ID")
        })

        it("successive calls of reportObligations demonstrates lastSettlementIdProcessed serial is updated properly", async () => {
            let testSerial = settlementId

            await expect(coordination.connect(consensus).reportObligations(testSerial, [obligation])).to.not
                .be.reverted
            testSerial = testSerial.add(1)
            await expect(coordination.connect(consensus).reportObligations(testSerial, [obligation])).to.not
                .be.reverted
            testSerial = testSerial.add(1)
            await expect(coordination.connect(consensus).reportObligations(testSerial, [obligation])).to.not
                .be.reverted
            testSerial = testSerial.add(1)
            await expect(coordination.connect(consensus).reportObligations(testSerial, [obligation])).to.not
                .be.reverted
        })

        it("calling reportObligations with reallocate==false in obligation writes obligation to wallet", async () => {
            obligation.reallocate = false
            let oldWriteCount = await assetCustodyMock.callStatic.writeCount()
            let oldReallocateCount = await assetCustodyMock.callStatic.reallocateCount()

            await coordination.connect(consensus).reportObligations(settlementId, [obligation])

            let newWriteCount = await assetCustodyMock.callStatic.writeCount()
            let newReallocateCount = await assetCustodyMock.callStatic.reallocateCount()

            // writeObligation call counter goes up, but reallocateObligation call counter does not
            expect(newWriteCount.sub(oldWriteCount)).to.be.eq(1)
            expect(newReallocateCount.sub(oldReallocateCount)).to.be.eq(0)
        })

        it("calling reportObligations with reallocate==true in obligation reallocates obligation in wallet", async () => {
            obligation.reallocate = true
            let oldWriteCount = await assetCustodyMock.callStatic.writeCount()
            let oldReallocateCount = await assetCustodyMock.callStatic.reallocateCount()

            await coordination.connect(consensus).reportObligations(settlementId, [obligation])

            let newWriteCount = await assetCustodyMock.callStatic.writeCount()
            let newReallocateCount = await assetCustodyMock.callStatic.reallocateCount()

            // reallocateObligation call counter goes up, but writeObligation call counter does not
            expect(newWriteCount.sub(oldWriteCount)).to.be.eq(0)
            expect(newReallocateCount.sub(oldReallocateCount)).to.be.eq(1)
        })

        // todo: fix error messages
        it("calling reportObligations to write obligations fails if writeObligations fails", async () => {
            await assetCustodyMock.connect(alice).setFunctionFailure()
            obligation.reallocate = false
            await expect(coordination.connect(consensus).reportObligations(settlementId, [obligation])).to.be
                .reverted //With("WRITE_OB_FAILED")
        })

        // todo: fix error messages
        it("calling reportObligations to reallocate obligations fails if reallocateObligations fails", async () => {
            await assetCustodyMock.connect(alice).setFunctionFailure()
            obligation.reallocate = true
            await expect(coordination.connect(consensus).reportObligations(settlementId, [obligation])).to.be
                .reverted //With("WRITE_OB_FAILED")
        })

        it("When array of many obligations called, writes/reallocates many times but only increments counter once", async () => {
            obligation.reallocate = false
            var writeOb1: Obligation = {...obligation}
            var writeOb2: Obligation = {...obligation}
            var writeOb3: Obligation = {...obligation}
            let writeTotal = 3

            var reallocateOb1: Obligation = {...obligation}
            reallocateOb1.reallocate = true
            var reallocateOb2: Obligation = {...obligation}
            reallocateOb2.reallocate = true
            let reallocateTotal = 2

            let oldWriteCount = await assetCustodyMock.callStatic.writeCount()
            let oldReallocateCount = await assetCustodyMock.callStatic.reallocateCount()

            const lastSettlementIdProcessed = await coordination.callStatic.lastSettlementIdProcessed()
            await coordination
                .connect(consensus)
                .reportObligations(settlementId, [writeOb1, reallocateOb1, writeOb2, reallocateOb2, writeOb3])

            let newWriteCount = await assetCustodyMock.callStatic.writeCount()
            let newReallocateCount = await assetCustodyMock.callStatic.reallocateCount()

            // writeObligation call counter goes up, but reallocateObligation call counter does not
            expect(newWriteCount.sub(oldWriteCount)).to.be.eq(writeTotal)
            expect(newReallocateCount.sub(oldReallocateCount)).to.be.eq(reallocateTotal)

            expect(await coordination.callStatic.lastSettlementIdProcessed()).to.be.eq(
                lastSettlementIdProcessed.add(1)
            )
        })

        it("Report Obligations will fail if token of obligation being written or reallocated does not match the settlement request token", async () => {
            obligation.reallocate = false
            var writeOb1: Obligation = {...obligation}
            var writeOb2: Obligation = {...obligation}
            var writeOb3: Obligation = {...obligation}

            var reallocateOb1: Obligation = {...obligation}
            reallocateOb1.reallocate = true
            var reallocateOb2: Obligation = {...obligation}
            reallocateOb2.reallocate = true
            reallocateOb1.token = dummyCoin.address
            await expect(
                coordination
                    .connect(consensus)
                    .reportObligations(settlementId, [
                        writeOb1,
                        reallocateOb1,
                        writeOb2,
                        reallocateOb2,
                        writeOb3,
                    ])
            ).to.be.revertedWith("INVALID_TOKEN")
            reallocateOb1.token = _za
            writeOb2.token = dummyCoin.address
            await expect(
                coordination
                    .connect(consensus)
                    .reportObligations(settlementId, [
                        writeOb1,
                        reallocateOb1,
                        writeOb2,
                        reallocateOb2,
                        writeOb3,
                    ])
            ).to.be.revertedWith("INVALID_TOKEN")
        })

        it("calling reportObligations emits a event: ObligationsWritten(settlementId)", async () => {
            await expect(coordination.connect(consensus).reportObligations(settlementId, [obligation]))
                .to.emit(coordination, "ObligationsWritten")
                .withArgs(settlementId)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    it("assetMock writeObligation() and reallocateObligation() work", async () => {
        expect(await assetCustodyMock.callStatic.writeCount()).to.be.eq(0)
        expect(await assetCustodyMock.callStatic.reallocateCount()).to.be.eq(0)
        await assetCustodyMock.connect(bob).writeObligation(_za, _za, _za, 24)
        expect(await assetCustodyMock.callStatic.writeCount()).to.be.eq(1)
        expect(await assetCustodyMock.callStatic.reallocateCount()).to.be.eq(0)
        await assetCustodyMock.connect(bob).reallocateObligation(_za, _za, _za, 24)
        expect(await assetCustodyMock.callStatic.writeCount()).to.be.eq(1)
        expect(await assetCustodyMock.callStatic.reallocateCount()).to.be.eq(1)
    })
})
