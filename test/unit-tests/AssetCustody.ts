// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Contract, Signer} from "ethers"

import {
    AssetCustody,
    Contracts,
    setupTest,
    _za,
    DummyCoin,
    getSigner,
    IdentityRegistry,
    SettlementLib,
    getDeployed,
    CoordinationMock,
    approveAndDeposit,
} from "../utils"

describe("Asset Custody", function () {
    const hre = require("hardhat")
    this.timeout(200000)

    let identity: IdentityRegistry
    let library: SettlementLib
    let coordination: CoordinationMock
    let unnamedAccounts: Address[]
    let exchange: Signer

    let alice: Signer
    let bob: Signer
    let charlie: Signer

    let aliceAddress: Address
    let bobAddress: Address
    let charlieAddress: Address
    let feeRecipientAddress: Address

    let aliceAsset: AssetCustody
    let bobAsset: AssetCustody
    let charlieAsset: AssetCustody
    let hardhatAsset: AssetCustody
    let zeroEth = ethers.utils.parseEther("0")

    let contracts: Contracts
    let dummyCoin: DummyCoin

    let coordRole: string
    let tokenRole: string
    let exchangeRole: string

    let contractBalanceBefore = zeroEth
    let bobAssetBalanceBefore = zeroEth

    const ethTransfer = ethers.utils.parseEther("0.7")
    let tokenDeposit = 1450
    const bobDeposit = 4900
    const aliceDeposit = 2400
    const totalDeposits = bobDeposit + aliceDeposit
    const bobDepositEth = ethers.utils.parseEther("4.9")
    const aliceDepositEth = ethers.utils.parseEther("2.4")
    const totalDepositsEth = bobDepositEth.add(aliceDepositEth)

    const bobEthDeposits = [
        ethers.utils.parseEther("1.3"),
        ethers.utils.parseEther("0.4"),
        ethers.utils.parseEther("2.9"),
    ]
    const aliceEthDeposits = [
        ethers.utils.parseEther("2.2"),
        ethers.utils.parseEther("0.3"),
        ethers.utils.parseEther("2.8"),
    ]

    const bobDeposits = [1300, 400, 2900]
    const aliceDeposits = [2200, 300, 2800]
    let bobDepositSum = zeroEth
    let aliceDepositSum = zeroEth

    before(async () => {
        unnamedAccounts = await getUnnamedAccounts()
        const namedAccounts = await getNamedAccounts()
        alice = await ethers.provider.getSigner(namedAccounts.alice)
        bob = await ethers.provider.getSigner(namedAccounts.bob)
        charlie = await ethers.provider.getSigner(namedAccounts.charlie)

        aliceAddress = ethers.utils.getAddress(await alice.getAddress())
        bobAddress = ethers.utils.getAddress(await bob.getAddress())
        charlieAddress = ethers.utils.getAddress(await charlie.getAddress())
        feeRecipientAddress = namedAccounts.feeRecipient
        exchange = await getSigner(namedAccounts.exchange)
    })

    beforeEach(async function () {
        contracts = await setupTest()
        identity = contracts.identity
        coordination = contracts.coordinationMock

        hardhatAsset = contracts.assetCustody
        aliceAsset = hardhatAsset.connect(alice) // could also use a factory
        bobAsset = hardhatAsset.connect(bob)
        charlieAsset = hardhatAsset.connect(charlie)
        dummyCoin = contracts.dummyCoin
        library = (await getDeployed("SettlementLib")) as SettlementLib
        coordRole = await library.callStatic.ROLE_LOCALCOORD()
        tokenRole = await library.callStatic.TRADEABLE_TOKEN()
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Test Native Token Deposits", async () => {
        beforeEach(async () => {
            contractBalanceBefore = await ethers.provider.getBalance(hardhatAsset.address)
            bobAssetBalanceBefore = await hardhatAsset.depositBalances(bobAddress, _za)
            await expect(bobAssetBalanceBefore).to.equal(zeroEth)
            await expect(contractBalanceBefore).to.equal(zeroEth)
            bobDepositSum = zeroEth
            aliceDepositSum = zeroEth
        })
        it("Accepts native token deposits and recieves tokens to contract", async () => {
            /**
             * User Deposits native token using deposit function part 1:
             * The smart contract receives the token to contract address
             * sent using the deposit function.
             */

            //bob deposits eth to the contract using the deposit function
            await bobAsset.deposit({value: bobDepositEth})
            let contractBalanceAfter = await ethers.provider.getBalance(hardhatAsset.address)
            await expect(contractBalanceAfter).to.equal(bobDepositEth)
        })

        it("Accepts native token deposits to user custody account in contract", async () => {
            /**
             * User Deposits native token using deposit function part 2:
             * Upon receipt of the native token using the deposit function the
             * smart contract records the ownership of the exact token quantity
             * to the mapping at the index: [user address, token address:_za]
             */
            //bob deposits eth to the contract using the deposit function
            await bobAsset.deposit({value: bobDepositEth})
            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, _za)
            await expect(bobAssetBalanceAfter).to.equal(bobDepositEth)
        })

        it("Accepts multiple native token deposits to same custody account address ", async () => {
            /**
             * Smart Contract accepts multiple deposits from the same address
             * using the deposit function. Will pass if both contract wallet
             * and user custody account map location is updated properly
             *
             * Native Token
             */

            for (let ii = 0; ii < bobEthDeposits.length; ii++) {
                await bobAsset.deposit({value: bobEthDeposits[ii]})
                bobDepositSum = bobDepositSum.add(bobEthDeposits[ii])
            }
            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, _za)
            let contractBalanceAfter = await ethers.provider.getBalance(hardhatAsset.address)
            await expect(bobAssetBalanceAfter).to.equal(bobDepositSum)
            await expect(contractBalanceAfter).to.equal(bobDepositSum)
        })

        it("Accepts multiple native token deposits to multiple custody account addresses", async () => {
            /**
             * Smart Contract accepts multiple deposits from multiple addresses
             * using the deposit function. Will pass if both contract wallet
             * and both user custody account map locations are updated properly
             *
             * Native Token
             */

            //interleaved deposits from bob & alice to asset custody contract

            for (let ii = 0; ii < bobEthDeposits.length; ii++) {
                await bobAsset.deposit({value: bobEthDeposits[ii]})
                await aliceAsset.deposit({value: aliceEthDeposits[ii]})
                bobDepositSum = bobDepositSum.add(bobEthDeposits[ii])
                aliceDepositSum = aliceDepositSum.add(aliceEthDeposits[ii])
            }

            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, _za)
            let aliceAssetBalanceAfter = await hardhatAsset.depositBalances(aliceAddress, _za)
            let contractBalanceAfter = await ethers.provider.getBalance(hardhatAsset.address)
            await expect(bobAssetBalanceAfter).to.equal(bobDepositSum)
            await expect(aliceAssetBalanceAfter).to.equal(aliceDepositSum)
            await expect(contractBalanceAfter).to.equal(bobDepositSum.add(aliceDepositSum))
        })

        it("Rejects native token sent to contract without the deposit fuction", async () => {
            /**
             * Smart contract rejects a generic deposit.
             * This means a deposit sent as a generic blockchain transaction with
             * a non-zero value field will be rejected.
             * mySigner.sendTransaction({value: 10, to:myContract.address,}) in ethers
             */
            await expect(
                bob.sendTransaction({
                    value: bobDepositEth,
                    to: hardhatAsset.address,
                })
            ).to.be.revertedWith(
                "Transaction reverted: function selector was not recognized and there's no fallback nor receive function"
            )
        })

        it("Native token deposit to asset custody contract emits an event", async () => {
            /**
             * Makes a native token deposit and checks to see that the contract emits an event
             * in the form Deposit(user address, token amount, token zero address) as expected
             */
            await expect(bobAsset.deposit({value: bobDepositEth}))
                .to.emit(hardhatAsset, "Deposit")
                .withArgs(bobAddress, bobDepositEth, _za)
        })
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests ERC20 Token Deposits", async () => {
        it("Accepts ERC20 token deposits and recieves tokens to contract", async () => {
            await dummyCoin.connect(bob).approve(hardhatAsset.address, bobDeposit)
            await bobAsset.depositToken(bobDeposit, dummyCoin.address)
            const contractBalance = await dummyCoin.balanceOf(hardhatAsset.address)

            expect(contractBalance).equal(bobDeposit)
        })

        it("Accepts ERC20 token deposits and updates user custody account", async () => {
            await dummyCoin.connect(bob).approve(hardhatAsset.address, bobDeposit)
            await bobAsset.depositToken(bobDeposit, dummyCoin.address)
            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, dummyCoin.address)

            expect(bobAssetBalanceAfter).equal(bobDeposit)
        })

        it("Accepts multiple ERC20 token deposits to same custody account address ", async () => {
            /**
             * Smart Contract accepts multiple deposits from the same address
             * using the deposit function. Will pass if both contract wallet
             * and user custody account map location is updated properly
             * ERC20
             */

            let bobDepositSum = 0
            for (let ii = 0; ii < bobDeposits.length; ii++) {
                await approveAndDeposit(dummyCoin, bob, hardhatAsset, bobDeposits[ii])
                bobDepositSum += bobDeposits[ii]
            }
            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, dummyCoin.address)
            const contractBalance = await dummyCoin.balanceOf(hardhatAsset.address)

            await expect(bobAssetBalanceAfter).to.equal(bobDepositSum)
            await expect(contractBalance).to.equal(bobDepositSum)
        })

        it("Accepts multiple ERC20 token deposits to custody account addresses", async () => {
            /**
             * Smart Contract accepts multiple deposits from multiple addresses
             * using the deposit function. Will pass if both contract wallet
             * and both user custody account map locations are updated properly
             * ERC20
             */
            let bobDepositSum = 0
            let aliceDepositSum = 0
            for (let ii = 0; ii < bobDeposits.length; ii++) {
                await approveAndDeposit(dummyCoin, bob, hardhatAsset, bobDeposits[ii])
                bobDepositSum += bobDeposits[ii]
                await approveAndDeposit(dummyCoin, alice, hardhatAsset, aliceDeposits[ii])
                aliceDepositSum += aliceDeposits[ii]
            }

            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, dummyCoin.address)
            let aliceAssetBalanceAfter = await hardhatAsset.depositBalances(aliceAddress, dummyCoin.address)
            const contractBalance = await dummyCoin.balanceOf(hardhatAsset.address)

            await expect(bobAssetBalanceAfter).to.equal(bobDepositSum)
            await expect(aliceAssetBalanceAfter).to.equal(aliceDepositSum)
            await expect(contractBalance).to.equal(bobDepositSum + aliceDepositSum)
        })

        it("Reverts upon failed ERC20 token deposit and does not update user custody account", async () => {
            /**
             * makes a deposit higher than the approved amount to trigger ERC20 deposit failure
             * verifies that the user custody account, and contract ERC20 balance does not change
             */
            const bobDeposit = 1000
            // tramsfer allowance set to be 50 less than bobDeposit to trigger a failed transfer
            await dummyCoin.connect(bob).approve(hardhatAsset.address, bobDeposit - 50)

            await expect(bobAsset.depositToken(bobDeposit, dummyCoin.address)).to.be.reverted

            let bobAssetBalanceAfter = await hardhatAsset.depositBalances(bobAddress, dummyCoin.address)
            const contractBalance = await dummyCoin.balanceOf(hardhatAsset.address)
            expect(contractBalance).equal(0)
            expect(bobAssetBalanceAfter).equal(0)
        })

        it("ERC20 token deposit to asset custody contract emits an event", async () => {
            /**
             * Makes an ERC20 token deposit and checks to see that the contract emits an event
             * in the form Deposit(user address, token amount, token address) as expected
             */
            await dummyCoin.connect(bob).approve(hardhatAsset.address, bobDeposit)
            await expect(bobAsset.depositToken(bobDeposit, dummyCoin.address))
                .to.emit(hardhatAsset, "Deposit")
                .withArgs(bobAddress, bobDeposit, dummyCoin.address)
        })
        it("Rejects token deposit of untradeable tokens, accepts deposit of tradeable tokens", async () => {
            await identity.connect(exchange).revokeRole(tokenRole, dummyCoin.address)

            await dummyCoin.connect(bob).approve(hardhatAsset.address, bobDeposit)

            await expect(bobAsset.depositToken(bobDeposit, dummyCoin.address)).to.be.revertedWith(
                "NOT_TRADEABLE_TOKEN"
            )

            await identity.connect(exchange).grantRole(tokenRole, dummyCoin.address)

            await expect(bobAsset.depositToken(bobDeposit, dummyCoin.address)).to.not.be.reverted
        })
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests requestSettlement() function", async () => {
        beforeEach(async () => {
            await aliceAsset.upgradeCoordinator()
            await bobAsset.upgradeCoordinator()
            await charlieAsset.upgradeCoordinator()
        })
        it("Maps trader address to serial ID of trader's most recent settlement address", async () => {
            /**
             * Makes settlement requests by calling the requestSettlement function
             * from 3 different addresses, and checks that the stored most recent settlemnt ID
             * for each address is correct.
             */

            await aliceAsset.requestSettlement(_za)
            await bobAsset.requestSettlement(_za)
            await charlieAsset.requestSettlement(_za)

            expect(await hardhatAsset.lastSettlementReqId(aliceAddress, _za)).to.equal(2)
            expect(await hardhatAsset.lastSettlementReqId(bobAddress, _za)).to.equal(3)
            expect(await hardhatAsset.lastSettlementReqId(charlieAddress, _za)).to.equal(4)
        })

        it("Maps settlement ID serial to token address and block number of settlement execution", async () => {
            /**
             * Makes settlement requests using the requestSettlement() function and checks
             * that the block number of settlement request has been stored properly. They are stored
             * in a mapping where the settlement ID serial number is mapped to the block number.
             */

            let testSerial = 1

            await aliceAsset.requestSettlement(dummyCoin.address)
            testSerial++
            await bobAsset.requestSettlement(dummyCoin.address)
            testSerial++

            const txReceipt = await (await charlieAsset.requestSettlement(dummyCoin.address)).wait()
            testSerial++

            // check block number from testSerial
            expect(txReceipt.blockNumber).to.equal(
                await (
                    await coordination.callStatic.getReqData(testSerial)
                ).requestBlock
            )
            // check block number by getting serial from the lastSettlementReqId mapping in the contract
            expect(txReceipt.blockNumber).to.equal(
                (
                    await coordination.callStatic.getReqData(
                        await hardhatAsset.lastSettlementReqId(charlieAddress, dummyCoin.address)
                    )
                ).requestBlock
            )
            expect(await (await coordination.callStatic.getReqData(testSerial)).token).to.equal(
                dummyCoin.address
            )
        })

        it("Verifies that the contract emits the expected event", async () => {
            /**
             * After making a few settlement requests to increment the serial number from the default,
             * a settlement is requested using requestSettlement(). The transaction log is parsed to
             * verify that the settlement request triggered an event emission and that the event emission
             * has all the expected properties.
             */

            let testSerial = 1

            await aliceAsset.requestSettlement(dummyCoin.address)
            testSerial++
            await bobAsset.requestSettlement(dummyCoin.address)
            testSerial++
            //increment one more time for event emit call
            testSerial++
            await expect(charlieAsset.requestSettlement(dummyCoin.address))
                .to.emit(hardhatAsset, "SettlementRequested")
                .withArgs(testSerial, charlieAddress, dummyCoin.address)
        })

        it("requestSettlement function returns the correct serial number", async () => {
            /**
             * After making a few settlement requests to increment the serial number from the default,
             * check that requestSettlement()'s function returns are working and deliver the correct serial number
             * for the settlement requested by the function call
             */

            let testSerial = 1

            await aliceAsset.requestSettlement(_za)
            testSerial++
            await bobAsset.requestSettlement(_za)
            testSerial++

            const functionReturnSerialID = await charlieAsset.callStatic.requestSettlement(_za)
            testSerial++

            expect(functionReturnSerialID).to.be.equal(ethers.BigNumber.from(testSerial))
        })
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing restrictions on request settlement", async () => {
        beforeEach(async () => {
            //load up custody accounts
            await aliceAsset.deposit({value: aliceDepositEth})
            await bobAsset.deposit({value: bobDepositEth})

            await bobAsset.upgradeCoordinator()
            await aliceAsset.upgradeCoordinator()
        })
        it("Disallows requesting settlement if the user's settlement is currently being processed", async () => {
            /**
             * Disallows requesting settlement if the most recent processed settlement Id is not at least one ahead
             * of the user's last requested settlement Id
             */

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                aliceDepositEth,
                hardhatAsset.address
            )

            await bobAsset.requestSettlement(dummyCoin.address)
            await coordination.completeSettlements(1)

            await bobAsset.collateralize(dummyCoin.address)
            expect(await coordination.callStatic.lastSettlementIdProcessed()).to.be.eq(
                await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, dummyCoin.address)
            )
            await expect(bobAsset.requestSettlement(dummyCoin.address)).to.be.revertedWith(
                "SETTLEMENT_IN_PROGRESS"
            )
        })
        it("Allows user to request settlement after their settlement is completed ", async () => {
            /**
             * Allows requesting settlement if the most recent processed settlement Id is at least one ahead
             * of the user's last requested settlement Id
             */

            await bobAsset.requestSettlement(dummyCoin.address)

            await coordination.completeSettlements(2)

            await bobAsset.collateralize(dummyCoin.address)
            expect(
                (await coordination.callStatic.lastSettlementIdProcessed()).sub(
                    await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, dummyCoin.address)
                )
            ).to.be.eq(1)
            await expect(bobAsset.requestSettlement(dummyCoin.address)).to.not.be.reverted
        })

        it("Allows user to request settlement with multiple tokens concurrently", async () => {
            /**
             * Allows requesting settlement if the most recent processed settlement Id is at least one ahead
             * of the user's last requested settlement Id
             */

            await bobAsset.requestSettlement(dummyCoin.address)
            await bobAsset.requestSettlement(_za)

            await coordination.completeSettlements(2)
            await bobAsset.collateralize(_za)
            await bobAsset.collateralize(dummyCoin.address)
            expect(
                (await coordination.callStatic.lastSettlementIdProcessed()).sub(
                    await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, dummyCoin.address)
                )
            ).to.be.eq(1)
            expect(
                (await coordination.callStatic.lastSettlementIdProcessed()).sub(
                    await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, _za)
                )
            ).to.be.eq(0)
            await expect(bobAsset.requestSettlement(dummyCoin.address)).to.not.be.reverted
            await expect(bobAsset.requestSettlement(_za)).to.be.revertedWith("SETTLEMENT_IN_PROGRESS")
            await coordination.completeSettlements(1)
            expect(
                (await coordination.callStatic.lastSettlementIdProcessed()).sub(
                    await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, _za)
                )
            ).to.be.eq(1)
            await expect(bobAsset.requestSettlement(_za)).to.not.be.reverted
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing writing of obligations.", async () => {
        beforeEach(async () => {
            //load up custody accounts to create obligations with
            await aliceAsset.deposit({value: ethers.utils.parseEther("2")})
            await approveAndDeposit(dummyCoin, charlie, hardhatAsset, 3000)

            await aliceAsset.upgradeCoordinator()
            await bobAsset.upgradeCoordinator()
            await charlieAsset.upgradeCoordinator()
        })

        it("Coordinator able to write obligations with different recipients and deliverers ", async () => {
            /**
             * Coordinator writes several overlapping obligations with different recipients and deliverers
             * using different tokens, then verifies proper obligation result
             */

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await coordination.assetWriteObligation(
                charlieAddress,
                aliceAddress,
                dummyCoin.address,
                tokenDeposit,
                hardhatAsset.address
            )
            await coordination.assetWriteObligation(
                charlieAddress,
                aliceAddress,
                dummyCoin.address,
                tokenDeposit,
                hardhatAsset.address
            )

            expect(await hardhatAsset.obligations(aliceAddress, bobAddress, _za)).to.be.equal(
                ethTransfer.add(ethTransfer)
            )
            expect(
                await hardhatAsset.obligations(charlieAddress, aliceAddress, dummyCoin.address)
            ).to.be.equal(tokenDeposit * 2)
        })

        it("Coordinator writes several obligations to different addresses and checks that the allocated tokens decrease properly", async () => {
            /**
             * Writes several overlapping obligations with different recipients and deliverers
             * using different tokens, then verifies proper allocated tokens sum for the
             * deliverers
             */

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await coordination.assetWriteObligation(
                charlieAddress,
                aliceAddress,
                dummyCoin.address,
                tokenDeposit,
                hardhatAsset.address
            )
            await coordination.assetWriteObligation(
                charlieAddress,
                aliceAddress,
                dummyCoin.address,
                tokenDeposit,
                hardhatAsset.address
            )

            expect(await hardhatAsset.allocated(aliceAddress, _za)).to.be.equal(ethTransfer.add(ethTransfer))
            expect(await hardhatAsset.allocated(charlieAddress, dummyCoin.address)).to.be.equal(
                tokenDeposit * 2
            )
        })

        it("Rejects attempts to write obligations to custody accounts by non-coordinator accounts", async () => {
            /**
             * Non-Coordinator account attempts to write obligation to custody account.
             * Test verifies that it is properly rejected.
             */

            await expect(
                hardhatAsset.connect(alice).writeObligation(aliceAddress, bobAddress, _za, ethTransfer)
            ).to.be.revertedWith("SENDER_NOT_VALID_COORD")
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Role Management", async () => {
        it("Asset Custody Role set correctly in setupTest()", async () => {
            /**
             * Checks that the identity.getLatestAssetCustody() returns the address of the
             * asset custody contract retrieved from the setupTest() function
             */
            expect(await identity.getLatestAssetCustody()).to.be.equal(hardhatAsset.address)
        })
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing reallocation of obligations.", async () => {
        /**
         * Testing reallocations of obligations using reallocateObligations(delivererAddress, recipientAddress, tokenAddress, amount)
         * the allocations are always allocated from the zero address to the recipientAddress where the zero address is the previous
         * recipient. The delivererAddress stays the same in the new written obligation.
         */

        beforeEach(async () => {
            await bobAsset.upgradeCoordinator()
            await aliceAsset.upgradeCoordinator()

            //load up custody accounts to create obligations with
            await aliceAsset.deposit({value: ethTransfer.mul(2)})
            await coordination.assetWriteObligation(
                aliceAddress,
                _za,
                _za,
                ethTransfer.mul(2),
                hardhatAsset.address
            )
        })

        it("reallocating obligaions lowers past obligations", async () => {
            await coordination.assetReallocateObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await expect(await hardhatAsset.callStatic.obligations(aliceAddress, _za, _za)).to.be.eq(
                ethTransfer
            )
            await coordination.assetReallocateObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await expect(await hardhatAsset.callStatic.obligations(aliceAddress, _za, _za)).to.be.eq(zeroEth)
        })

        it("reallocating obligaions increases new obligations", async () => {
            await coordination.assetReallocateObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await expect(await hardhatAsset.callStatic.obligations(aliceAddress, bobAddress, _za)).to.be.eq(
                ethTransfer
            )
            await coordination.assetReallocateObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )
            await expect(await hardhatAsset.callStatic.obligations(aliceAddress, bobAddress, _za)).to.be.eq(
                ethTransfer.mul(2)
            )
        })

        it("prevents coordinator from reallocating more obligations than exist to be reallocated", async () => {
            await expect(
                coordination.assetReallocateObligation(
                    aliceAddress,
                    bobAddress,
                    _za,
                    ethTransfer.mul(2).add(1),
                    hardhatAsset.address
                )
            ).to.be.revertedWith("UNDEROBLIGATED")
        })

        it("prevents non coordinator address from calling reallocateObligation()", async () => {
            await expect(
                aliceAsset.reallocateObligation(aliceAddress, bobAddress, _za, ethTransfer)
            ).to.be.revertedWith("SENDER_NOT_VALID_COORD")
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing request funds owed. Alice obligated to Bob, then Bob requests funds", async () => {
        /**
         * requestFundsOwed() function in asset custody contract tested
         * from a starting point of a funded account Alice who has an obligation to
         * Bob.
         * function is executed and the correct execution of several operations is verified
         */

        beforeEach(async () => {
            //load up custody accounts to create obligations with
            await aliceAsset.deposit({value: ethers.utils.parseEther("2")})

            await aliceAsset.upgradeCoordinator()
            await bobAsset.upgradeCoordinator()

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                ethTransfer,
                hardhatAsset.address
            )

            await aliceAsset.requestSettlement(_za)
            await bobAsset.requestSettlement(_za)

            await coordination.completeSettlements(3)
        })

        it("Alice balance lowers by correct amount", async () => {
            let aliceBalanceBefore = await hardhatAsset.depositBalances(aliceAddress, _za)
            await bobAsset.requestFundsOwed(aliceAddress, _za)
            let aliceBalanceAfter = await hardhatAsset.depositBalances(aliceAddress, _za)
            expect(aliceBalanceBefore.sub(aliceBalanceAfter)).to.be.equal(ethTransfer)
        })

        it("Alice allocation lowers by correct amount", async () => {
            let aliceAllocatedBefore = await hardhatAsset.allocated(aliceAddress, _za)

            await bobAsset.requestFundsOwed(aliceAddress, _za)
            let aliceAllocatedAfter = await hardhatAsset.allocated(aliceAddress, _za)
            expect(aliceAllocatedBefore.sub(aliceAllocatedAfter)).to.be.equal(ethTransfer)
        })

        it("Alice's obligation to Bob lowers by correct amount", async () => {
            let aliceOwesBobBefore = await hardhatAsset.obligations(aliceAddress, bobAddress, _za)

            await bobAsset.requestFundsOwed(aliceAddress, _za)
            let aliceOwesBobAfter = await hardhatAsset.obligations(aliceAddress, bobAddress, _za)
            expect(aliceOwesBobBefore.sub(aliceOwesBobAfter)).to.be.equal(ethTransfer)
        })

        it("Bob's balance raises by correct amount minus fee", async () => {
            let {n, d} = await identity.fee()
            let fee = ethTransfer.mul(n).div(d)

            let bobBalanceBefore = await hardhatAsset.depositBalances(bobAddress, _za)

            await bobAsset.requestFundsOwed(aliceAddress, _za)
            let bobBalanceAfter = await hardhatAsset.depositBalances(bobAddress, _za)
            expect(bobBalanceAfter.sub(bobBalanceBefore)).to.be.equal(ethTransfer.sub(fee))
        })

        it("Bob's allocations unchanged", async () => {
            let bobAllocatedBefore = await hardhatAsset.allocated(bobAddress, _za)
            await bobAsset.requestFundsOwed(aliceAddress, _za)
            let bobAllocatedAfter = await hardhatAsset.allocated(bobAddress, _za)
            expect(bobAllocatedBefore.sub(bobAllocatedAfter)).to.be.equal(0)
        })

        it("Bob attempt to request funds from a token and user with no funds owed reverted properly", async () => {
            await expect(bobAsset.requestFundsOwed(charlieAddress, _za)).to.be.revertedWith("NOTHING_OWED")
        })
        it("Fee recipient recieves correct fee upon transfer", async () => {
            let {n, d} = await identity.fee()
            let fee = ethTransfer.mul(n).div(d)

            let feeRecipientBalanceBefore = await hardhatAsset.depositBalances(feeRecipientAddress, _za)
            await bobAsset.requestFundsOwed(aliceAddress, _za)
            let feeRecipientBalanceAfter = await hardhatAsset.depositBalances(feeRecipientAddress, _za)
            expect(feeRecipientBalanceAfter.sub(feeRecipientBalanceBefore)).to.be.equal(fee)
        })

        it("Calling requestFundsOwed emits event:RequestFundsOwed(address requester, address counterparty, address tokenAddress)", async () => {
            await expect(bobAsset.requestFundsOwed(aliceAddress, _za))
                .to.emit(hardhatAsset, "RequestFundsOwed")
                .withArgs(bobAddress, aliceAddress, _za)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing restrictions on request funds owed", async () => {
        beforeEach(async () => {
            //load up custody accounts
            await bobAsset.upgradeCoordinator()
            await aliceAsset.upgradeCoordinator()

            await aliceAsset.deposit({value: aliceDepositEth})
            await bobAsset.deposit({value: bobDepositEth})

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                aliceDepositEth,
                hardhatAsset.address
            )
            await bobAsset.requestSettlement(_za)
        })
        it("Disallows requesting Funds Owed if the user's settlement is currently being processed", async () => {
            /**
             * Disallows requesting Funds Owed if the most recent processed settlement Id is not at least one ahead
             * of the user's last requested settlement Id
             */

            await coordination.completeSettlements(1)

            expect(await coordination.callStatic.lastSettlementIdProcessed()).to.be.eq(
                await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, _za)
            )
            await expect(bobAsset.requestFundsOwed(aliceAddress, _za)).to.be.revertedWith(
                "SETTLEMENT_IN_PROGRESS"
            )
        })
        it("Allows user to request funds owed after their settlement is completed ", async () => {
            /**
             * Allows requesting Funds Owed if the most recent processed settlement Id is at least one ahead
             * of the user's last requested settlement Id
             */

            await coordination.completeSettlements(2)

            await expect(bobAsset.requestFundsOwed(aliceAddress, _za)).to.not.be.reverted

            expect(
                (await coordination.callStatic.lastSettlementIdProcessed()).sub(
                    await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, _za)
                )
            ).to.be.eq(1)
        })

        it("Dissallows user from requesting funds owed if unlock timer interval has not passed", async () => {
            /**
             * Dissallows request funds owed if the current block number is equal to or less than
             * the user's last requested settlement block plus timeout interval
             */

            let unlockInterval = (await identity.callStatic.unlockInterval()).toNumber()
            let blockInterval = "0x" + (unlockInterval - 1).toString(16)
            await hre.network.provider.send("hardhat_mine", [blockInterval])

            await expect(bobAsset.requestFundsOwed(aliceAddress, _za)).to.be.revertedWith(
                "SETTLEMENT_IN_PROGRESS"
            )
        })

        it("Allows user to request funds owed if unlock timer interval has passed", async () => {
            /**
             * Allows request funds owed if the current block number is at least one ahead
             * of the user's last requested settlement block plus timeout interval
             */

            let unlockInterval = (await identity.callStatic.unlockInterval()).toNumber()
            let blockInterval = "0x" + unlockInterval.toString(16)
            await hre.network.provider.send("hardhat_mine", [blockInterval])

            await expect(bobAsset.requestFundsOwed(aliceAddress, _za)).to.not.be.reverted
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("testing ERC20 withdraw", async () => {
        /**
         * withdraw() function in asset custody contract tested
         * from a starting point of funded accounts Alice and Bob
         * function is executed and the correct execution of several operations is verified
         *
         * withdraw allows the withdrawal of native tokens and ERC20 tokens
         * ERC20 token withdrawal is being tested here
         */

        beforeEach(async () => {
            //load up custody accounts
            await approveAndDeposit(dummyCoin, bob, hardhatAsset, bobDeposit)
            await approveAndDeposit(dummyCoin, alice, hardhatAsset, aliceDeposit)
            await coordination.completeSettlements(3)

            await aliceAsset.upgradeCoordinator()
            await bobAsset.upgradeCoordinator()

            await aliceAsset.requestSettlement(dummyCoin.address)
            await bobAsset.requestSettlement(dummyCoin.address)
        })

        it("Alice wallet receives tokens upon withdraw", async () => {
            let aliceBalanceBefore = await dummyCoin.balanceOf(aliceAddress)
            await aliceAsset.withdraw(aliceDeposit / 2, dummyCoin.address)
            let aliceBalanceAfter = await dummyCoin.balanceOf(aliceAddress)
            await expect(aliceBalanceAfter.sub(aliceBalanceBefore)).to.be.equal(aliceDeposit / 2)
        })

        it("Alice deposit balance in custody account decrements upon withdraw", async () => {
            let aliceBalanceBefore = await hardhatAsset.depositBalances(aliceAddress, dummyCoin.address)
            await aliceAsset.withdraw(aliceDeposit / 2, dummyCoin.address)
            let aliceBalanceAfter = await hardhatAsset.depositBalances(aliceAddress, dummyCoin.address)
            expect(aliceBalanceBefore.sub(aliceBalanceAfter)).to.be.equal(aliceDeposit / 2)
        })

        it("Asset Custody Contract token balance decrements upon withdrawal from different addresses", async () => {
            let assetCustodyBalanceBefore = await dummyCoin.balanceOf(hardhatAsset.address)
            await aliceAsset.withdraw(aliceDeposit / 2, dummyCoin.address)
            let assetCustodyBalanceAfter = await dummyCoin.balanceOf(hardhatAsset.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(aliceDeposit / 2)

            await bobAsset.withdraw(bobDeposit / 2, dummyCoin.address)
            assetCustodyBalanceAfter = await dummyCoin.balanceOf(hardhatAsset.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(totalDeposits / 2)
        })

        it("Alice withdrawal does not effect Bob's balance", async () => {
            let bobBalanceBefore = await hardhatAsset.depositBalances(bobAddress, dummyCoin.address)
            await aliceAsset.withdraw(aliceDeposit / 2, dummyCoin.address)
            let bobBalanceAfter = await hardhatAsset.depositBalances(bobAddress, dummyCoin.address)
            await expect(bobBalanceBefore).to.be.eq(bobBalanceAfter)
        })

        it("Alice cannot withdraw more than her total deposit balance, failed withdrawal does not alter contract", async () => {
            await aliceAsset.withdraw(aliceDeposit, dummyCoin.address)
            await expect(aliceAsset.withdraw(1, dummyCoin.address)).to.be.revertedWith("INSUFFICIENT_BALANCE")
            expect(await dummyCoin.balanceOf(hardhatAsset.address)).to.be.equal(bobDeposit)
            expect(await hardhatAsset.depositBalances(aliceAddress, dummyCoin.address)).to.be.equal(0)
        })

        it("Alice cannot withdraw more than her unallocated balance, failed withdrawal does not alter contract", async () => {
            // await identity.updateRole(coordRole, coordAddress)
            // // write obligation to force allocation of some of alice's tokens
            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                dummyCoin.address,
                aliceDeposit / 2,
                hardhatAsset.address
            )

            await aliceAsset.withdraw(aliceDeposit / 2, dummyCoin.address)
            await expect(aliceAsset.withdraw(1, dummyCoin.address)).to.be.revertedWith("INSUFFICIENT_BALANCE")
            expect(await dummyCoin.balanceOf(hardhatAsset.address)).to.be.equal(
                totalDeposits - aliceDeposit / 2
            )
            expect(await hardhatAsset.depositBalances(aliceAddress, dummyCoin.address)).to.be.equal(
                aliceDeposit / 2
            )
            expect(await hardhatAsset.allocated(aliceAddress, dummyCoin.address)).to.be.equal(
                aliceDeposit / 2
            )
        })

        it("ERC20 token withdrawal emits event: Withdraw(user address, token amount, token address)", async () => {
            await expect(aliceAsset.withdraw(aliceDeposit, dummyCoin.address))
                .to.emit(hardhatAsset, "Withdraw")
                .withArgs(aliceAddress, aliceDeposit, dummyCoin.address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("testing native token withdraw", async () => {
        /**
         * withdraw() function in asset custody contract tested
         * from a starting point of funded accounts Alice and Bob
         * function is executed and the correct execution of several operations is verified
         *
         * withdraw allows the withdrawal of native tokens and ERC20 tokens
         * Native token withdrawal is being tested here
         */

        beforeEach(async () => {
            //load up custody accounts
            await aliceAsset.deposit({value: aliceDepositEth})
            await bobAsset.deposit({value: bobDepositEth})

            await aliceAsset.upgradeCoordinator()
            await bobAsset.upgradeCoordinator()

            await aliceAsset.requestSettlement(_za)
            await bobAsset.requestSettlement(_za)
            await coordination.completeSettlements(3)
        })

        it("Alice wallet receives tokens upon withdraw", async () => {
            let aliceBalanceBefore = await ethers.provider.getBalance(aliceAddress)
            // save tx to get txReceipt. gas fees will need to be calculated for test
            const tx = await aliceAsset.withdraw(aliceDepositEth.div(2), _za)
            await ethers.provider.waitForTransaction(tx.hash)
            const txReceipt = await ethers.provider.getTransactionReceipt(tx.hash)

            let aliceBalanceAfter = await ethers.provider.getBalance(aliceAddress)
            await expect(aliceBalanceAfter.sub(aliceBalanceBefore)).to.be.equal(
                aliceDepositEth.div(2).sub(txReceipt.gasUsed.mul(txReceipt.effectiveGasPrice))
            )
        })

        it("Alice deposit balance in custody account decrements upon withdraw", async () => {
            let aliceBalanceBefore = await hardhatAsset.depositBalances(aliceAddress, _za)
            await aliceAsset.withdraw(aliceDepositEth.div(2), _za)
            let aliceBalanceAfter = await hardhatAsset.depositBalances(aliceAddress, _za)
            expect(aliceBalanceBefore.sub(aliceBalanceAfter)).to.be.equal(aliceDepositEth.div(2))
        })

        it("Asset Custody Contract token balance decrements upon withdrawal from different addresses", async () => {
            let assetCustodyBalanceBefore = await ethers.provider.getBalance(hardhatAsset.address)
            await aliceAsset.withdraw(aliceDepositEth.div(2), _za)
            let assetCustodyBalanceAfter = await ethers.provider.getBalance(hardhatAsset.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(
                aliceDepositEth.div(2)
            )

            await bobAsset.withdraw(bobDepositEth.div(2), _za)
            assetCustodyBalanceAfter = await ethers.provider.getBalance(hardhatAsset.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(
                totalDepositsEth.div(2)
            )
        })

        it("Alice withdrawal does not effect Bob's balance", async () => {
            let bobBalanceBefore = await hardhatAsset.depositBalances(bobAddress, _za)
            await aliceAsset.withdraw(aliceDepositEth.div(2), _za)
            let bobBalanceAfter = await hardhatAsset.depositBalances(bobAddress, _za)
            await expect(bobBalanceBefore).to.be.eq(bobBalanceAfter)
        })

        it("Alice cannot withdraw more than her total deposit balance, failed withdrawal does not alter contract", async () => {
            await aliceAsset.withdraw(aliceDepositEth, _za)
            await expect(aliceAsset.withdraw(1, _za)).to.be.revertedWith("INSUFFICIENT_BALANCE")
            expect(await ethers.provider.getBalance(hardhatAsset.address)).to.be.equal(bobDepositEth)
            expect(await hardhatAsset.depositBalances(aliceAddress, _za)).to.be.equal(0)
        })

        it("Alice cannot withdraw more than her unallocated balance, failed withdrawal does not alter contract", async () => {
            // await identity.updateRole(coordRole, coordAddress)
            // // write obligation to force allocation of some of alice's tokens

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                aliceDepositEth.div(2),
                hardhatAsset.address
            )

            await aliceAsset.withdraw(aliceDepositEth.div(2), _za)
            await expect(aliceAsset.withdraw(1, _za)).to.be.revertedWith("INSUFFICIENT_BALANCE")
            expect(await ethers.provider.getBalance(hardhatAsset.address)).to.be.equal(
                totalDepositsEth.sub(aliceDepositEth.div(2))
            )
            expect(await hardhatAsset.depositBalances(aliceAddress, _za)).to.be.equal(aliceDepositEth.div(2))
            expect(await hardhatAsset.allocated(aliceAddress, _za)).to.be.equal(aliceDepositEth.div(2))
        })

        it("Native token withdrawal emits event: Withdraw(user address, token amount, token address)", async () => {
            await expect(aliceAsset.withdraw(aliceDepositEth, _za))
                .to.emit(hardhatAsset, "Withdraw")
                .withArgs(aliceAddress, aliceDepositEth, _za)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing restrictions on withdraw", async () => {
        beforeEach(async () => {
            //load up custody accounts
            await aliceAsset.deposit({value: aliceDepositEth})
            await bobAsset.deposit({value: bobDepositEth})

            await bobAsset.upgradeCoordinator()
            await aliceAsset.upgradeCoordinator()
        })
        it("Disallows withdraw if the user's settlement is currently being processed", async () => {
            /**
             * Disallows withdraw if the most recent processed settlement Id is not at least one ahead
             * of the user's last requested settlement Id
             */

            await coordination.assetWriteObligation(
                aliceAddress,
                bobAddress,
                _za,
                aliceDepositEth,
                hardhatAsset.address
            )

            await bobAsset.requestSettlement(_za)
            await coordination.completeSettlements(1)

            expect(await coordination.callStatic.lastSettlementIdProcessed()).to.be.eq(
                await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, _za)
            )
            await expect(bobAsset.withdraw(bobDepositEth, _za)).to.be.revertedWith("SETTLEMENT_IN_PROGRESS")
        })
        it("Allows user to withdraw after their settlement is completed ", async () => {
            /**
             * Allows withdraw if the most recent processed settlement Id is at least one ahead
             * of the user's last requested settlement Id
             */

            await bobAsset.requestSettlement(_za)

            await coordination.completeSettlements(2)

            expect(
                (await coordination.callStatic.lastSettlementIdProcessed()).sub(
                    await hardhatAsset.callStatic.lastSettlementReqId(bobAddress, _za)
                )
            ).to.be.eq(1)
            await expect(bobAsset.withdraw(bobDepositEth, _za)).to.not.be.reverted
        })

        it("Dissallows user from withdrawing if unlock timer interval has not passed (skipped to save test time, unskip for full test)", async () => {
            /**
             * Dissallows withdraw if the current block number is equal to or less than
             * the user's last requested settlement block plus timeout interval
             */

            await bobAsset.requestSettlement(_za)

            let unlockInterval = (await identity.callStatic.unlockInterval()).toNumber()
            let blockInterval = "0x" + (unlockInterval - 1).toString(16)
            await hre.network.provider.send("hardhat_mine", [blockInterval])

            await expect(bobAsset.withdraw(bobDepositEth, _za)).to.be.revertedWith("SETTLEMENT_IN_PROGRESS")
        })

        it("Allows user to withdraw if unlock timer interval has passed (skipped to save test time, unskip for full test)", async () => {
            /**
             * Allows withdraw if the current block number at least one ahead
             * of the user's last requested settlement block plus timeout interval
             */

            await bobAsset.requestSettlement(_za)

            let unlockInterval = (await identity.callStatic.unlockInterval()).toNumber()
            let blockInterval = "0x" + unlockInterval.toString(16)
            await hre.network.provider.send("hardhat_mine", [blockInterval])

            await expect(bobAsset.withdraw(bobDepositEth, _za)).to.not.be.reverted
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("testing collateralization", async () => {
        /**
         * Users must set the state of their wallet to "collateralized" in order to be able to trade
         * The state is stored as an inverted boolean called "uncollateralized" in order to be initialized
         * to a collateralized state
         * Function added to allow this, and automatic removal of this state when the user requests
         * settlement. Various requirements of this added state functinality are tested here.
         */

        beforeEach(async () => {
            await aliceAsset.deposit({value: aliceDepositEth})
            await bobAsset.deposit({value: bobDepositEth})

            await aliceAsset.upgradeCoordinator()
            await bobAsset.upgradeCoordinator()
        })

        it("User Custody Accounts initialized to collateralized state", async () => {
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
            expect(await hardhatAsset.uncollateralized(aliceAddress, dummyCoin.address)).to.be.false
        })

        it("Requesting settlement de-collateralizes user", async () => {
            await aliceAsset.requestSettlement(dummyCoin.address)
            expect(await hardhatAsset.uncollateralized(aliceAddress, dummyCoin.address)).to.be.true
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
        })

        it("Requesting settlement fails if user is uncollateralized", async () => {
            await aliceAsset.requestSettlement(_za)
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.true
            expect(await hardhatAsset.uncollateralized(aliceAddress, dummyCoin.address)).to.be.false
            await expect(aliceAsset.requestSettlement(_za)).to.be.revertedWith("NOT_COLLATERALIZED")
        })

        it("collateralize() function updates uncollaterlized status for user to false", async () => {
            await aliceAsset.requestSettlement(_za)
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.true
            await aliceAsset.collateralize(_za)
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
        })

        it("does not allow withdrawal when collateralized", async () => {
            await expect(aliceAsset.withdraw(aliceDepositEth.div(2), _za)).to.be.revertedWith(
                "COLLATERALIZED"
            )
        })

        it("does not allow request funds owed when collateralized", async () => {
            // await identity.updateRole(coordRole, coordAddress)

            await coordination.assetWriteObligation(
                bobAddress,
                aliceAddress,
                _za,
                bobDepositEth.div(2),
                hardhatAsset.address
            )
            await expect(aliceAsset.requestFundsOwed(bobAddress, _za)).to.be.revertedWith("COLLATERALIZED")
        })

        it("bob wallet not collateralized when alice wallet collateralized", async () => {
            await aliceAsset.requestSettlement(_za)
            await bobAsset.requestSettlement(_za)
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.true
            expect(await hardhatAsset.uncollateralized(bobAddress, _za)).to.be.true
            await aliceAsset.collateralize(_za)
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
            expect(await hardhatAsset.uncollateralized(bobAddress, _za)).to.be.true
        })

        it("bob wallet not de-collateralized when alice wallet de-collateralized", async () => {
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
            expect(await hardhatAsset.uncollateralized(bobAddress, _za)).to.be.false
            await aliceAsset.requestSettlement(_za)
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.true
            expect(await hardhatAsset.uncollateralized(bobAddress, _za)).to.be.false
        })

        it("native token not collateralized when dummyCoin collateralized", async () => {
            await aliceAsset.requestSettlement(_za)
            await aliceAsset.requestSettlement(dummyCoin.address)
            expect(await hardhatAsset.uncollateralized(aliceAddress, dummyCoin.address)).to.be.true
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.true
            await aliceAsset.collateralize(dummyCoin.address)
            expect(await hardhatAsset.uncollateralized(aliceAddress, dummyCoin.address)).to.be.false
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.true
        })

        it("native token not de-collateralized when dummyCoin de-collateralized", async () => {
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
            await aliceAsset.requestSettlement(dummyCoin.address)
            expect(await hardhatAsset.uncollateralized(aliceAddress, dummyCoin.address)).to.be.true
            expect(await hardhatAsset.uncollateralized(aliceAddress, _za)).to.be.false
        })

        it("Collateralization emits event in format: Collateralized(userAddress,tokenAddress)", async () => {
            await aliceAsset.requestSettlement(dummyCoin.address)
            await expect(aliceAsset.collateralize(dummyCoin.address))
                .to.emit(hardhatAsset, "Collateralized")
                .withArgs(aliceAddress, dummyCoin.address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing Coordinator Upgrade and Version Handling", async () => {
        /**
         * Note: coordinator version in identity registry is 1 indexed when getLatestCoordinatorVersion() is called, but
         * it is 0 indexed as stored in the asset custody contract and in identity registry when getRoleMember() is called
         */
        it("User coordinator version and min version ID initialized to 0", async () => {
            expect(await hardhatAsset.callStatic.minCoordinatorVersionId(aliceAddress)).to.be.eq(0)
            expect(await hardhatAsset.callStatic.minCoordinatorVersionId(bobAddress)).to.be.eq(0)
            expect(await hardhatAsset.callStatic.coordinatorVersionId(aliceAddress)).to.be.eq(0)
            expect(await hardhatAsset.callStatic.coordinatorVersionId(bobAddress)).to.be.eq(0)
        })
        it("upgradeCoodrinator updates the coordinator version to the latest coordinator Id", async () => {
            await bobAsset.upgradeCoordinator()
            expect(await hardhatAsset.callStatic.coordinatorVersionId(bobAddress)).to.be.eq(1)
        })
        it("upgradeCoodrinator does not effect users other than the function caller", async () => {
            await bobAsset.upgradeCoordinator()
            expect(await hardhatAsset.callStatic.coordinatorVersionId(aliceAddress)).to.be.eq(0)
        })
        it("upgradeCoordinator prevents upgrade when already up to date", async () => {
            bobAsset.upgradeCoordinator()
            await expect(bobAsset.upgradeCoordinator()).to.be.revertedWith("COORD_UP_TO_DATE")
        })
        it("Coordinator calling writeobligation() and ReallocateObligation() fail if user has not opted in to latest coordinator", async () => {
            await expect(
                coordination.assetWriteObligation(aliceAddress, _za, _za, ethTransfer, hardhatAsset.address)
            ).to.be.revertedWith("SENDER_NOT_VALID_COORD")

            await expect(
                coordination.assetReallocateObligation(
                    aliceAddress,
                    bobAddress,
                    _za,
                    ethTransfer,
                    hardhatAsset.address
                )
            ).to.be.revertedWith("SENDER_NOT_VALID_COORD")
        })
        it("Coordinator calling writeobligation() and ReallocateObligation() succeeds if user has opted in to latest coordinator", async () => {
            await aliceAsset.upgradeCoordinator()

            await aliceAsset.deposit({value: ethTransfer})
            await expect(
                coordination.assetWriteObligation(aliceAddress, _za, _za, ethTransfer, hardhatAsset.address)
            ).to.not.be.reverted

            await expect(
                coordination.assetReallocateObligation(
                    aliceAddress,
                    bobAddress,
                    _za,
                    ethTransfer,
                    hardhatAsset.address
                )
            ).to.not.be.reverted
        })
        it("requestSettlement fails if user has not opted in to latest coordinator", async () => {
            await expect(bobAsset.requestSettlement(_za)).to.be.revertedWith("TRADER_NOT_OPTED_IN")
        })
        it("requestSettlement succeeds if user has opted in to latest coordinator", async () => {
            await bobAsset.upgradeCoordinator()
            await expect(bobAsset.requestSettlement(_za)).to.not.be.reverted
        })
        it("Coordinator version indexing functions properly", async () => {
            expect(await hardhatAsset.callStatic.coordinatorVersionId(bobAddress)).to.be.eq(0)
            expect(await identity.callStatic.getLatestCoordinatorVersion()).to.be.eq(2)
            await bobAsset.upgradeCoordinator()
            expect(await hardhatAsset.callStatic.coordinatorVersionId(bobAddress)).to.be.eq(1)

            identity.updateRole(coordRole, aliceAddress)
            expect(await identity.callStatic.getLatestCoordinatorVersion()).to.be.eq(3)
            await bobAsset.upgradeCoordinator()
            expect(await hardhatAsset.callStatic.coordinatorVersionId(bobAddress)).to.be.eq(2)
        })
    })
})
