// Copyright © 2022 TXA PTE. LTD.
import {assert, expect} from "chai"
import {Signer} from "ethers"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/types"
import {Contracts, deployWithLibraries, IdentityRegistry, setupTest, SettlementLib, _za} from "../utils"

describe("SettlementLib", function () {
    let feeRecipient: Address
    let exchange: Signer
    let noRole: Signer
    let alice: Signer
    let bob: Signer
    let trustedForwarder: Address
    let identity: IdentityRegistry
    let library: SettlementLib
    let contracts: Contracts
    let sdps: Signer[] = []
    let localCoord: Signer

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        feeRecipient = namedAccounts.feeRecipient
        exchange = await ethers.provider.getSigner(namedAccounts.exchange)
        trustedForwarder = namedAccounts.trustedForwarder
        noRole = await ethers.provider.getSigner(namedAccounts.noRole)
        alice = await ethers.provider.getSigner(namedAccounts.alice)
        bob = await ethers.provider.getSigner(namedAccounts.bob)
        let sdpAddrs = [namedAccounts.sdpAdmin1, namedAccounts.sdpAdmin2, namedAccounts.sdpAdmin3]
        for (let i = 0; i < sdpAddrs.length; i++) {
            sdps[i] = await ethers.provider.getSigner(sdpAddrs[i])
        }

        localCoord = await ethers.provider.getSigner((await getUnnamedAccounts())[0])
    })

    beforeEach(async () => {
        contracts = await setupTest()
        identity = contracts.identity
        library = contracts.library
        await identity.updateRole(await library.callStatic.ROLE_LOCALCOORD(), await localCoord.getAddress())
    })
})
