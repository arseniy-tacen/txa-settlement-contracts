// Copyright © 2022 TXA PTE. LTD.
import {
  ethers,
  deployments,
  getNamedAccounts,
  getUnnamedAccounts,
  artifacts,
} from "hardhat"

import { MerkleTree } from "merkletreejs"
import { Signer, BigNumberish, Contract, BigNumber } from "ethers"
import { Address } from "hardhat-deploy/dist/types"
import { IdentityRegistry as _IdentityRegistry } from "../typechain/IdentityRegistry"
import { SettlementLib as _ExchangeLib } from "../typechain/SettlementLib"
import { ExchangeEIP712 as _ExchangeEIP712 } from "../typechain/ExchangeEIP712"
import { SettlementCoordination as _SettlementCoordination } from "../typechain/SettlementCoordination"
import { CoordinationMock as _CoordinationMock } from "../typechain/CoordinationMock"
import { ConsensusMock as _ConsensusMock } from "../typechain/ConsensusMock"
import { SettlementDataConsensus as _SettlementDataConsensus } from "../typechain/SettlementDataConsensus"
import { CollateralMock as _CollateralMock } from "../typechain/CollateralMock"
import { Artifact, Libraries } from "hardhat/types"
import { DummyCoin as _DummyCoin } from "../typechain/DummyCoin"
import { DummyCoin777 as _DummyCoin777 } from "../typechain/DummyCoin777"
import { ProtocolToken as _ProtocolToken } from "../typechain/ProtocolToken"
import { AssetCustody as _AssetCustody } from "../typechain/AssetCustody"
import { CollateralCustody as _CollateralCustody } from "../typechain/CollateralCustody"
import { AssetMock as _AssetMock } from "../typechain/AssetMock"

export const _za = "0x0000000000000000000000000000000000000000"

export type SettlementLib = _ExchangeLib
export type ExchangeEIP712 = _ExchangeEIP712
export type SettlementCoordination = _SettlementCoordination
export type CoordinationMock = _CoordinationMock
export type ConsensusMock = _ConsensusMock
export type CollateralMock = _CollateralMock
export type SettlementDataConsensus = _SettlementDataConsensus
export type IdentityRegistry = _IdentityRegistry
export type DummyCoin = _DummyCoin
export type DummyCoin777 = _DummyCoin777
export type ProtocolToken = _ProtocolToken
export type AssetCustody = _AssetCustody
export type CollateralCustody = _CollateralCustody
export type AssetMock = _AssetMock

export interface Contracts {
  library: SettlementLib
  identity: IdentityRegistry
  coordination: SettlementCoordination
  coordinationMock: CoordinationMock
  consensusMock: ConsensusMock
  collateralMock: CollateralMock
  dummyCoin: DummyCoin
  dummyCoin777: DummyCoin777
  protocolToken: ProtocolToken
  consensus: SettlementDataConsensus
  assetCustody: AssetCustody
  collateralCustody: CollateralCustody
  assetMock: AssetMock
}

export const getSigner = async (address: string) => {
  return ethers.provider.getSigner(address)
}

export const getDeployed = async (contractName: string, signer?: Signer) => {
  const contract = await deployments.get(contractName)
  return await ethers.getContractAt(contract.abi, contract.address, signer)
}

export const getNamedSigners = async () => {
  let namedAccounts = await getNamedAccounts()
  let sdps: Signer[] = []
  let sdpAddrs = [
    namedAccounts.sdpAdmin1,
    namedAccounts.sdpAdmin2,
    namedAccounts.sdpAdmin3,
    namedAccounts.sdpAdmin4,
  ]
  for (let i = 0; i < sdpAddrs.length; i++) {
    sdps[i] = ethers.provider.getSigner(sdpAddrs[i])
  }
  return {
    exchange: ethers.provider.getSigner(namedAccounts.exchange),
    alice: ethers.provider.getSigner(namedAccounts.alice),
    bob: ethers.provider.getSigner(namedAccounts.bob),
    sdps: sdps,
  }
}

export type NamedSigners = {
  exchange: Signer
  alice: Signer
  bob: Signer
  sdps: Signer[]
}

export type Obligation = {
  amount: BigNumberish
  deliverer: string
  recipient: string
  token: string
  reallocate: boolean
}

export type ExchangeReport = {
  merkleRoot: string
  settlementId: BigNumber
  coordinator: Address
}

export const getTraders = async (amount: number) => {
  const NAMED_OFFSET = 12
  let accounts: string[] = await getUnnamedAccounts()
  let traders: Signer[] = []
  for (let i = 0; i < amount; i++) {
    traders[i] = await ethers.provider.getSigner(accounts[i + NAMED_OFFSET])
  }

  return traders
}

export const deploy = async (
  contractName: string,
  args?: Array<any>,
  signer?: Signer
) => {
  return await (
    await ethers.getContractFactory(contractName, signer)
  ).deploy(...(args ?? []))
}

export const deployWithLibraries = async (
  contractName: string,
  libraries: Libraries,
  args?: Array<any>,
  signer?: Signer
) => {
  const cArtifact = await artifacts.readArtifact(contractName)
  const linkedBytecode = linkBytecode(cArtifact, libraries)
  const Contract = await ethers.getContractFactory(
    cArtifact.abi,
    linkedBytecode,
    signer
  )
  return await (
    await ethers.getContractFactory(cArtifact.abi, Contract.bytecode, signer)
  ).deploy(...(args ?? []))
}

export const setupTest = deployments.createFixture(
  async ({ deployments }, options) => {
    await deployments.fixture(["Test", "Tokens"])
    return {
      library: (await getDeployed("SettlementLib")) as SettlementLib,
      identity: (await getDeployed("IdentityRegistry")) as IdentityRegistry,
      coordination: (await getDeployed(
        "SettlementCoordination"
      )) as SettlementCoordination,
      coordinationMock: (await getDeployed(
        "CoordinationMock"
      )) as CoordinationMock,
      consensusMock: (await getDeployed("ConsensusMock")) as ConsensusMock,
      collateralMock: (await getDeployed("CollateralMock")) as CollateralMock,
      dummyCoin: (await getDeployed("DummyCoin")) as DummyCoin,
      dummyCoin777: (await getDeployed("DummyCoin777")) as DummyCoin777,
      protocolToken: (await getDeployed("ProtocolToken")) as ProtocolToken,
      consensus: (await getDeployed(
        "SettlementDataConsensus"
      )) as SettlementDataConsensus,
      assetCustody: (await getDeployed("AssetCustody")) as AssetCustody,
      collateralCustody: (await getDeployed(
        "CollateralCustody"
      )) as CollateralCustody,
      assetMock: (await getDeployed("AssetMock")) as AssetMock,
    }
  }
)

export function linkBytecode(artifact: Artifact, libraries: Libraries) {
  let bytecode = artifact.bytecode

  for (const [fileName, fileReferences] of Object.entries(
    artifact.linkReferences
  )) {
    for (const [libName, fixups] of Object.entries(fileReferences)) {
      const addr = libraries[libName]
      if (addr === undefined) {
        continue
      }

      for (const fixup of fixups) {
        bytecode =
          bytecode.substr(0, 2 + fixup.start * 2) +
          addr.substr(2) +
          bytecode.substr(2 + (fixup.start + fixup.length) * 2)
      }
    }
  }

  return bytecode
}

export const copyArray = (arr: Obligation[]): Obligation[] =>
  JSON.parse(JSON.stringify(arr))

/**
 * Helper Functions for tests to improve implementability and readability
 */

/**
 * Shorter implementation for depositing erc tokens
 * Works for all Asset Custody, Collateral Custody and related Mock Contracts
 *
 * @param token token contract
 * @param signer the user who is depositing tokens to their collateral or asset custody account
 * @param contract Asset Custody, Collateral Custody, or Mock contract for submitting deposit to
 * @param deposit  Amount of tokens to be deposited
 */
export async function approveAndDeposit(
  token: Contract,
  signer: Signer,
  custody: Contract,
  deposit: number
) {
  await token.connect(signer).approve(custody.address, deposit)
  await custody.connect(signer).depositToken(deposit, token.address)
}
/**
 * Function to simplify assigning an SDP Operator to an SDP Admin account for tests
 * That need this to be dont but are not testing this functionality specifically
 * @param admin sdp admin, who controls the collateral
 * @param operator sdp operator who will be able to submit obligation reports using admin's collateral
 * @param contract Collateral Custody Contract where this permission assignment is submitted to
 */

export async function operatorInit(
  admin: Signer,
  operator: Signer,
  contract: Contract
) {
  let sdpAdmin1AddressBytes = ethers.utils.arrayify(await admin.getAddress())
  let signedMessage = await operator.signMessage(sdpAdmin1AddressBytes)
  let signature = signedMessage.substring(2)
  let r = "0x" + signature.substring(0, 64)
  let s = "0x" + signature.substring(64, 128)
  let v = parseInt(signature.substring(128, 130), 16)
  await contract
    .connect(admin)
    .approveOperator(await operator.getAddress(), v, r, s)
}

/**
 * Gets a merkle root for an array of obligations needed for obligation reports
 * @param obligations array of obligations
 * @returns returns a merkle root
 */
export async function merkleFromObs(obligations: Obligation[]) {
  const calculateLeafFromObligation = (obligation: Obligation) =>
    ethers.utils.solidityKeccak256(
      ["uint256", "address", "address", "address", "bool"],
      [
        obligation.amount,
        obligation.deliverer,
        obligation.recipient,
        obligation.token,
        obligation.reallocate,
      ]
    )
  let merkleTree = new MerkleTree(
    obligations.map(calculateLeafFromObligation),
    ethers.utils.keccak256,
    { hashLeaves: false, sortPairs: true }
  )
  return merkleTree.getHexRoot()
}
