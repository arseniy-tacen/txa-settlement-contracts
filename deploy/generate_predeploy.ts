// Copyright © 2022 TXA PTE. LTD.
import { HardhatRuntimeEnvironment, Libraries } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import { IdentityRegistry, SettlementLib } from "../test/utils"
import { PredeployConstants } from "../typechain/PredeployConstants"
import fs from "fs"

const {
  ERC1820_REGISTRY_DEPLOY_TX,
  ERC1820_REGISTRY_ADDRESS,
} = require("@openzeppelin/test-helpers/src/data")

/**
 * Runs deployments of contracts that are instrumented for adding to a geth genesis block.
 * For each contract, grabs the deployed bytecode and appends it to the genesis block file
 * so that the specified addresses will have the same bytecode when running on test chain.
 */
const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, ethers } = hre
  const { deploy } = deployments
  const { exchange, feeRecipient, trustedForwarder } = await getNamedAccounts()

  const deployFromExchange = async (
    contractName: string,
    args: any[] = [],
    libraries?: Libraries
  ) => {
    const factory = await ethers.getContractFactory(
      contractName,
      await ethers.getSigner(exchange)
    )
    console.log("Deploying " + contractName)
    return await factory.deploy(...args)
  }

  // Object to write to geth genesis block
  interface DeployedBytecodes {
    [address: string]: {
      balance: string
      code: string
    }
  }
  let deployedBytecodes: DeployedBytecodes = {}
  const addBytecode = async (contractAddress: string, byteCode: string) => {
    deployedBytecodes[contractAddress] = {
      balance: "0",
      code: byteCode,
    }
  }

  // Deploy libraries
  let chainId = await ethers.provider.send("eth_chainId", [])
  let eip712 = await deployFromExchange("ExchangeEIP712", ["1", chainId])
  let library = await deployFromExchange("SettlementLib")
  let percentLib = await deployFromExchange("FullMath")
  let predeployConstants = (await deployFromExchange(
    "PredeployConstants"
  )) as PredeployConstants

  // Deploy ERC1820Registry
  if (
    (await ethers.provider.getCode(ERC1820_REGISTRY_ADDRESS)).length <=
    "0x0".length
  ) {
    console.log("Deploying ERC1820 Registry ...")
    await (
      await (
        await ethers.getSigners()
      )[0].sendTransaction({
        to: "0xa990077c3205cbDf861e17Fa532eeB069cE9fF96",
        value: ethers.utils.parseEther("1.08"),
      })
    ).wait()
    await (
      await ethers.provider.sendTransaction(ERC1820_REGISTRY_DEPLOY_TX)
    ).wait()
  }
  await addBytecode(
    ERC1820_REGISTRY_ADDRESS,
    await ethers.provider.getCode(ERC1820_REGISTRY_ADDRESS)
  )

  const withdrawInterval = hre.network.name.startsWith("test") ? 241 : 6000
  // Identity
  let identity = await deployFromExchange("IdentityRegistryPreDeploy", [
    exchange,
    withdrawInterval,
  ])
  await addBytecode(
    await predeployConstants.callStatic.IDENTITY_REGISTRY(),
    await ethers.provider.getCode(identity.address)
  )

  await addBytecode(
    await predeployConstants.callStatic.LIBRARY(),
    await ethers.provider.getCode(library.address)
  )

  // Asset Custody Contract
  let assetCustody = await deployFromExchange("AssetCustodyPreDeploy", [])
  await addBytecode(
    await predeployConstants.callStatic.SINGLETON(),
    await ethers.provider.getCode(assetCustody.address)
  )

  // Collateral Custody Contract
  let collateralCustody = await deployFromExchange(
    "CollateralCustodyPreDeploy",
    []
  )
  await addBytecode(
    await predeployConstants.callStatic.COLLATERAL_CUSTODY(),
    await ethers.provider.getCode(collateralCustody.address)
  )

  // Coordinator
  let coord = await deployFromExchange("SettlementCoordinationPreDeploy", [])
  await addBytecode(
    await predeployConstants.callStatic.COORDINATOR(),
    await ethers.provider.getCode(coord.address)
  )

  // Consensus
  let consensus = await deployFromExchange(
    "SettlementDataConsensusPreDeploy",
    []
  )
  await addBytecode(
    await predeployConstants.callStatic.CONSENSUS(),
    await ethers.provider.getCode(consensus.address)
  )

  // append addresses with pre-deployed bytecode to cliquebait genesis block
  let data = await fs.promises.readFile("base_cliquebait.json")
  let genesisBlock = await JSON.parse(data.toString())
  genesisBlock.alloc = Object.assign(genesisBlock.alloc, deployedBytecodes)
  await fs.promises.writeFile("cliquebait.json", JSON.stringify(genesisBlock))
}
func.tags = ["PreDeploy"]
export default func
