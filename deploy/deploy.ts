// Copyright © 2022 TXA PTE. LTD.
import { HardhatRuntimeEnvironment, Libraries } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import { IdentityRegistry, SettlementLib } from "../test/utils"

const {
  ERC1820_REGISTRY_DEPLOY_TX,
  ERC1820_REGISTRY_ADDRESS,
} = require("@openzeppelin/test-helpers/src/data")

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, ethers } = hre
  const { deploy } = deployments
  const {
    exchange,
    feeRecipient,
    trustedForwarder,
    sdpAdmin1,
    sdpAdmin2,
    sdpAdmin3,
    sdpAdmin4,
  } = await getNamedAccounts()

  const deployFromExchange = (
    contractName: string,
    args?: any[],
    libraries?: Libraries
  ) => {
    return deploy(contractName, {
      from: exchange,
      args,
      libraries,
      log: true,
      fieldsToCompare: "data",
      deterministicDeployment: true,
    })
  }

  const deployOrExisting = async (
    contractName: string,
    deployArgs?: any[],
    libraries?: Libraries
  ) => {
    let newContract
    let existing = await deployments.getOrNull(contractName)
    if (existing !== undefined && existing !== null) {
      // deployment exists; determine if upgrade is needed by comparing source files
      if (existing.metadata !== undefined) {
        let existingSources = JSON.parse(existing.metadata).sources
        let newContractArtifact = await deployments.getExtendedArtifact(
          contractName
        )

        if (newContractArtifact.metadata !== undefined) {
          let newSources = JSON.parse(newContractArtifact.metadata).sources
          let sourcesMatch = true
          for (let source of Object.keys(newSources)) {
            if (
              existingSources[source] === null ||
              existingSources[source] === undefined
            ) {
              sourcesMatch = false
              break
            }
            if (
              existingSources[source].keccak256 !== newSources[source].keccak256
            ) {
              sourcesMatch = false
              break
            }
          }
          newContract = sourcesMatch
            ? existing
            : await deployFromExchange(contractName, deployArgs, libraries)
        } else {
          throw Error("New deployment had no metadata")
        }
      } else {
        throw Error("Old deployment had no metadata")
      }
    } else {
      newContract = await deployFromExchange(
        contractName,
        deployArgs,
        libraries
      )
    }
    return newContract
  }

  // Deploy libraries
  let chainId = await ethers.provider.send("eth_chainId", [])
  let eip712 = await deployFromExchange("ExchangeEIP712", ["1", chainId])
  let library = await deployFromExchange("SettlementLib")
  let libraryInstance = (await ethers.getContractAt(
    "SettlementLib",
    library.address
  )) as SettlementLib
  let percentLib = await deployFromExchange("FullMath")

  // Deploy ERC1820Registry
  if (
    (await ethers.provider.getCode(ERC1820_REGISTRY_ADDRESS)).length <=
    "0x0".length
  ) {
    console.log("Deploying ERC1820 Registry ...")
    await (
      await (
        await ethers.getSigners()
      )[0].sendTransaction({
        to: "0xa990077c3205cbDf861e17Fa532eeB069cE9fF96",
        value: ethers.utils.parseEther("1.08"),
      })
    ).wait()
    await ethers.provider.sendTransaction(ERC1820_REGISTRY_DEPLOY_TX)
  }

  const withExchangeLib = { ["SettlementLib"]: library.address }

  const withdrawInterval = hre.network.name.startsWith("test") ? 241 : 6000
  // Identity
  let identity = await deployFromExchange(
    "IdentityRegistry",
    [exchange, withdrawInterval],
    {
      ...withExchangeLib,
      ["FullMath"]: percentLib.address,
    }
  )

  let identityInstance = (await ethers.getContractAt(
    identity.abi,
    identity.address
  )) as IdentityRegistry

  // Asset Custody Contract
  let assetCustody = await deployFromExchange("AssetCustody", [
    identity.address,
  ])
  let hasAssetCustodyRole = await identityInstance.callStatic.hasRole(
    await libraryInstance.callStatic.ROLE_ASSET_CUSTODY(),
    assetCustody.address
  )
  let assetCustodyRoleCount =
    await identityInstance.callStatic.getRoleMemberCount(
      await libraryInstance.callStatic.ROLE_ASSET_CUSTODY()
    )
  if (assetCustodyRoleCount.eq(0)) {
    console.log("Initializing first Asset Custody Contract ...")
    await identityInstance.initializeRole(
      await libraryInstance.callStatic.ROLE_ASSET_CUSTODY(),
      assetCustody.address
    )
  } else if (!hasAssetCustodyRole) {
    console.log("Upgrading Asset Custody Contract ...")
    await identityInstance.updateRole(
      await libraryInstance.callStatic.ROLE_ASSET_CUSTODY(),
      assetCustody.address
    )
  }

  // Collateral Custody Contract
  let collateralCustody = await deployFromExchange("CollateralCustody", [
    identity.address,
  ])
  let hasCollateralCustodyRole = await identityInstance.callStatic.hasRole(
    await libraryInstance.callStatic.ROLE_COLLATERAL_CUSTODY(),
    collateralCustody.address
  )
  let collateralCustodyRoleCount =
    await identityInstance.callStatic.getRoleMemberCount(
      await libraryInstance.callStatic.ROLE_COLLATERAL_CUSTODY()
    )
  if (collateralCustodyRoleCount.eq(0)) {
    console.log("Initializing first Collateral Custody ...")
    await identityInstance.initializeRole(
      await libraryInstance.callStatic.ROLE_COLLATERAL_CUSTODY(),
      collateralCustody.address
    )
  } else if (!hasCollateralCustodyRole) {
    console.log("Upgrading Collateral Custody ...")
    await identityInstance.updateRole(
      await libraryInstance.callStatic.ROLE_COLLATERAL_CUSTODY(),
      collateralCustody.address
    )
  }

  // Set SDP Admins
  await identityInstance.grantSDPAdmin(sdpAdmin1)
  await identityInstance.grantSDPAdmin(sdpAdmin2)
  await identityInstance.grantSDPAdmin(sdpAdmin3)
  await identityInstance.grantSDPAdmin(sdpAdmin4)

  // Coordinator
  let coord = await deployOrExisting(
    "SettlementCoordination",
    [identity.address],
    withExchangeLib
  )
  let hasLocalCoordRole = await identityInstance.callStatic.hasRole(
    await libraryInstance.callStatic.ROLE_LOCALCOORD(),
    coord.address
  )
  let coordRoleCount = await identityInstance.callStatic.getRoleMemberCount(
    await libraryInstance.callStatic.ROLE_LOCALCOORD()
  )
  if (coordRoleCount.eq(0)) {
    console.log("Initializing first Coordinator ...")
    await identityInstance.initializeRole(
      await libraryInstance.callStatic.ROLE_LOCALCOORD(),
      coord.address
    )
  } else if (!hasLocalCoordRole) {
    console.log("Upgrading Coordinator ...")
    await identityInstance.updateRole(
      await libraryInstance.callStatic.ROLE_LOCALCOORD(),
      coord.address
    )
  }

  await identityInstance.setFeeRecipient(feeRecipient)

  // Consensus
  let consensus = await deployOrExisting(
    "SettlementDataConsensus",
    [identity.address],
    withExchangeLib
  )
  let hasConsensusRole = await identityInstance.callStatic.hasRole(
    await libraryInstance.callStatic.ROLE_CONSENSUS(),
    consensus.address
  )
  let consensusRoleCount = await identityInstance.callStatic.getRoleMemberCount(
    await libraryInstance.callStatic.ROLE_CONSENSUS()
  )
  if (consensusRoleCount.eq(0)) {
    console.log("Initializing first Consensus ...")
    await identityInstance.initializeRole(
      await libraryInstance.callStatic.ROLE_CONSENSUS(),
      consensus.address
    )
  } else if (!hasConsensusRole) {
    console.log("Upgrading consensusRoleCount ...")
    await identityInstance.updateRole(
      await libraryInstance.callStatic.ROLE_CONSENSUS(),
      consensus.address
    )
  }
}
func.tags = ["Core"]
export default func
