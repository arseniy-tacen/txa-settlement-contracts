// Copyright © 2022 TXA PTE. LTD.
import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import { getDeployed } from "../test/utils"
import { IdentityRegistry } from "../typechain/IdentityRegistry"
import { SettlementLib } from "../typechain/SettlementLib"

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre
  const { deploy } = deployments
  const { exchange } = await getNamedAccounts()
  let identity = (await getDeployed("IdentityRegistry")) as IdentityRegistry
  const library = (await getDeployed("SettlementLib")) as SettlementLib
  let coord = await deploy("CoordinationMock", {
    from: exchange,
    args: [identity.address],
    deterministicDeployment: true,
    log: true,
  })
  await identity.updateRole(
    await library.callStatic.ROLE_LOCALCOORD(),
    coord.address
  )
  let assetCustody = await deploy("AssetMock", {
    from: exchange,
    args: [identity.address],
    deterministicDeployment: true,
    log: true,
  })
  let collateralCustody = await deploy("CollateralMock", {
    from: exchange,
    args: [identity.address],
    deterministicDeployment: true,
    log: true,
  })
  // uncomment to grant asset custody contract role to AssetMock
  // await identity.updateRole(
  //   await library.callStatic.ROLE_ASSET_CUSTODY(),
  //   assetCustody.address
  // )
  let consensusMock = await deploy("ConsensusMock", {
    from: exchange,
    args: [identity.address],
    deterministicDeployment: true,
    log: true,
  })
}
func.tags = ["Test"]
func.dependencies = ["Core"]
export default func
