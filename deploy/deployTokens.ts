// Copyright © 2022 TXA PTE. LTD.
import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import {
  DummyCoin,
  getSigner,
  IdentityRegistry,
  SettlementLib,
  getDeployed,
} from "../test/utils"

// Each token should be mintable
const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, getUnnamedAccounts, ethers } = hre
  const { deploy } = deployments
  const namedAccounts = await getNamedAccounts()
  let exchange = namedAccounts.exchange
  const accounts = (await getUnnamedAccounts()).concat(
    Object.values(namedAccounts)
  )

  const library = (await getDeployed("SettlementLib")) as SettlementLib
  const identity = (await getDeployed("IdentityRegistry")) as IdentityRegistry
  const exchangeSigner = await getSigner(exchange)
  const tokenRole = await library.callStatic.TRADEABLE_TOKEN()
  const protocolTokenRole = await library.callStatic.PROTOCOL_TOKEN()

  console.log("Deploying DummyCoin...")
  const network = hre.network.name
  const dcName = "DummyCoin"
  const dcTicker = "DMC"

  const airdropAddresses = network.startsWith("smoke_test")
    ? [(await getUnnamedAccounts()).slice(10), (await getUnnamedAccounts())[25]]
    : accounts

  console.log(`Deploying DummyCoin with name ${dcName} and ticker ${dcTicker}`)
  const dc = await deploy("DummyCoin", {
    from: exchange,
    args: [airdropAddresses, dcName, dcTicker],
  })
  await identity.connect(exchangeSigner).grantRole(tokenRole, dc.address)

  console.log("Deploying DummyCoin777...")
  const dc777 = await deploy("DummyCoin777", {
    from: exchange,
    args: [accounts, []],
  })
  await identity.connect(exchangeSigner).grantRole(tokenRole, dc777.address)

  console.log("Deploying ProtocolToken...")
  const ptc = await deploy("ProtocolToken", {
    from: exchange,
    args: [accounts, []],
  })
  await identity
    .connect(exchangeSigner)
    .grantRole(protocolTokenRole, ptc.address)

  console.log("Deploying USDT...")
  const usdt = await deploy("USDT", {
    from: exchange,
    args: [airdropAddresses],
    deterministicDeployment: true,
    log: true,
  })
  await identity.connect(exchangeSigner).grantRole(tokenRole, usdt.address)
}
func.tags = ["Tokens"]
func.dependencies = ["Core"]
export default func
