import { HardhatRuntimeEnvironment } from "hardhat/types"

export const depositToken = async (
  account: string,
  amount: number,
  hre: HardhatRuntimeEnvironment,
  token: string
) => {
  const SINGLETON_CONTRACT_NAME = "AssetCustody"
  const TOKEN_CONTRACT_NAME = "ERC20"
  const accounts = await hre.getNamedAccounts()
  const signer = await hre.ethers.getSigner(accounts[account] ?? account)

  const assetCustodyDeployment = await hre.deployments.getOrNull(
    SINGLETON_CONTRACT_NAME
  )
  if (assetCustodyDeployment === null || assetCustodyDeployment === undefined) {
    throw new Error(`No deployment found for ${SINGLETON_CONTRACT_NAME}`)
  }
  const assetCustodyContract = await hre.ethers.getContractAt(
    SINGLETON_CONTRACT_NAME,
    assetCustodyDeployment.address,
    signer
  )

  const tokenDeployment = await hre.deployments.getOrNull(token)

  if (tokenDeployment === null || tokenDeployment === undefined) {
    throw new Error(`No deployment found for token contract with name ${token}`)
  }
  const tokenContract = await hre.ethers.getContractAt(
    TOKEN_CONTRACT_NAME,
    tokenDeployment.address,
    signer
  )
  const tokenSymbol = await tokenContract.callStatic.symbol()

  await (
    await tokenContract.approve(assetCustodyContract.address, amount)
  ).wait()
  await (
    await assetCustodyContract.depositToken(amount, tokenContract.address)
  ).wait()

  console.log(
    `${amount} ${tokenSymbol} (${tokenContract.address}) deposited by ${account}`
  )
}
