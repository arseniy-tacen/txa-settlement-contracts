FROM registry.gitlab.com/projecttxa/txa-dsl/txa-utils-cliquebait

EXPOSE 8545
EXPOSE 30303
EXPOSE 30303/udp


WORKDIR /app

COPY . /app

RUN apk add nodejs npm

RUN npm install --only=prod
RUN export DOCKER_BUILD=true 
RUN npx hardhat compile
RUN npx hardhat deploy --tags PreDeploy
RUN mv cliquebait.json ../cliquebait/cliquebait.json

ENTRYPOINT [ "./entry.sh" ]
