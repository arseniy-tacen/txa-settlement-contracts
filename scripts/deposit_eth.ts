import { ethers, getNamedAccounts } from "hardhat"
import { AssetCustody } from "../typechain/AssetCustody"

export async function depositEth() {
  const { alice, bob } = await getNamedAccounts()
  const assetCustody = (await ethers.getContractAt(
    "AssetCustody",
    "0x8A3cB37a7aC1acEd79034E93D409906eACb2Da6b"
  )) as AssetCustody
  for (const address of [alice, bob]) {
    await (
      await assetCustody
        .connect(await ethers.getSigner(address))
        .deposit({ value: ethers.utils.parseEther("1.256") })
    ).wait()
  }
  console.log("deposited 1.256 eth for alice and bob")
}

depositEth()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
